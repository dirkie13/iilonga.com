<?php

namespace App\Helpers;

use App\Mail\SendEmail;

use Mail;

use Log;

class SendEmailHelper
{

    public $subject;
    public $message;

    public $emails = [];


    public function __construct($subject, $title, $message)
    {
        $this->subject = $subject;
        $this->title = $title;
        $this->message = $message;
    }

    public function addEmail($email)
    {
        $this->emails[] = $email;
    }

    public function sendEmail()
    {

        foreach ($this->emails as $email) {
            
            $details = [
                'title' => $this->title,
                'subject' => $this->subject,
                'body' => $this->message
            ];

            try {
                Mail::to($email)
                ->send(new SendEmail($details));
            }
            catch (\Exception $e){

                // abort(403, $e->getMessage());
                Log::critical($e->getMessage());
                Log::critical("The email could not be sent to: ".$email.'. Message Body is: '.$this->message);
                
            }

            
        }
    }
}
