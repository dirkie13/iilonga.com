<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Password;

use Illuminate\Http\Request;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function reset(Request $request)
    {
        $request->validate($this->rules(), $this->validationErrorMessages());

        // Here we will attempt to reset the user's password. If it is successful we
        // will update the password on an actual user model and persist it to the
        // database. Otherwise we will parse the error and return the response.
        $response = $this->broker()->reset(
            $this->credentials($request),
            function ($user, $password) {
                $this->resetPassword($user, $password);
            }
        );

        // If the password was successfully reset, we will redirect the user back to
        // the application's home authenticated view. If there is an error we can
        // redirect them back to where they came from with their error message.
        return $response == Password::PASSWORD_RESET
            ? $this->sendResetResponse($request, $response)
            : $this->sendResetFailedResponse($request, $response);
    }

    
    /**
     * Get the password reset validation rules.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'token' => 'required',
            'email' => 'required|email',
            'password'          =>      [
                'required',
                'confirmed',
                'min:10',
                function ($attribute, $value, $fail): void {

                    $lowerCaseMatch = preg_match('/[a-z]/', $value);    // must contain at least one lowercase letter
                    $upperCaseMatch = preg_match('/[A-Z]/', $value);    // must contain at least one uppercase letter
                    $digitMatch = preg_match('/[0-9]/', $value);        // must contain at least one digit

                    // $valid = ($lowerCaseMatch == true && $upperCaseMatch == true && $digitMatch == true) ;

                    if (!$lowerCaseMatch) {

                        $fail('The password must contain atleast one lower case character');
                    }
                    if (!$upperCaseMatch) {

                        $fail('The password must contain atleast one upper case character');
                    }
                    if (!$digitMatch) {

                        $fail('The password must contain atleast one number');
                    }
                }
            ],
        ];
    }
}
