<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\EmploymentType;
use App\Models\JobCategory;
use Auth;
use App\Models\User;
use App\Models\Credit;
use App\Models\YearsOfExperience;

class BrowseCvController extends Controller
{
    public function index(Request $request)
    {
        // dd($request->all());
        // $employmentTypes = EmploymentType::all();
        // $jobCategories = JobCategory::all();


        // $users = User::whereRole('candidate')->whereHas('userCandidate', function ($query) {
        //     $query->where('active', '=', true);
        // })->paginate(15);
        // // dd($user);
        // // return view('website.employer-portal.browse-candidates')->with('users', $users)->with('employmentTypes', $employmentTypes)->with('jobCategories', $jobCategories);


        // return view('website.browse-cv')->with('users', $users)->with('employmentTypes', $employmentTypes)->with('jobCategories', $jobCategories);




        $employmentTypes = EmploymentType::all();
        $jobCategories = JobCategory::whereNull('parent_id')->orderBy('column_order')->get();
        $yearsOfExperiences = YearsOfExperience::all();
        
// dd($request->all());
        

        if ($request->allCategories != null && $request->allCategories == 'on') {

            $users = User::whereRole('candidate','userCandidate.professional_title ')->whereHas('userCandidate', function ($query) {
                $query->where('active', '=', true);
                // $query->where('jobCategories.id', '=', 1);

            });
            // dd('here');


                // ->whereHas('userCandidate.jobCategories', function ($query) use ($request) {
                //     // $query->where('active', '=', true);
                //     $query->whereIn('job_categories.id', $request->jobCategory);
                // })->with('userCandidate.jobCategories')


                
        } elseif ($request->jobCategory != null) {
            $users = User::whereRole('candidate')->whereHas('userCandidate', function ($query) {
                $query->where('active', '=', true);
                

            })


                ->whereHas('userCandidate.jobCategories', function ($query) use ($request) {
                    // $query->where('active', '=', true);
                    // $query->whereIn('job_categories.parent.id', $request->jobCategory);
                    $query->whereHas('parent', function ($query) use ($request) {
                        $query->whereIn('id', $request->jobCategory);
                    });
                })->with('userCandidate.jobCategories');


                
        } else {

            $users = User::whereRole('candidate')->whereHas('userCandidate', function ($query) {
                $query->where('active', '=', true);
                // $query->where('jobCategories.id', '=', 1);

            })


                ->whereHas('userCandidate.jobCategories', function ($query) use ($request) {
                    // $query->where('active', '=', true);
                    $query->whereIn('job_categories.id', []);
                })->with('userCandidate.jobCategories');


                
        }
        // dd($request->all());
        if (!empty($request->yearsOfExperienceChosen) ) {
            // dd($request->yearsOfExperienceChosen);
            $yearsOfExperienceChosen = YearsOfExperience::whereId($request->yearsOfExperienceChosen)->first();
            $minimumExperience = $yearsOfExperienceChosen->value;
            // dd( $minimumExperience);
            $users = $users->whereHas('userCandidate', function ($query) use ($minimumExperience) {
                $query->whereHas('jobCategories', function ($q) use ($minimumExperience) {
                    $q->where('job_category_user_candidate.years_of_experience', '>=', $minimumExperience);
                });
            });
        } 

        

        
        

        $users = $users->paginate(15);

        if (Auth::check() && Auth::user()->role == 'business') {
            foreach ($users as $user) {
                $token = Credit::where('user_business_id', Auth::user()->userBusiness->id)->where('user_candidate_id', $user->userCandidate->id)->first();

                if ($token == false) {
                    
                    // $user->userCandidate->created_at
                    
        
                    $user->profile_image = '/website/website/logo/iilonga_icon-01_lbwd5p';
                } else {
                    $user->profile_image = $user->userCandidate->profile_image;
                }
                // dd($user);

            }
        }
        // dd($user);
        // return view('website.employer-portal.browse-candidates')->with('users', $users)->with('employmentTypes', $employmentTypes)->with('jobCategories', $jobCategories);
        if (!isset($request->jobCategory)) {
            $jobCategory = [];
        } else {
            $jobCategory = $request->jobCategory;
        }
        // dd($request->all());
        if (!empty($request->yearsOfExperienceChosen)) {
            $yearsOfExperienceChosen = $request->yearsOfExperienceChosen;
        } else {
            $yearsOfExperienceChosen = YearsOfExperience::first()->id;
        }
        // dd($yearsOfExperienceChosen);
        return view('website.browse-cv')->with('jobCategoriesInput', $jobCategory)->with('yearsOfExperienceInput', $yearsOfExperienceChosen)->with('users', $users)->with('employmentTypes', $employmentTypes)->with('jobCategories', $jobCategories)->with('yearsOfExperiences', $yearsOfExperiences);
    }

    public function filterResults(Request $request)
    {

        // dd($request->all());
        // foreach($request->jobCategory as $job){
        // }


        $employmentTypes = EmploymentType::all();
        $jobCategories = JobCategory::all();

        if ($request->allCategories != null && $request->allCategories == 'on') {

            $users = User::whereRole('candidate')->whereHas('userCandidate', function ($query) {
                $query->where('active', '=', true);
                // $query->where('jobCategories.id', '=', 1);

            })


                // ->whereHas('userCandidate.jobCategories', function ($query) use ($request) {
                //     // $query->where('active', '=', true);
                //     $query->whereIn('job_categories.id', $request->jobCategory);
                // })->with('userCandidate.jobCategories')


                ->paginate(15);
        } elseif ($request->jobCategory != null) {
            $users = User::whereRole('candidate')->whereHas('userCandidate', function ($query) {
                $query->where('active', '=', true);
                // $query->where('jobCategories.id', '=', 1);

            })


                ->whereHas('userCandidate.jobCategories', function ($query) use ($request) {
                    // $query->where('active', '=', true);
                    $query->whereIn('job_categories.id', $request->jobCategory);
                })->with('userCandidate.jobCategories')


                ->paginate(15);
        } else {

            $users = User::whereRole('candidate')->whereHas('userCandidate', function ($query) {
                $query->where('active', '=', true);
                // $query->where('jobCategories.id', '=', 1);

            })


                ->whereHas('userCandidate.jobCategories', function ($query) use ($request) {
                    // $query->where('active', '=', true);
                    $query->whereIn('job_categories.id', []);
                })->with('userCandidate.jobCategories')


                ->paginate(15);
        }
        // dd($user);
        // return view('website.employer-portal.browse-candidates')->with('users', $users)->with('employmentTypes', $employmentTypes)->with('jobCategories', $jobCategories);

        // dd($users);
        return view('website.browse-cv')->with('users', $users)->with('employmentTypes', $employmentTypes)->with('jobCategories', $jobCategories);
    }
}
