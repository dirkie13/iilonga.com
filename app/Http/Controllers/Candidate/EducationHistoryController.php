<?php

namespace App\Http\Controllers\Candidate;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\EducationType;
use App\Models\UserCandidateEducationHistory;
use App\Models\UserCandidate;

use Auth;

class EducationHistoryController extends Controller
{
    public function index()
    {
     
        return view('website.candidate-portal.education-history');
    }


    public function getDetail(){
        
        $educationHistories = UserCandidateEducationHistory::whereUserCandidateId(Auth::user()->userCandidate->id)->get();
        $educationTypes = EducationType::all();
        $userCandidate = UserCandidate::whereUserId(Auth::user()->id)->first();
        return [
            'success' => true,
            'educationHistories' => $educationHistories,
            'educationTypes' => $educationTypes,
            'userCandidate' => $userCandidate,
        ];
    }

    public function addEducationHistory(Request $request){
        // dd($request);
        $educationHistory = new UserCandidateEducationHistory;
        $educationHistory->user_candidate_id = Auth::user()->userCandidate->id;
        $educationHistory->education_type_id = $request->form['educationType_id'];
     
        $educationHistory->title = $request->form['title'];
        $educationHistory->date_from = $request->form['dateFrom'];
        $educationHistory->date_to = $request->form['dateTo'];
        $educationHistory->summary = $request->form['summary'];
        $educationHistory->name = $request->form['educationName'];

        $educationHistory->save();

        return [
            'success' => true,
            
        ];

    }

    public function editEducationHistory(Request $request){
        // dd($request);
        $educationHistory = UserCandidateEducationHistory::whereId($request->historyId)->first();
        $educationHistory->user_candidate_id = Auth::user()->userCandidate->id;
        $educationHistory->education_type_id = $request->form['educationType_id'];
     
        $educationHistory->title = $request->form['title'];
        $educationHistory->date_from = $request->form['dateFrom'];
        $educationHistory->date_to = $request->form['dateTo'];
        $educationHistory->summary = $request->form['summary'];
        $educationHistory->name = $request->form['educationName'];

        $educationHistory->save();

        return [
            'success' => true,
            
        ];

    }

    public function removeEducationHistory($historyId){
        $educationHistory = UserCandidateEducationHistory::whereId($historyId)->delete();
        

        return [
            'success' => true,
            
        ];
    }


}
