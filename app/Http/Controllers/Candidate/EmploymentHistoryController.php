<?php

namespace App\Http\Controllers\Candidate;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


use App\Models\EmploymentType;
use App\Models\UserCandidateEmploymentHistory;
use App\Models\UserCandidate;

use Auth;

class EmploymentHistoryController extends Controller
{
    public function index()
    {
     
        return view('website.candidate-portal.employment-history');
    }

    public function getDetail(){
        
        $employmentHistories = UserCandidateEmploymentHistory::whereUserCandidateId(Auth::user()->userCandidate->id)->orderBy('date_from')->get();
        $employmentTypes = EmploymentType::all();
        $userCandidate = UserCandidate::whereUserId(Auth::user()->id)->first();
        return [
            'success' => true,
            'employmentHistories' => $employmentHistories,
            'employmentTypes' => $employmentTypes,
            'userCandidate' => $userCandidate,
        ];
    }

    public function addEmploymentHistory(Request $request){
        $employmentHistory = new UserCandidateEmploymentHistory;
        // dd($request);
        $employmentHistory->user_candidate_id = Auth::user()->userCandidate->id;
        $employmentHistory->employment_type_id = $request->form['employmentType_id'];
        $employmentHistory->city = $request->form['city'];
        $employmentHistory->title = $request->form['title'];
        $employmentHistory->date_from = $request->form['dateFrom'];
        $employmentHistory->date_to = $request->form['dateTo'];
        $employmentHistory->summary = $request->form['summary'];
        $employmentHistory->name = $request->form['companyName'];

        $employmentHistory->save();

        return [
            'success' => true,
            
        ];

    }


    public function editEmploymentHistory(Request $request){
        // dd($request->all());
        $employmentHistory = UserCandidateEmploymentHistory::whereId($request->historyId)->first();
        $employmentHistory->user_candidate_id = Auth::user()->userCandidate->id;
        $employmentHistory->employment_type_id = $request->form['employmentType_id'];
        $employmentHistory->city = $request->form['city'];
        $employmentHistory->title = $request->form['title'];
        $employmentHistory->date_from = $request->form['dateFrom'];
        $employmentHistory->date_to = $request->form['dateTo'];
        $employmentHistory->summary = $request->form['summary'];
        $employmentHistory->name = $request->form['companyName'];

        $employmentHistory->save();

        return [
            'success' => true,
            
        ];

    }

    public function removeEmploymentHistory($historyId){
        $employmentHistory = UserCandidateEmploymentHistory::whereId($historyId)->delete();
        

        return [
            'success' => true,
            
        ];
    }
}
