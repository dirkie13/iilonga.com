<?php

namespace App\Http\Controllers\Candidate;

use App\Http\Controllers\Controller;
use App\Models\JobCategory;
use Illuminate\Http\Request;

use App\Models\UserCandidateSkill;
// use App\Models\ProfessionCategoryUserCandidate;
use App\Models\JobCategoryUserCandidate;
use App\Models\UserCandidate;
use Auth;

class ProfessionalDetailController extends Controller
{
    public function index()
    {
     
        return view('website.candidate-portal.professional-details');
    }


    public function removeSkill ($id){
        $skill = UserCandidateSkill::whereId($id)->delete();
        // $skill->delete();
		
		$response = array();
		
		$response['success'] = true;
		
		return $response;
    }


    public function addSkill (Request $request){
        // dd($request->all());
        $skill = new UserCandidateSkill;
        $skill->user_candidate_id = Auth::user()->userCandidate->id;
        $skill->name = $request->skillName;
        $skill->save();

        return [
            'success' => true,
            
        ];
    }
    public function editSkill (Request $request){
        // dd($request->all());
        $skill = UserCandidateSkill::whereId($request->editSkillId)->first();
        // $skill->user_candidate_id = Auth::user()->userCandidate->id;
        $skill->name = $request->skillName;
        $skill->save();

        return [
            'success' => true,
            
        ];
    }

    public function removeCategory ($id){
        // dd($id);
        $category = JobCategoryUserCandidate::whereId($id)->delete();
        // $skill->delete();
		
		$response = array();
		
		$response['success'] = true;
		
		return $response;
    }


    public function addCategory (Request $request){
        // dd($request->all());
        $category = new JobCategoryUserCandidate;
        $category->user_candidate_id = Auth::user()->userCandidate->id;
        $category->job_category_id = $request->ProfessionCategoryId;
        $category->years_of_experience = $request->YearsOfExperience;
        $category->save();

        return [
            'success' => true,
            
        ];
    }


    public function editCategory(Request $request){
        // dd($request->all());
        $category = JobCategoryUserCandidate::whereId($request->ProfessionPivotId)->first();
        // $skill->user_candidate_id = Auth::user()->userCandidate->id;
        // dd($category);
        $category->job_category_id = $request->ProfessionCategoryId;
        $category->years_of_experience = $request->YearsOfExperience;
        $category->save();

        return [
            'success' => true,
            
        ];
    }

    public function updateProfessionalTitle(Request $request){
        $userCandidate = UserCandidate::whereUserId(Auth::user()->id)->first();
        $userCandidate->professional_title = $request->professionalTitle;
        $userCandidate->save();

        return [
            'success' => true,
           
        ];
    }
    

    public function getDetail(){
        // dd(Auth::user()->userCandidate->id);
        $skills = UserCandidateSkill::whereUserCandidateId(Auth::user()->userCandidate->id)->get();
        // $categories = UserCandidateSkill::whereUserId(Auth::user()->id)->get();
        // $professionCategoryOptions = JobCategory::all();
        $professionCategoryOptions = JobCategory::whereNull('parent_id')->with('children')->get();
        // $professionCategories = ProfessionCategoryUserCandidate::whereUserCandidateId(Auth::user()->userCandidate->id)->with('professionCategory')->get();
        $userCandidate = UserCandidate::whereUserId(Auth::user()->id)->with('jobCategories')->first();
        // dd($userCandidate->professionCategories);
        return [
            'success' => true,
            'skills' => $skills,
            'professionCategoryOptions' => $professionCategoryOptions,
            'professionCategories' => $userCandidate->jobCategories,
            'professionalTitle' => $userCandidate->professional_title,
            'userCandidate' => $userCandidate,
        ];
    }
}
