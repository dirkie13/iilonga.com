<?php

namespace App\Http\Controllers\Candidate;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\User;
use App\Models\UserCandidate;
use App\Models\Country;
use App\Models\City;
use App\Models\Ethnicity;
use App\Models\CityUser;
use App\Models\EmploymentType;

use Auth;
use CloudinaryLabs\CloudinaryLaravel\Facades\Cloudinary;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $user = User::whereId(Auth::user()->id)->first();

        $userCandidate = UserCandidate::whereUserId($user->id)->first();
        $countries = Country::all();
        $cities = City::whereCountryId(152)->get();
        $ethnicities = Ethnicity::all();
        $cities_of_employment =  CityUser::whereUserId($user->id)->get();
        $employment_types = EmploymentType::all();
        // dd($cities_of_employment);

        $cities_of_employment_array = array();

        foreach ($cities_of_employment as $record) {
            array_push($cities_of_employment_array, $record->city_id);
        }

        // dd($cities_of_employment_array);




        return view('website.candidate-portal.profile')->with('user', $user)->with('userCandidate', $userCandidate)->with('countries', $countries)->with('cities', $cities)->with('ethnicities', $ethnicities)->with('cities_of_employment_array', $cities_of_employment_array)->with('employment_types', $employment_types);
    }

    public function toggleActivate()
    {
        // dd('hi');
        $userCandidate = UserCandidate::whereUserId(Auth::user()->id)->first();
        if(($userCandidate->cv == null || $userCandidate->cv == '')&&  $userCandidate->active == false){
            return back()->with('activate', 'A CV is required');
        }

        if ($userCandidate->active == true) {
            $userCandidate->active = false;
        } else {
            $userCandidate->active = true;
        }


        $userCandidate->save();

        return redirect()->route('candidate.profile');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }


    public function imageUpload(Request $request)
    {
        $request->validate(
            [

                'image'                 => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:10240',


            ]
        );

        $path = 'website/profileImage/';

        // $uploadedFileUrl = Cloudinary::upload($request->file('image')->getRealPath())->getSecurePath();
        $uploadedFileUrl = $request->image->storeOnCloudinary($path);

        // dd($uploadedFileUrl->getPublicId());
        $userCandidate = UserCandidate::whereId(Auth::user()->userCandidate->id)->first();
        $userCandidate->profile_image = $uploadedFileUrl->getPublicId();
        $userCandidate->save();

        // env('CLOUDINARY_WEBSITE_URL');
        return back()->with("success", "Success! Image Updated.");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = User::whereId(Auth::user()->id)->first();

        // dd($request->all());

        $request->validate(
            [


                'name'                  =>      'required|string|max:255',
                'surname'               =>      'required|string|max:255',
                'date_of_birth'         =>      'required',


                // 'email'              =>      "required|email|unique:users,email,$user->id",
                'cellphone'             =>      'required|numeric|min:9',
                // 'website'           =>          '',
                'city'                  =>          'required|string|max:255',
                'ethnicity'             =>      'required',
                'cities_of_employment'  =>   'required|array|min:1',
                'about_me'              =>   'required|string',
                'employment_type'       =>  'required',

            ]
        );




        $user = User::whereId(Auth::user()->id)->first();
        $userCandidate = UserCandidate::whereUserId(Auth::user()->id)->first();


        $user->name = $request->name;
        // $user->email = $request->email;
        $user->cellphone = $request->cellphone;
        $user->save();

        $userCandidate->surname = $request->surname;
        $userCandidate->date_of_birth = $request->date_of_birth;
        $userCandidate->city = $request->city;
        $userCandidate->nationality_id = $request->nationality_id;
        $userCandidate->ethnicity_id = $request->ethnicity;
        $userCandidate->employment_type_id = $request->employment_type;
        $userCandidate->city = $request->city;
        $userCandidate->about_me = $request->about_me;
        $userCandidate->save();

        // foreach($request->cities_of_employment as $city){
        //     // dd($category);
        //     $cityUser = new CityUser;
        //     $cityUser->user_id = $user->id;
        //     $cityUser->city_id = $city;
        //     $cityUser->save();
        // }

        $user->cities()->sync($request->cities_of_employment);


        if($userCandidate->active == true){

            return back()->with("success", "Success! Profile Updated.");
        }else{
            return redirect('/candidate/professional_details');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
