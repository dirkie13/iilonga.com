<?php

namespace App\Http\Controllers\Candidate;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\User;
use App\Models\UserCandidate;
use Illuminate\Support\Facades\Hash;

use Auth;

use App\Helpers\SendEmailHelper;

class RegistrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('website.candidate-portal.registration');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate(
            [
                'name'              =>      'required|string|max:255',
                'surname'              =>      'required|string|max:255',
                'cellphone'             =>      'required|numeric|min:9',
                'email'             =>      'required|email|unique:users,email',
                'password'          =>      [
                    'required',
                    'min:10',
                    function ($attribute, $value, $fail): void {

                        $lowerCaseMatch = preg_match('/[a-z]/', $value);    // must contain at least one lowercase letter
                        $upperCaseMatch = preg_match('/[A-Z]/', $value);    // must contain at least one uppercase letter
                        $digitMatch = preg_match('/[0-9]/', $value);        // must contain at least one digit

                        // $valid = ($lowerCaseMatch == true && $upperCaseMatch == true && $digitMatch == true) ;

                        if (!$lowerCaseMatch) {

                            $fail('The password must contain atleast one lower case character');
                        }
                        if (!$upperCaseMatch) {

                            $fail('The password must contain atleast one upper case character');
                        }
                        if (!$digitMatch) {

                            $fail('The password must contain atleast one number');
                        }
                    }
                ],
                'confirm_password'  =>      'required|same:password',
                'captcha'              =>      [
                    'required',
                    'int',
                    'max:100',
                    function ($attribute, $value, $fail) use ($request): void {
                        if (($request->sum1 + $request->sum2) != $request->captcha) {
                            //abort(405, 'The Captcha is incorrect');
                            $fail('Captcha is incorrect');
                        }
                    }
                ]

            ]
        );
        // dd($request->all());
        $dataArray      =       array(
            "name"          =>          $request->name,
            "email"         =>          $request->email,
            "cellphone"     =>          $request->cellphone,
            "role"          =>          'candidate',
            "password"      =>          Hash::make($request->password),
        );
        // dd($request->all());
        $user           =       User::create($dataArray);

        $emailMessage = "<p>Thank you for registering with Iilonga.com.</p>
        <p> To make it official, follow the link below and complete your profile to be a part of the Iilonga community.</p>
        <p><a href='https://Iilonga.com/login'>https://Iilonga.com/login</a> </p>";

        $sendEmail = new SendEmailHelper("Iilonga", "Iilonga", $emailMessage);

        $sendEmail->addEmail($user->email);
		
		$sendEmail->sendEmail(); 


        if (!is_null($user)) {
            $userCandidate = new UserCandidate;
            $userCandidate->user_id = $user->id;
            $userCandidate->surname = $request->surname;
            $userCandidate->save();
            // dd($request->all());
            Auth::login($user);
            return redirect('candidate/profile');
            return back()->with("success", "Success! Registration completed");
        } else {
            // return back()->with("failed", "Alert! Failed to register");
            // dd($request->all());

            return redirect()->back()->with("failed", "Alert! Failed to register");
        }


        // 
        // 

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
