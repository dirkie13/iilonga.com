<?php

namespace App\Http\Controllers\Candidate;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\UserCandidate;

use Auth;

use Carbon\Carbon;

use CloudinaryLabs\CloudinaryLaravel\Facades\Cloudinary;

class UploadCVController extends Controller
{
    public function index()
    {
        $userCandidate = UserCandidate::whereUserId(Auth::user()->id)->first();
        return view('website.candidate-portal.upload-cv')->with('userCandidate', $userCandidate);
    }

    public function getDetail(){
       
        $userCandidate = UserCandidate::whereId(Auth::user()->userCandidate->id)->first();
        $envCloudinary = env('CLOUDINARY_WEBSITE_URL');
     
 
        return [
            'success' => true,
  

            'userCandidate' => $userCandidate,
            'envCloudinary' => $envCloudinary,
        ];
    }

    public function cvUpload(Request $request){

        // dd($request->all());
        $request->validate(
            [

                'file'                 => 'required|mimes:pdf|max:10240',


            ]
        );

        $path = 'website/cv/';

        // $uploadedFileUrl = Cloudinary::upload($request->file('image')->getRealPath())->getSecurePath();
        $uploadedFileUrl = $request->file->storeOnCloudinary($path);

        // dd($uploadedFileUrl->getPublicId());
        $userCandidate = UserCandidate::whereId(Auth::user()->userCandidate->id)->first();
        $userCandidate->cv = $uploadedFileUrl->getPublicId();
        $userCandidate->cv_uploaded_date = Carbon::now();
        
        
        $userCandidate->save();

        // env('CLOUDINARY_WEBSITE_URL');
        return [
            'success' => true,
  
        ];
    }


    public function cvRemove(Request $request){

        

        

        $userCandidate = UserCandidate::whereId(Auth::user()->userCandidate->id)->first();
        $userCandidate->cv = null;
        $userCandidate->cv_uploaded_date = null;
        $userCandidate->active = false;
        $userCandidate->save();

        // env('CLOUDINARY_WEBSITE_URL');
        return [
            'success' => true,

        ];
    }

}
