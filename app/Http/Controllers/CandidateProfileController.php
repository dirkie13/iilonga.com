<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Credit;
use Auth;

class CandidateProfileController extends Controller
{
    public function index($id)
    {

        $user = User::whereHas('userCandidate', function ($query) use ($id) {
            $query->where('id', '=', $id);
        })->first();

        if(Auth::check() && Auth::user()->role == 'business'){
            $token = Credit::where('user_business_id', Auth::user()->userBusiness->id)->where('user_candidate_id', $id)
            ->first();

        }else{
            $token = false;
        }
        if ($token == false) {
            $bought = false;
            $user->name = '*******';
            $user->userCandidate->surname = '********';
            $user->userCandidate->surname = '********';

            $user->cellphone = '081 *** ****';
            $user->email = '******@******.***';
            // $user->userCandidate->created_at
            foreach ($user->userCandidate->educationHistories as $educationHistory) {
                $educationHistory->name = '******';
            }

            foreach ($user->userCandidate->employmentHistories as $employmentHistory) {
                $employmentHistory->name = '******';
            }

            $user->userCandidate->profile_image = '/v1/website/website/logo/iilongaCircle_evvmm2';
        } else {
            $bought = true;
        }

        if($token == true){
            $boughtDate = $token->date_used;

        }else{
            $boughtDate = null;
        }
        // dd($user);
        if(Auth::check()){
            if(Auth::user()->role == 'business'){
                return view('website.employer-portal.candidate-profile')->with('user', $user)->with('jobId', 0)->with('bought', $bought)->with('boughtDate', $boughtDate);
            }else{

                return view('website.candidate-profile')->with('user', $user)->with('bought', $bought);
            }

        }else{

            return view('website.candidate-profile')->with('user', $user)->with('bought', $bought);
        }
    }
}
