<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rules\MatchOldPassword;
use Illuminate\Support\Facades\Hash;
use App\User;

class ChangePasswordController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
   
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('auth/passwords/changePassword');
    } 
   
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function store(Request $request)
    {
        $request->validate([
            'current_password' => ['required', new MatchOldPassword],
            'new_password'          =>      [
                'required',
                'min:10',
                function ($attribute, $value, $fail): void {

                    $lowerCaseMatch = preg_match('/[a-z]/', $value);    // must contain at least one lowercase letter
                    $upperCaseMatch = preg_match('/[A-Z]/', $value);    // must contain at least one uppercase letter
                    $digitMatch = preg_match('/[0-9]/', $value);        // must contain at least one digit

                    // $valid = ($lowerCaseMatch == true && $upperCaseMatch == true && $digitMatch == true) ;

                    if (!$lowerCaseMatch) {

                        $fail('The password must contain atleast one lower case character');
                    }
                    if (!$upperCaseMatch) {

                        $fail('The password must contain atleast one upper case character');
                    }
                    if (!$digitMatch) {

                        $fail('The password must contain atleast one number');
                    }
                }
            ],
            
            'new_confirm_password' => ['same:new_password'],
        ]);
   
        User::find(auth()->user()->id)->update(['password'=> Hash::make($request->new_password)]);
   
        
    }
}
