<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Helpers\SendEmailHelper;

class ContactUsController extends Controller
{
    public function index()
    {
     
        return view('website.contact-us');
    }


    public function sendEmail(Request $request){
        // dd($request->all());

        $request->validate(
            [

                'captcha'              =>      [
                    'required',
                    'int',
                    'max:100',
                    function ($attribute, $value, $fail) use ($request): void {
                        if(($request->sum1 + $request->sum2) != $request->captcha ){
                            //abort(405, 'The Captcha is incorrect');
                            $fail('Captcha is incorrect');
                        }
                    }
                ]
                
            ]
        );

        if(($request->sum1 + $request->sum2) != $request->captcha ){
            //abort(405, 'The Captcha is incorrect');
            return back()->with('failed', 'Captcha is incorrect');
        }

        $sendEmail = new SendEmailHelper($request->subject, $request->subject, $request->message);

        // $sendEmail->addEmail('dirkie@pixel-penguin.com');
		$sendEmail->addEmail('info@iilonga.com');
		// $sendEmail->addEmail('');
		$sendEmail->sendEmail();

        return redirect('/contact-us')->with('success', 'Success! Email sent.');
    }
}
