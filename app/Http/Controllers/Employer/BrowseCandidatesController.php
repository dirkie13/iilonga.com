<?php

namespace App\Http\Controllers\Employer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\User;
use App\Models\EmploymentType;
use App\Models\JobCategory;
use App\Models\Credit;
use App\Models\YearsOfExperience;

use App\Models\Job;

use Auth;

class BrowseCandidatesController extends Controller
{
    public function index($jobId, Request $request)
    {
        // dd($request->all());
        $minimumExperience = $request->minimum_experience;
        $minimumExperienceAscOrDesc = $request->minimumExperienceAscOrDesc;
        $employmentType = $request->employment_type;

        $employmentTypes = EmploymentType::all();
        $jobCategories = JobCategory::all();
        $yearsOfExperiences = YearsOfExperience::all();

        $job = Job::whereId($jobId)->with('categories')->first();
        $jobCategoryIds = $job->categories->pluck('id');
        // dd($jobCategoryIds);

        $users = User::select('users.*', 'job_category_user_candidate.years_of_experience')
            ->leftJoin('user_candidates', 'user_candidates.user_id', 'users.id')
            ->leftJoin('job_category_user_candidate', 'job_category_user_candidate.user_candidate_id', 'user_candidates.id')
            ->leftJoin('job_categories', 'job_category_user_candidate.job_category_id', 'job_categories.id')
            ->leftJoin('job_job_category', 'job_categories.id', 'job_job_category.job_category_id')
            ->leftJoin('jobs', 'jobs.id', 'job_job_category.job_id')
            ->distinct('jobs.id')

            ->whereHas('userCandidate', function ($query)  {
                $query->where('active', true);
               
            })

            ->where('jobs.user_id', Auth::user()->id);
        // ->paginate(15);

        $users = $users->whereHas('userCandidate', function ($query) use ($jobCategoryIds) {
            $query->whereHas('jobCategories', function ($q) use ($jobCategoryIds) {
                $q->whereIn('job_categories.id', $jobCategoryIds);
                    
            });
        });

        if ($request->minimum_experience != null) {

            $users = $users->whereHas('userCandidate', function ($query) use ($jobCategoryIds, $minimumExperience) {
                $query->whereHas('jobCategories', function ($q) use ($jobCategoryIds, $minimumExperience) {
                    $q->whereIn('job_categories.id', $jobCategoryIds)
                        ->where('job_category_user_candidate.years_of_experience', '>=', $minimumExperience);
                });
            });
        }

        if ($minimumExperienceAscOrDesc != null) {
            $users = $users
                ->orderBy('years_of_experience', $minimumExperienceAscOrDesc);
        }

        if ($employmentType != null && $employmentType != 'all') {
            $users = $users->whereHas('userCandidate', function ($query) use ($employmentType) {
                $query->where('employment_type_id', $employmentType);
               
            });
        }

        //  elseif ($request->jobCategory != null) {
        //     $users = User::whereRole('candidate')->whereHas('userCandidate', function ($query) {
        //         $query->where('active', '=', true);
        //         // $query->where('jobCategories.id', '=', 1);

        //     })


        //         ->whereHas('userCandidate.jobCategories', function ($query) use ($request) {
        //             // $query->where('active', '=', true);
        //             // $query->whereIn('job_categories.parent.id', $request->jobCategory);
        //             $query->whereHas('parent', function ($query) use ($request) {
        //                 $query->whereIn('id', $request->jobCategory);
        //             });
        //         })->with('userCandidate.jobCategories')


        //         ->paginate(15);
        // } else {

        //     $users = User::whereRole('candidate')->whereHas('userCandidate', function ($query) {
        //         $query->where('active', '=', true);
        //         // $query->where('jobCategories.id', '=', 1);

        //     })


        //         ->whereHas('userCandidate.jobCategories', function ($query) use ($request) {
        //             // $query->where('active', '=', true);
        //             $query->whereIn('job_categories.id', []);
        //         })->with('userCandidate.jobCategories')


        //         ->paginate(15);
        // }





        $users = $users->paginate(15);

        // dd($users);


        foreach ($users as $user) {
            $token = Credit::where('user_business_id', Auth::user()->userBusiness->id)->where('user_candidate_id', $user->userCandidate->id)->first();

            if ($token == false) {

                // $user->userCandidate->created_at


                $user->profile_image = '/website/website/logo/iilonga_icon-01_lbwd5p';
            } else {
                $user->profile_image = $user->userCandidate->profile_image;
            }
            // dd($user);

        }

        // $users = User::whereRole('candidate')->whereHas('userCandidate', function ($query) {
        //     $query->where('active', '=', true);
        // })->paginate(15);
        // dd($user);



        return view('website.employer-portal.browse-candidates')->with('users', $users)->with('jobId', $jobId)->with('employmentTypes', $employmentTypes)->with('jobCategories', $jobCategories)->with('yearsOfExperiences', $yearsOfExperiences);
    }





    public function boughtCandidatesIndex($jobId)
    {
        // dd($employmentType);

        $employmentTypes = EmploymentType::all();
        $jobCategories = JobCategory::all();

        $userCandidateIds = Credit::where('user_business_id', Auth::user()->userBusiness->id)->whereJobId($jobId)->whereNotNull('user_candidate_id')->pluck('user_candidate_id');


        $users = User::whereRole('candidate')->whereHas('userCandidate', function ($query) use ($userCandidateIds) {
            $query->where('active', '=', true)
                ->whereIn('id', $userCandidateIds);
        })->paginate(15);

        // dd($user);
        return view('website.employer-portal.browse-candidates')->with('users', $users)->with('jobId', $jobId)->with('employmentTypes', $employmentTypes)->with('jobCategories', $jobCategories);
    }
}
