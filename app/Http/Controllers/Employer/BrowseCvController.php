<?php

namespace App\Http\Controllers\Employer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BrowseCvController extends Controller
{
    public function index($id)
    {
     
        return view('website.employer-portal.browse-cv');
    }
}
