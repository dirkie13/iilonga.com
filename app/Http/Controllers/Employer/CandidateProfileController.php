<?php

namespace App\Http\Controllers\Employer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\UserCandidate;
use App\Models\Credit;

use App\Models\User;

use Auth;

class CandidateProfileController extends Controller
{
    public function index($jobId, $id)
    {

        $user = User::whereHas('userCandidate', function ($query) use ($id) {
            $query->where('id', '=', $id);
        })->first();

        // dd($user->userCandidate->jobCategories);

        $token = Credit::where('user_business_id', Auth::user()->userBusiness->id)->where('user_candidate_id', $id)
            ->first();
        if ($token == false) {
            $bought = false;
            $user->name = '*******';
            $user->userCandidate->surname = '********';
            $user->userCandidate->surname = '********';

            $user->cellphone = '081 *** ****';
            $user->email = '******@****.***';
            // $user->userCandidate->created_at
            foreach ($user->userCandidate->educationHistories as $educationHistory) {
                $educationHistory->name = '******';
            }

            foreach ($user->userCandidate->employmentHistories as $employmentHistory) {
                $employmentHistory->name = '******';
            }
            $user->userCandidate->profile_image = '/v1/website/website/logo/iilongaCircle_evvmm2';
        } else {
            $bought = true;
        }
        if($token == true){
            $boughtDate = $token->date_used;

        }else{
            $boughtDate = null;
        }

        // dd($user);
        return view('website.employer-portal.candidate-profile')->with('user', $user)->with('jobId', $jobId)->with('bought', $bought)->with('boughtDate', $boughtDate);
    }

    public function buyCandidate($jobId, $id)
    {
        // dd('hi');
        // dd($id);


        $userCandidate = UserCandidate::whereId($id)->first();

        $token = Credit::where('user_business_id', Auth::user()->userBusiness->id)->where('user_candidate_id', $userCandidate->id)
            // ->where('date_used', '>', Carbon::now()->subDays(1)->toDateTimeString())
            ->first();

        if ($token == false) {
            // dd($id);
            $token = Credit::where('used', false)->whereNull('date_used')->where('user_business_id', Auth::user()->userBusiness->id)->first();
            if ($token == false) {
                return back()->with("noTokens", "Failed! You have no tokens.");
            }
            $token->user_candidate_id = $userCandidate->id;
            $token->job_id = $jobId;
            $token->used = true;
            $token->date_used = date('Y-m-d H:i:s');
            $token->save();
        }

        return back()->with("success", "Success! Candidate bought");
    }
}
