<?php

namespace App\Http\Controllers\Employer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Job;
use App\Models\Credit;
use App\Models\UserCandidate;
use App\Models\Result;
use App\Models\User;
use Auth;

use DB;

class ManageJobsController extends Controller
{
    public function index()
    {



        // dd($jobs);
        return view('website.employer-portal.manage-jobs');
        // ->with('jobs', $jobs)->with('results', $results);
    }

    public function jobDone(Request $request)
    {
        // dd($request);
        $job = Job::whereUserId(Auth::user()->id)->whereId($request->job_id)->with('categories')->first();
        $job->result_id = $request->result_id;
        $job->done = true;
        $job->save();

        return [
            'success'=> true,
        ];

    }

    public function getDetail(Request $request)
    {
        // $jobs = Job::whereUserId(Auth::user()->id)->with('categories')->get();


        $jobs = Job::select('jobs.*', 'users.name', DB::raw('COUNT(distinct users.id) AS potential_users'))
        ->groupBy('jobs.id')
        ->leftJoin('job_job_category', 'jobs.id', 'job_job_category.job_id')
        ->leftJoin('job_categories', 'job_job_category.job_category_id', 'job_categories.id')
        ->leftJoin('job_category_user_candidate', 'job_category_user_candidate.job_category_id', 'job_categories.id')
        ->leftJoin('user_candidates', 'job_category_user_candidate.user_candidate_id', 'user_candidates.id')
        //->leftJoin('users', 'users.id', 'user_candidates.user_id')

        ->leftJoin('users', function ($query) {
            $query->on('users.id', '=', 'user_candidates.user_id')
                ->where('user_candidates.active', true);
        })

        ->where('jobs.user_id', Auth::user()->id)

        // ->where('user_candidates.active', true)

        ->with('categories')
        ->OrderBy('jobs.created_at', 'Desc')
        ->get();


        
        foreach($jobs as $job){
            $jobCategoryIds = Job::whereUserId(Auth::user()->id)->whereId($job->id)->with('categories')->pluck('id');
            
        }
        

        
       
        foreach ($jobs as $job) {
            
            $job->cvObtained = Credit::where('user_business_id', Auth::user()->userBusiness->id)->whereJobId($job->id)->whereNotNull('user_candidate_id')->count();
            $job->potentialCandidates = UserCandidate::whereHas('jobCategories', function ($q) use ($jobCategoryIds) {
                // foreach($job->jobCategories()->count as $)
                // dd($q);
                $q->whereIn('job_categories.id', $jobCategoryIds);
            })->get();
        }


        $results = Result::all();

        return[
            'success' => true,
            'jobs' => $jobs,
            'results' => $results,
        ];
    }
}
