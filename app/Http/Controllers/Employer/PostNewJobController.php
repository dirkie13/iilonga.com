<?php

namespace App\Http\Controllers\Employer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\JobCategory;
use App\Models\Job;
use App\Models\User;
use App\Models\JobJobCategory;

use Auth;

class PostNewJobController extends Controller
{
    public function index()
    {


        $categories = JobCategory::whereNotNull('parent_id')->get();
        $mainCategories = JobCategory::whereNull('parent_id')->with('children')->get();


        // dd($mainCategories);
        return view('website.employer-portal.post-new-job')->with('jobCategories', $categories)->with('mainJobCategories', $mainCategories);
    }


    public function create(Request $request)
    {



        $request->validate(
            [
                'job_title'              =>      'required|string|max:255',
                'job_categories'              =>      'required|array|min:1',
                'job_type'              =>      'required|string|max:255',
                'city'           =>      'required|string|max:255',

            ]
        );

        $user = User::whereId(Auth::user()->id)->first();
        // dd($request->all());

        $job = new Job;
        $job->user_id = $user->id;
        $job->title = $request->job_title;
        $job->job_type = $request->job_type;
        $job->city = $request->city;

        $job->save();

        
        foreach($request->job_categories as $category){
            // dd($category);
            $jobjobCategory = new JobJobCategory;
            $jobjobCategory->job_id = $job->id;
            $jobjobCategory->job_category_id = $category;
            $jobjobCategory->save();
        }

        return redirect('/employer/manage-jobs')->with('success', 'Success! Job Added');
        // return back()->with("success", "Success! Job Added");
    }
}
