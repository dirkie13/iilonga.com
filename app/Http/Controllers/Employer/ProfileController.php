<?php

namespace App\Http\Controllers\Employer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\UserBusiness;
use App\Models\User;
use App\Models\Country;

use Auth;

class ProfileController extends Controller
{
    public function index()
    {
        $user = User::whereId(Auth::user()->id)->first();

        $userBusiness = UserBusiness::whereUserId($user->id)->first();
        $countries = Country::all();

        return view('website.employer-portal/profile')->with('user', $user)->with('userBusiness', $userBusiness)->with('countries', $countries);
    }

    public function update(Request $request)
    {
        // dd($request);
        $user = User::whereId(Auth::user()->id)->first();



        $request->validate(
            [

                // 'company_name'              =>     [
                //     'required',
                //     'string',
                //     function($attribute, $value, $fail): void{
                //         if($value == 'Dirkie'){
                //             $fail('This is a name, not a company');
                //         }
                //     }
                // ],
                'company_name'              =>      'required|string|max:255',
                'since'              =>      '',
                'team_size'              =>      'required|numeric',
                'business_type'           =>      'required|string|max:255',
                // 'email'             =>      "required|email|unique:users,email,$user->id",
                'cellphone'             =>      'required|numeric|min:9',
                // 'website'           =>          '',
                'city'           =>          'required|string|max:255',
                'address'           =>      'required|string|max:255'
            ]
        );




        $user = User::whereId(Auth::user()->id)->first();
        $userBusiness = UserBusiness::whereUserId(Auth::user()->id)->first();

        if ($userBusiness->business_type == null) {
            $shouldContinue = true;
        } else {
            $shouldContinue = false;
        }


        $user->name = $request->company_name;
        // $user->email = $request->email;
        $user->cellphone = $request->cellphone;
        $user->save();

        $userBusiness->since = $request->since;
        $userBusiness->team_size = $request->team_size;
        $userBusiness->business_type = $request->business_type;
        $userBusiness->website = $request->website;
        $userBusiness->country_id = $request->country_id;
        $userBusiness->city = $request->city;
        $userBusiness->street_address = $request->address;
        $userBusiness->save();


        if ($shouldContinue == false){
            return back()->with("success", "Success! Profile Updated.");

        }else{
            return redirect('/employer/post-new-job')->with('success', 'Success! Profile updated.');
        }
    }
}
