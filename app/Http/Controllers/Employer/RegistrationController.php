<?php

namespace App\Http\Controllers\Employer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\User;
use App\Models\UserBusiness;
use Illuminate\Support\Facades\Hash;
use Auth;

use App\Helpers\SendEmailHelper;

class RegistrationController extends Controller
{
    public function index()
    {

        return view('website.employer-portal.registration');
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate(
            [
                'company_name'              =>      'required|string|max:255',
                'email'             =>      'required|email|unique:users,email',
                'cellphone'             =>      'required|numeric|min:9',
                'password'          =>      [
                    'required',
                    'min:10',
                    function ($attribute, $value, $fail): void {

                        $lowerCaseMatch = preg_match('/[a-z]/', $value);    // must contain at least one lowercase letter
                        $upperCaseMatch = preg_match('/[A-Z]/', $value);    // must contain at least one uppercase letter
                        $digitMatch = preg_match('/[0-9]/', $value);        // must contain at least one digit

                        // $valid = ($lowerCaseMatch == true && $upperCaseMatch == true && $digitMatch == true) ;

                        if (!$lowerCaseMatch) {

                            $fail('The password must contain atleast one lower case character');
                        }
                        if (!$upperCaseMatch) {

                            $fail('The password must contain atleast one upper case character');
                        }
                        if (!$digitMatch) {

                            $fail('The password must contain atleast one number');
                        }
                    }
                ],
                'confirm_password'  =>      'required|same:password',
                'captcha'              =>      [
                    'required',
                    'int',
                    'max:100',
                    function ($attribute, $value, $fail) use ($request): void {
                        if(($request->sum1 + $request->sum2) != $request->captcha ){
                            //abort(405, 'The Captcha is incorrect');
                            $fail('Captcha is incorrect');
                        }
                    }
                ]
            ]
        );
        // dd($request->all());
        $dataArray      =       array(
            "name"          =>          $request->company_name,
            "email"         =>          $request->email,
            "cellphone"     =>          $request->cellphone,
            "role"          =>          'business',
            "password"      =>          Hash::make($request->password),
        );
        // dd($request->all());
        $user           =       User::create($dataArray);


        $emailMessage = "<p>Thank you for registering with Iilonga.com.</p>
        <p> To make it official, follow the link below and complete your profile to be a part of the Iilonga community.</p>
        <p><a href='https://Iilonga.com/login'>https://Iilonga.com/login</a> </p>";

        $sendEmail = new SendEmailHelper("Iilonga", "Iilonga", $emailMessage);

        $sendEmail->addEmail($user->email);
		
		$sendEmail->sendEmail(); 


        if (!is_null($user)) {
            $userBusiness = new UserBusiness;
            $userBusiness->user_id = $user->id;
            $userBusiness->save();
            // dd($request->all());
            Auth::login($user);
            return redirect('employer/profile');
            return back()->with("success", "Success! Registration completed");
        } else {
            // return back()->with("failed", "Alert! Failed to register");
            // dd($request->all());

            return redirect()->back()->with("failed", "Alert! Failed to register");
        }


        // 
        // 
    }
}
