<?php

namespace App\Http\Controllers\Employer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Credit;
use Auth;

class TransactionsController extends Controller
{
    public function index()
    {
        $tokens = Credit::where('user_business_id', Auth::user()->userBusiness->id)->where('used', true)->get();
        // dd($tokens[0]->userCandidate->user);

        return view('website.employer-portal.transactions')->with('tokens', $tokens);
    }
}
