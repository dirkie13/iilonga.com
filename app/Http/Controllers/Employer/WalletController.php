<?php

namespace App\Http\Controllers\Employer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Payment;
use App\Models\Credit;
// use App\Models\

use Ixudra\Curl\Facades\Curl;

use Auth;

class WalletController extends Controller
{
    public function index()
    {
        $tokenQuantity = $this->getTokenQuantity();
        return view('website.employer-portal.wallet')->with('tokenQuantity', $tokenQuantity['tokens']);
    }

    public function purchaseHistoryIndex()
    {
        return view('website.employer-portal.purchase-history');
    }

    public function purchaseTokensIndex()
    {
        return view('website.employer-portal.purchase-tokens');
    }


    public function pay($paymentId){
        
        $payment = Payment::whereId($paymentId)->first();
        
        return view('website.employer-portal.add-credit.pay', ['payment' => $payment]);
		
	}

  

    public function addPayment(Request $request)
    {
        // dd('hi');
        $input = $request->all();

        $tokens = $input['tokens'];

        $amount = $tokens * Credit::TOKEN_PRICE;


        $payment = new Payment();

        $payment->user_business_id = Auth::user()->userBusiness->id;
        $payment->tokens = $tokens;
        $payment->amount = $amount;
        $payment->paid = false;

        $payment->save();

        
        // return redirect('/employer/wallet');
        return [
            'success' => true,
            'data' => $payment
        ];
    }

    private function getTokenQuantity()
    {


        $tokens = Credit::where('user_business_id', Auth::user()->userBusiness->id)->where('used', false)->count();

        return [
            'success' => true,
            'tokens' => $tokens
        ];
    }

    public function addTokens($reference)
    {

        //$input = $request->all();

        $payment = Payment::where('id', $reference)->first();
        // dd( $payment);
        if ($payment->paid == false) {
            // dd('$toInsert[0]');
            $tokens = $payment->tokens;

            $toInsert = [];

            for ($x = 0; $x < (int)$tokens; $x++) {

                $newEntry =
                    $toInsert[] = [
                        'user_business_id' => $payment->user_business_id,
                    ];
            }

            // dd($toInsert);


            Credit::insert($toInsert);

            $payment->paid = true;
            $payment->save();
        }





        return true;
    }
    public function getTransactions()
    {
        $payments = Payment::wherePaid(true)->whereUserBusinessId(Auth::user()->userBusiness->id)->get();


        return[
            'success' => true,
            'transactions' => $payments,
        ];
    }



    public function confirmPayment($reference){
        
        return view('website.employer-portal.add-credit.confirm-payment', ['reference' => $reference]);
        
    }
    
    public function checkPayment($reference){

        

        $response = Curl::to('https://paytoday.com.na/transactions/txstatus/'.env('PAYTODAY_BUSINESS_ID').'/IilongaPayment'.$reference.'.json')
        ->asJson()
        ->get();
        

        if($response->status == true){
            $this->addTokens($reference);
        }

        return [
            'success' => true,
        ];
        //echo $response->status;

        //dd($response);

    }



}
