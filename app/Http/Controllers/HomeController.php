<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use App\Models\UserBusiness;
use App\Models\User;

class HomeController extends Controller
{


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }


    public function login(Request $request)
    {
        $request->validate(
            [

                'email'             =>      'required',

                'password'          =>      'required',

            ]
        );

        // dd($request);
        if (Auth::attempt(['username' => $request->username, 'password' => $request->password])) {

            echo "success with username!";
        } elseif (Auth::attempt(['email' => $request->username, 'password' => $request->password])) {

            echo "success with email!";
        } else {
            echo "fail!";
        }

        



        // if (!is_null($user)) {
        //     $userBusiness = new UserBusiness;
        //     $userBusiness->user_id = $user->id;
        //     $userBusiness->save();
        //     // dd($request->all());
        //     Auth::login($user);
        //     return redirect('employer/profile');
        //     return back()->with("success", "Success! Registration completed");
        // } else {
        //     // return back()->with("failed", "Alert! Failed to register");
        //     // dd($request->all());

        //     return redirect()->back()->with("failed", "Alert! Failed to register");
        // }
    }
}
