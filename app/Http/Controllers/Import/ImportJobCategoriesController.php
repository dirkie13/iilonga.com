<?php

namespace App\Http\Controllers\Import;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Imports\JobCategoriesImport;
use App\Models\JobCategory;

use Excel;

class ImportJobCategoriesController extends Controller
{

    private $countMain = 0;
    private $countSub = 0;

    public function index(Request $request){

        $array = Excel::toCollection(new JobCategoriesImport, request()->file('file_name'));


        $array = $array[0];
        
        unset($array[0]);

        foreach($array as $item){

            $jobCategory = $this->jobCategoryStart($item[0]);

            if($jobCategory != null){
                $jobCategorySub = $this->jobCategorySub($item[1], $jobCategory);    
            }
        }

        echo 'done';
    }

    private function jobCategoryStart($name){

        $this->countMain++;

        $item = JobCategory::where('name', $name)->first();

        if($item == null){

            $item = new JobCategory();
            $item->name = $name;
            $item->column_order = $this->countMain;
            
        }
        else{
            $item->column_order = $this->countMain;
        }
        
        $item->parent_id = NULL;
        $item->save();    


        return $item;

    }

    private function jobCategorySub($name, $jobCategory){

        $this->countSub++;

        $item = JobCategory::where('name', $name)->first();

        if($item == null){

            $item = new JobCategory();
            $item->parent_id = $jobCategory->id;
            $item->name = $name;
            $item->column_order = $this->countSub;
            $item->save();    
            
        }
        else{
            $item->parent_id = $jobCategory->id;
            $item->column_order = $this->countSub;
            $item->save();    
        }


        return $item;

    }
}
