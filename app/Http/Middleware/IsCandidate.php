<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;

use Closure;

class IsCandidate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check())
        {
            if(Auth::user()->role  == 'candidate')
            {
                return $next($request);
            }
            else
            {
                return redirect('/login');
            }

        }
        else
        {
            return redirect('/login');
        }


    }
}
