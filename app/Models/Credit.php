<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Credit extends Model
{
    use HasFactory;

    const TOKEN_PRICE = 100;

    public function userCandidate(){

       

        return $this->belongsTo(UserCandidate::class);
    }
}
