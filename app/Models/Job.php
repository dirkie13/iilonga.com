<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\JobCategory;

class Job extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'title',
        'job_type',
        'city',

    ];

    public function categories()
    {
        return $this->belongsToMany(JobCategory::class, 'job_job_category');
    }
}
