<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobCategory extends Model
{
    use HasFactory;

    public function parent()
    {
        return $this->belongsTo('App\Models\JobCategory', 'parent_id')->with('parent');
    }
    public function children()
    {
        // recursively return all children
        return $this->hasMany('App\Models\JobCategory', 'parent_id')->with('children');
    }
}
