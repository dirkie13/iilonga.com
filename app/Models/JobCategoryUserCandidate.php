<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\Pivot;

class JobCategoryUserCandidate extends Pivot
{
    use HasFactory;

    public function jobCategory()
    {
        return $this->belongsTo(JobCategory::class);
    }
}
