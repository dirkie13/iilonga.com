<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\Pivot;

class ProfessionCategoryUserCandidate extends Pivot
{
    use HasFactory;

    public function professionCategory()
    {
        return $this->belongsTo(ProfessionCategory::class);
    }
}
