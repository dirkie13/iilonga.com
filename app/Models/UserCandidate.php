<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\UserCandidateSkill;

class UserCandidate extends Model
{
    use HasFactory;

    public function skills()
    {
        return $this->hasMany(UserCandidateSkill::class);
    }


    public function jobCategories()
    {
        return $this->belongsToMany(JobCategory::class, 'job_category_user_candidate')->with('parent')->withPivot('id', 'years_of_experience');
    }

    public function educationHistories()
    {
        return $this->hasMany(UserCandidateEducationHistory::class);
    }

    public function employmentHistories()
    {
        return $this->hasMany(UserCandidateEmploymentHistory::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function ethnicity()
    {
        return $this->belongsTo(Ethnicity::class);
    }
}
