<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserCandidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_candidates', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            
            $table->string('surname')->nullable();
            $table->string('profile_image')->nullable();
            $table->date('date_of_birth')->nullable();
            
            $table->string('city')->nullable();

            $table->foreignId('nationality_id')->nullable();
            $table->foreignId('ethnicity_id')->nullable();
            
            $table->text('about_me')->nullable();

            $table->string('professional_title')->nullable();
            $table->string('cv')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_candidates');
    }
}
