<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserBusinessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_businesses', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            // $table->string('company_name')->nullable();
            $table->date('since')->nullable();

            $table->integer('team_size')->unsigned()->nullable();
            
            $table->string('business_type')->nullable();
            $table->string('website')->nullable();
            $table->foreignId('country_id')->nullable();
            $table->string('city')->nullable();
            $table->text('street_address')->nullable();


            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_businesses');
    }
}
