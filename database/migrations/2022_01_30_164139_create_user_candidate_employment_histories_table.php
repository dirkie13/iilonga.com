<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserCandidateEmploymentHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_candidate_employment_histories', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_candidate_id');
            $table->foreignId('employment_type_id');
            $table->string('name');
            $table->string('city');
            $table->string('title');
            $table->date('date_from');
            $table->date('date_to');
            $table->text('summary')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_candidate_employment_histories');
    }
}
