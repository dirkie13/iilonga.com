<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeUserCandidateSkillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_candidate_skills', function (Blueprint $table) {
            $table->renameColumn('user_id', 'user_candidate_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_candidate_skills', function (Blueprint $table) {
            $table->renameColumn('user_candidate_id', 'user_id');
        });
    }
}
