<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCvUploadedToUserCandidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_candidates', function (Blueprint $table) {
            $table->date('cv_uploaded_date')->after('cv')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_candidates', function (Blueprint $table) {
            $table->dropColumn('cv_uploaded_date');
        });
    }
}
