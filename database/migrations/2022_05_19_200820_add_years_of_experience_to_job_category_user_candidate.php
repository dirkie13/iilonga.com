<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddYearsOfExperienceToJobCategoryUserCandidate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('job_category_user_candidate', function (Blueprint $table) {
            $table->double('years_of_experience')->after('user_candidate_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('job_category_user_candidate', function (Blueprint $table) {
            $table->dropColumn('years_of_experience');
        });
    }
}
