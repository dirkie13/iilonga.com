<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEmploymentTypeToUserCandidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_candidates', function (Blueprint $table) {
            $table->foreignId('employment_type_id')->after('ethnicity_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_candidates', function (Blueprint $table) {
            $table->dropColumn('employment_type_id');
        });
    }
}
