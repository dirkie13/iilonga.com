/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue').default;
import VueSweetalert2 from 'vue-sweetalert2'; 
Vue.use(VueSweetalert2);
import 'sweetalert2/dist/sweetalert2.min.css';

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('professional-detail-list', require('./components/candidate-admin/professionaldetails/index.vue').default);
Vue.component('employment-history', require('./components/candidate-admin/employmenthistory/index.vue').default);
Vue.component('education-history', require('./components/candidate-admin/educationhistory/index.vue').default);
Vue.component('purchase-history', require('./components/business-admin/purchasehistory/index.vue').default);
Vue.component('purchase-tokens', require('./components/business-admin/purchasetokens/index.vue').default);
Vue.component('confirm-payment', require('./components/business-admin/purchasetokens/confirmpayment.vue').default);

Vue.component('manage-jobs', require('./components/business-admin/manage-jobs/index.vue').default);

Vue.component('upload-cv', require('./components/candidate-admin/uploadcv/index.vue').default);





/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});
