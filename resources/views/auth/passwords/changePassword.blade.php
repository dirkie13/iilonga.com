@extends('layouts.website')

@section('content')
    <div style="margin-top: 191px; margin-bottom: 57px" class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                {{-- <div style="text-align: -webkit-center;" class="res-logo mb-3"><a href="/" title=""><img style="width: 500px"
                            src="{{ env('CLOUDINARY_WEBSITE_URL') }}c_scale,w_500/v1/website/website/logo/iilonga-01_lwnogx_liwaic.png"
                            alt="" /></a></div> --}}
                <div class="card">
                    <div class="card-header">Change Password</div>
                    <div class="card-body">
                        
                        <form method="POST" action="{{ route('change.password') }}">
                            @csrf 
   
                         @foreach ($errors->all() as $error)
                            {{-- <p class="text-danger">{{ $error }}</p> --}}
                            <div class="alert alert-danger" role="alert">
                                {{ $error }}
                            </div>
                         @endforeach 
  
                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Current Password</label>
  
                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="current_password" autocomplete="current-password">
                            </div>
                        </div>
  
                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">New Password</label>
  
                            <div class="col-md-6">
                                <input id="new_password" minlength="10" type="password" class="form-control" name="new_password" autocomplete="current-password">
                            </div>
                        </div>
  
                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">New Confirm Password</label>
    
                            <div class="col-md-6">
                                <input id="new_confirm_password" minlength="10" type="password" class="form-control" name="new_confirm_password" autocomplete="current-password">
                            </div>
                        </div>
   
                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Update Password
                                </button>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('style')
    <style>
        .form-check label::before {
            content: " ";
            border: 2px solid #e6e7ef;

            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            -ms-border-radius: 3px;
            -o-border-radius: 3px;
            border-radius: 3px;
        }
    </style>
@endsection