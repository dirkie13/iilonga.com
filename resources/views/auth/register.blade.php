@extends('layouts.website')

@section('content')
<div style="margin-top: 191px; margin-bottom: 57px" class="container">
    <div class="row justify-content-center">
        <div class="col-md-8" style="color: black !important">
            <div class="card">

                <div class="block double-gap-top double-gap-bottom">
                    <div data-velocity="-.1" style="" class="parallax scrolly-invisible layer color"></div>
                    <!-- PARALLAX BACKGROUND IMAGE -->
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div style="border:1px solid #979797; border-radius: 5px; padding: 10px; color:#979797 !important" class="simple-text-block">
                                    <h3 style="" class="help-text-heading">Are you a business in need of new candidates?</h3>
        
                                    <span  class="help-text-text">Sign up as employer today to find suitable candidates for your vacancies</span>
                                    <a class="red-button-border-filled" style=""  href="/employer/registration" title="">Sign up as
                                        employer</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block double-gap-top double-gap-bottom">
                    <div data-velocity="-.1" style="" class="parallax scrolly-invisible layer color"></div>
                    <!-- PARALLAX BACKGROUND IMAGE -->
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div style="border:1px solid #979797; border-radius: 5px; padding: 10px" class="simple-text-block">
                                    <h3 class="help-text-heading">Are you in need of a job?</h3>
        
                                    <span class="help-text-text">Upload your CV today</span>
                                    <a style="" class="red-button-border-filled" href="/candidate/registration" title="">Sign up as
                                        candidate</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div> --}}
            </div>
        </div>
    </div>
</div>
@endsection

@section('style')
    <style>
        /* .modal { background: rgba(000, 000, 000, 0.8); min-height:1000000px; } */
        .help-text-heading {
            color:#979797 !important
        }

        .help-text-text {
            color:#979797 !important
        }

        

    </style>
@endsection