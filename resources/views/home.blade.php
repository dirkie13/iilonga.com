@extends('layouts.website')

@section('content')
    
    <section>
        <div class="block no-padding overlape">
            <div class="container fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="main-featured-sec style3">
                            <ul class="main-slider-sec style3 text-arrows">
                                <li><img src="https://res.cloudinary.com/pixel-penguin/image/upload/c_limit,e_fill_light:0,h_675,w_1920/v1639471540/websites/iilonga/slider_image/businessman-checking-time-from-watch-1_ziwzos.jpg" alt="slider_image" /></li>
                                {{-- <li><img src="http://placehold.it/1920x675" alt="" /></li>
							<li><img src="http://placehold.it/1920x675" alt="" /></li> --}}
                            </ul>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    

    <section>
        <div class="block double-gap-top double-gap-bottom">
            <div data-velocity="-.1" style="" class="parallax scrolly-invisible layer color"></div>
            <!-- PARALLAX BACKGROUND IMAGE -->
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div style="border:1px solid #979797; border-radius: 5px; padding: 10px" class="simple-text-block">
                            <h3 class="help-text-heading">Are you a business in need of new candidates?</h3>

                            <span class="help-text-text">Sign up as employer today to find suitable candidates for your vacancies</span>
							<div><a class="red-button-border-filled read-more-button" style=""  href="/about-us" title="">Read more</a></div>

                            <a class="red-button-border-filled" style=""  href="/employer/registration" title="">Sign up as
                                employer</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="block double-gap-top double-gap-bottom">
            <div data-velocity="-.1" style="" class="parallax scrolly-invisible layer color"></div>
            <!-- PARALLAX BACKGROUND IMAGE -->
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div style="border:1px solid #979797; border-radius: 5px; padding: 10px" class="simple-text-block">
                            <h3 class="help-text-heading">Are you in need of a job?</h3>

                            <span class="help-text-text">Upload your CV today</span>
							<div><a class="red-button-border-filled read-more-button" style=""  href="/about-us" title="">Read more</a></div>
                            <a style="" class="red-button-border-filled" href="/candidate/registration" title="">Sign up as
                                candidate</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    

@endsection
@section('scripts')

@endsection

@section('style')
    <style>
        .help-text-heading {
            color: #707070 !important;
            font-size: 21px;
            font-weight: 600;
        }

        .help-text-text {
            color: #707070 !important;
            font-size: 21px
        }

        

        /* a:hover,
        a:focus {
            color: #FF8585 !important;
            background-color: #723c3c !important;
        } */

    </style>
@endsection
