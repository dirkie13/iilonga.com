<section class="overlape">
            <div class="block no-padding">
                <div data-velocity="-.1"
                    style="background: url({{ env('CLOUDINARY_WEBSITE_URL') }}c_fill,w_1600,h_800/website/website/images/businessman-checking-time-from-watch-1_ziwzos_ufkhwh.jpg) repeat scroll 50% 422.28px transparent;"
                    class="parallax scrolly-invisible no-parallax"></div><!-- PARALLAX BACKGROUND IMAGE -->
                <div class="container fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="inner-header">
                                <h3>Welcome {{ Auth::user()->name }} {{ Auth::user()->userCandidate->surname }}</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


  