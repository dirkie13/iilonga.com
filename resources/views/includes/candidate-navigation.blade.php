<aside class="col-lg-3 column border-right">
    <div class="widget">
        <div class="tree_widget-sec">
            <ul>

                <li><a @if (Request::path() == 'candidate/profile') class="active-navigation" @endif href="profile"
                        title=""><i></i>MY PROFILE</a></li>
                <li><a @if (Request::path() == 'candidate/professional_details') class="active-navigation" @endif href="professional_details"
                        title=""><i></i>PROFESSION DETAILS</a>
                </li>
                <li><a @if (Request::path() == 'candidate/employment_history') class="active-navigation" @endif href="employment_history"
                        title=""><i></i>EMPLOYMENT HISTORY</a>
                </li>
                <li><a @if (Request::path() == 'candidate/education_history') class="active-navigation" @endif href="education_history"
                        title=""><i></i>EDUCATION</a></li>
                <li><a @if (Request::path() == 'candidate/upload_cv') class="active-navigation" @endif href="upload_cv"
                        title=""><i></i>UPLOAD CV</a></li>


                {{-- <li style="margin-bottom: 20px; margin-top: 10px">
                    <form id="logout-form" action="{{ url('logout') }}" method="POST">
                        {{ csrf_field() }}
                        <button class="logout" style="" class="" type="submit"><i
                                class="fa fa-sign-out" aria-hidden="true"></i>LOGOUT</button>
                    </form>
                </li> --}}


                {{-- <li><a href="/" title=""><i ></i>LOGOUT</a></li> --}}

                @if (Auth::user()->userCandidate->active == true)
                    <li style="width: 220px;" class="btn btn-success"><a style="color: white"
                            href="profile/toggle-activate" title=""><i class="fa fa-check-circle fa-5x"
                                aria-hidden="true"></i>Click to Disable profile!</a></li>
                @else
                    <li style="width: 220px" class="btn btn-danger"><a style="color: white"
                            href="profile/toggle-activate" title=""><i class="fa fa-times-circle"
                                aria-hidden="true"></i>Click to Enable profile!</a></li>
                @endif
                {{-- <li><a href="#" title=""><i class="la la-unlink"></i>Logout</a></li> --}}



                {{-- @if ($errors->any())
                    <li style="text-align-last: center;" class="mt-3">

                        
                        <div style="" class="alert alert-danger ">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        

                    </li>
                @endif --}}
                @if (\Session::has('activate'))
                    <li style="text-align-last: center;" class="mt-3">

                        {{-- @foreach ($errors->all() as $error) --}}
                        <div style="" class="alert alert-danger ">
                            <ul>
                                {{-- @foreach ($errors->all() as $error) --}}
                                    <li>{!! \Session::get('activate') !!}</li>
                                {{-- @endforeach --}}
                            </ul>
                        </div>
                        {{-- @endforeach --}}

                    </li>
                @endif

            </ul>
        </div>
    </div>

</aside>
