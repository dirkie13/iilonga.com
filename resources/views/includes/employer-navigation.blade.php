<aside class="col-lg-3 column border-right">
    <div class="widget">
        <div class="tree_widget-sec">
            <ul>
                <li><a @if (Request::path() == 'employer/profile') class="active-navigation" @endif href="/employer/profile"
                        title=""><i></i>COMPANY PROFILE</a></li>
                @if (Auth::user()->userBusiness->business_type != null)
                    <li><a @if (Request::path() == 'employer/post-new-job') class="active-navigation" @endif
                            href="/employer/post-new-job" title=""><i></i>ADD A NEW JOB</a></li>
                @else
                    <li class="grayed-out"><a><i></i>POST A NEW JOB</a></li>
                @endif
                @if (Auth::user()->userBusiness->business_type != null)
                    <li><a @if (Request::path() == 'employer/manage-jobs') class="active-navigation" @endif
                            href="/employer/manage-jobs" title=""><i></i>MANAGE JOBS</a>
                    </li>
                @else
                    <li class="grayed-out"><a><i></i>MANAGE JOBS</a></li>
                @endif
                @if (Auth::user()->userBusiness->business_type != null)
                    <li><a @if (Request::path() == 'employer/wallet') class="active-navigation" @endif href="/employer/wallet"
                            title=""><i></i>WALLET</a>
                    </li>
                @else
                    <li class="grayed-out"><a><i></i>WALLET</a></li>
                @endif
                @if (Auth::user()->userBusiness->business_type != null)
                    <li><a @if (Request::path() == 'employer/transactions') class="active-navigation" @endif
                            href="/employer/transactions" title=""><i></i>TRANSACTIONS</a></li>
                @else
                    <li class="grayed-out"><a><i></i>TRANSACTIONS</a></li>
                @endif


                <li ><a  href="/browse-cv?allCategories=on"><i></i>BROWSE ALL CV's</a></li>

                

                {{-- <li>

                    <form id="" action="{{ route('logout') }}" method="POST" class="">
                        @csrf
                        <a title=""><i></i>LOGOUT
                        </a>
                    </form>
                </li> --}}

                {{-- <li><a href="#" title=""><i class="la la-unlink"></i>Logout</a></li> --}}
            </ul>
        </div>
    </div>

</aside>
