@extends('layouts.website')

@section('content')

@yield('content')
<div id="app">
<div class="profile-sidebar">
    <span class="close-profile"><i class="la la-close"></i></span>
    <div class="can-detail-s">
        <div class="cst"><img src="http://placehold.it/145x145" alt="logo" /></div>
        <h3>David CARLOS</h3>
        <span><i>UX / UI Designer</i> at Atract Solutions</span>
        <p>creativelayers088@gmail.com</p>
        <p>Member Since, 2017</p>
        <p><i class="la la-map-marker"></i>Istanbul / Turkey</p>
    </div>
    <div class="tree_widget-sec">
        <ul>
            <li><a href="candidates_profile.html" title=""><i class="la la-file-text"></i>My Profile</a></li>
            <li><a href="candidates_my_resume.html" title=""><i class="la la-briefcase"></i>My Resume</a></li>
            <li><a href="candidates_shortlist.html" title=""><i class="la la-money"></i>Shorlisted Job</a></li>
            <li><a href="candidates_applied_jobs.html" title=""><i class="la la-paper-plane"></i>Applied Job</a></li>
            <li><a href="candidates_job_alert.html" title=""><i class="la la-user"></i>Job Alerts</a></li>
            <li><a href="candidates_cv_cover_letter.html" title=""><i class="la la-file-text"></i>Cv & Cover Letter</a>
            </li>
            <li><a href="candidates_change_password.html" title=""><i class="la la-flash"></i>Change Password</a>
            </li>


            


            <li><a href="#" title=""><i class="la la-unlink"></i>Logout</a></li>
        </ul>
    </div>
</div>




<div class="page-loading">
    <img src="images/loader.gif" alt="loader" />
    <span>Skip Loader</span>
</div>

<div class="theme-layout" id="scrollup">



    

    <section class="overlape">
        <div class="block no-padding">
            <div data-velocity="-.1"
                style="background: url(http://placehold.it/1600x800) repeat scroll 50% 422.28px transparent;"
                class="parallax scrolly-invisible no-parallax"></div><!-- PARALLAX BACKGROUND IMAGE -->
            <div class="container fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="inner-header">
                            <h3>Welcome Conrads</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="block no-padding">
            <div class="container">
                <div class="row no-gape">
                    <aside class="col-lg-3 column border-right">
                        <div class="widget">
                            <div class="tree_widget-sec">
                                <ul>
                                    <li><a href="candidates_profile.html" title=""><i class="la la-file-text"></i>My
                                            Profile</a></li>
                                    <li><a href="candidates_my_resume.html" title=""><i class="la la-briefcase"></i>My
                                            Resume</a></li>
                                    <li><a href="candidates_shortlist.html" title=""><i
                                                class="la la-money"></i>Shorlisted Job</a></li>
                                    <li><a href="candidates_applied_jobs.html" title=""><i
                                                class="la la-paper-plane"></i>Applied Job</a></li>
                                    <li><a href="candidates_job_alert.html" title=""><i class="la la-user"></i>Job
                                            Alerts</a></li>
                                    <li><a href="candidates_cv_cover_letter.html" title=""><i
                                                class="la la-file-text"></i>Cv & Cover Letter</a></li>
                                    <li><a href="candidates_change_password.html" title=""><i
                                                class="la la-flash"></i>Change Password</a></li>
                                    <li><a href="#" title=""><i class="la la-unlink"></i>Logout</a></li>
                                </ul>
                            </div>
                        </div>

                    </aside>
                    
                </div>
            </div>
        </div>
    </section>



</div>
</div>

@endsection
@section('scripts')

@endsection

@section('style')
  
@endsection
