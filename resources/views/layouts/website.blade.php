<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Iilonga</title>
    <link rel="icon" type="image/x-icon" href="/template/images/logo/iilonga_logo.png" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Upload your cv today">
    <meta name="keywords" content="job search job hunt work">
    <meta name="author" content="Iilonga">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="/template/css/bootstrap-grid.css" />
    <link rel="stylesheet" href="/template/css/icons.css">
    <link rel="stylesheet" href="/template/css/animate.min.css">
    <link rel="stylesheet" type="text/css" href="/template/css/style.css" />
    <link rel="stylesheet" type="text/css" href="/template/css/responsive.css" />
    <link rel="stylesheet" type="text/css" href="/template/css/chosen.css" />
    <link rel="stylesheet" type="text/css" href="/template/css/colors/colors.css" />
    <link rel="stylesheet" type="text/css" href="/template/css/bootstrap.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />

    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.4.19/sweetalert2.all.js"
        integrity="sha512-/vz9zuhaRh2nY4tgFy//B52ghWGt9+iOCYKr1OOmjYpCb68khSixntYopw1vgEdO3620Pmq3gi4WmwTQhv7Zrg=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <style>
        .menu-sec nav>ul>li>a:hover {
            /* border: 1px solid #FF8585 !important; */
            background-color: white !important;
            color: #4172D8 !important;

        }

        .logout {
            color: white;
            background-color: #EC3227;
            padding-left: 12px;
            padding-right: 12px;
            padding-top: 6px;
            padding-bottom: 6px;
            border-radius: 5px;
            font-family: Open Sans;
            font-size: 15px;

            float: left;
            line-height: 22px;
            display: inline-block;
        }

        .profile {
            color: white;
            background-color: blue !important;
            /* padding-left: 12px;
            padding-right: 12px;
            padding-top: 6px;
            padding-bottom: 6px; */
            border-radius: 5px;
            font-family: Open Sans;
            font-size: 15px;

            float: right;
            line-height: 22px;
            display: inline-block;
        }

        .logoutResponsive {
            font-family: Open Sans;
            font-size: 15px;
            color: #ffffff;
            float: left;
            line-height: 22px;
            background: none;
            padding-top: 0px;
        }


        .profileResponsive {
            font-family: Open Sans;
            font-size: 15px;
            color: #ffffff;
            float: left;
            line-height: 22px;
            background: none;
        }

        .profileResponsive a {

            background: #0B1B46;
        }



        .floatButton {
            position: fixed;

            top: 240px;
            right: -24px;
            background-color: #0B1B46;
            color: #FFF;
            border-radius: 2px;
            text-align: center;
            box-shadow: 0px 0px 5px #999;
            z-index: 1003;
            transform: rotate(270deg);
            padding: 5px 10px;
            font-size: 15px;

        }

        .floatButton:hover,
        .floatButton:active {

            background-color: #4172D8;
            color: #000;

        }

        .my-float {
            margin-top: 22px;
        }

        * {
            padding: 0;
            margin: 0;
        }
    </style>
    @yield('style')

</head>

<body class="newbg">
    <div id="app">

        <div class="page-loading">
            <img src="/template/images/loader.gif" alt="loader_spinner" />
        </div>

        <div class="theme-layout" id="scrollup">

            <div class="responsive-header">
                <div class="responsive-menubar">
                    <div class="res-logo"><a href="/" title=""><img style="width: 250px"
                                src="https://res.cloudinary.com/pixel-penguin/image/upload/c_scale,w_250/v1639471604/websites/iilonga/logo/iilonga-02_atefgx.png"
                                alt="Logo" /></a></div>
                    <div class="menu-resaction">
                        <div class="res-openmenu">
                            <img src="/template/images/icon.png" alt="menu_open" /> Menu
                        </div>
                        <div class="res-closemenu">
                            <img src="/template/images/icon2.png" alt="close_button" /> Close
                        </div>
                    </div>
                </div>
                <div class="responsive-opensec">
                    <div class="btn-extars">

                        <ul class="account-btns">
                            @guest
                                {{-- <li class="signup-popup"><a title=""><i class="la la-key"></i> Sign Up</a></li> --}}
                                <li class="signin-popup"><a href="/login" title=""><i
                                            class="la la-external-link-square"></i> Login</a>
                                </li>
                                <li class="signin-popup"><a href="/register" title=""><i class="fa fa-pencil-square-o"
                                            aria-hidden="true"></i> Register</a>
                                </li>
                            @endguest
                            @auth
                                <li>
                                    <form id="logout-form" action="{{ url('logout') }}" method="POST">
                                        {{ csrf_field() }}
                                        <button class="logoutResponsive" style="" class="" type="submit"><i
                                                class="fa fa-sign-out" aria-hidden="true"></i>Logout</button>
                                    </form>
                                </li>

                                <li class="profileResponsive"
                                    style="background: #222b38 !important;
                                                                border-color: white !important;
                                                                color: #ffffff !important;"
                                    class="">
                                    @if (Auth::user()->role == 'candidate')
                                        <a href="/candidate/profile" style="" title=""><i
                                                class="fa fa-user"></i> My
                                            Profile</a>
                                    @else
                                        <a href="/employer/profile" style="" title=""><i class="fa fa-user"></i>
                                            My
                                            Profile</a>
                                    @endif
                                </li>
                                <li class="profileResponsive"
                                    style="background: #222b38 !important;
                                                                border-color: white !important;
                                                                color: #ffffff !important;"
                                    class="">

                                    <a href="/change-password" style="" title=""><i class="fa fa-user"></i>
                                        Change password</a>



                                </li>

                            @endauth
                        </ul>
                    </div><!-- Btn Extras -->
                    {{-- <form class="res-search">
                        <input type="text" placeholder="Job title, keywords or company name" />
                        <button type="submit"><i class="la la-search"></i></button>
                    </form> --}}
                    <div class="responsivemenu">
                        <ul>

                            <li class="">
                                <a href="/" title="">Home</a>

                            </li>
                            <li class="">
                                <a href="/contact-us" title="">Contact Us</a>

                            </li>
                            <li class="">
                                <a href="/browse-cv?allCategories=on" title="">Browse CV's</a>

                            </li>
                            <li class="">
                                <a href="/about-us" title="">About Us</a>

                            </li>
                            <li class="">
                                <a href="/terms-and-conditions" title="">Terms and Conditions</a>

                            </li>

                        </ul>
                    </div>
                </div>
            </div>

            <header class="stick-top forsticky new-header">
                <div class="menu-sec">
                    <div class="container">
                        <div class="logo">
                            <a href="/" title=""><img style="width: 250px" class="hidesticky"
                                    src="https://res.cloudinary.com/pixel-penguin/image/upload/c_scale,w_250/v1639471604/websites/iilonga/logo/iilonga-01_lwnogx.png"
                                    alt="logo" /><img style="width: 250px" class="showsticky"
                                    src="https://res.cloudinary.com/pixel-penguin/image/upload/c_scale,w_250/v1639471604/websites/iilonga/logo/iilonga-01_lwnogx.png"
                                    alt="logo" /></a>
                        </div><!-- Logo -->
                        <div class="btn-extars">

                            <ul class="account-btns">
                                @guest

                                    <li class=""><a href="/login" class="red-button-border-filled"
                                            title=""><i class="la la-external-link-square"></i>
                                            Login</a>
                                    </li>
                                    <li class="signin-popup"><a href="/register" class="red-button-border-filled"
                                            title=""><i class="fa fa-pencil-square-o"></i>
                                            Register</a>
                                    </li>


                                @endguest

                            </ul>
                        </div><!-- Btn Extras -->
                        <nav>
                            <ul>
                                <li>
                                    <a @if (Request::path() == '/') class="active-navigation" @endif
                                        href="/" title="">Home</a>

                                </li>

                                <li>
                                    <a @if (Request::path() == 'contact-us') class="active-navigation" @endif
                                        href="/contact-us" title="">Contact us</a>

                                </li>
                                <li>
                                    <a @if (Request::path() == 'browse-cv') class="active-navigation" @endif
                                        href="/browse-cv?allCategories=on" title="">Browse CV's</a>

                                </li>

                                <li>
                                    <a @if (Request::path() == 'about-us') class="active-navigation" @endif
                                        href="/about-us" title="">About Us</a>

                                </li>
                                <li>
                                    <a @if (Request::path() == 'terms-and-conditions') class="active-navigation" @endif
                                        href="/terms-and-conditions" title="">Terms and Conditions</a>

                                </li>





                                @auth



                                    <li class="menu-item-has-children">
                                        <a href="#" title=""><i class="fa fa-user"></i>
                                            {{ Auth::user()->name }}</a>
                                        <ul>
                                            @if (Auth::user()->role == 'candidate')
                                                <li><a href="/candidate/profile" title=""><i
                                                            class="fa fa-user"></i> My
                                                        Profile</a></li>
                                            @else
                                                <li><a href="/employer/profile" title=""><i class="fa fa-user"></i>
                                                        My
                                                        Profile</a></li>
                                            @endif
                                            <li><a href="/change-password" title=""><i class="fa fa-key"></i>
                                                    Change password</a></li>
                                            <form id="logout-form-dropdown" action="{{ url('logout') }}"
                                                method="POST">
                                                {{ csrf_field() }}

                                                <li><a href="javascript:{}"
                                                        onclick="document.getElementById('logout-form-dropdown').submit();"><i
                                                            class="fa fa-sign-out" aria-hidden="true"></i> Logout</a></li>

                                            </form>







                                        </ul>
                                    </li>
                                @endauth
                            </ul>
                        </nav><!-- Menus -->
                    </div>
                </div>
            </header>

            @yield('content')


            <div class="page-loading">
                <img src="/template/images/loader.gif" alt="loader" />
                <span>Skip Loader</span>
            </div>


            <footer>
                <div class="block">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 column">
                                <div class="widget">
                                    <div class="about_widget">
                                        <div class="logo">
                                            <a href="#" title=""><img style="width: 300px;"
                                                    src="https://res.cloudinary.com/pixel-penguin/image/upload/c_scale,w_300/v1639471604/websites/iilonga/logo/iilonga-02_atefgx.png"
                                                    alt="logo" /></a>
                                        </div>
                                        {{-- <span></span> --}}

                                        <div class="social">
                                            <a href="https://www.facebook.com/iilongaAfrica" title=""><i
                                                    class="fa fa-facebook"></i></a>
                                            <a href="https://www.linkedin.com/company/iilonga/" title=""><i
                                                    class="fa fa-linkedin"></i></a>

                                        </div>
                                    </div><!-- About Widget -->
                                </div>
                            </div>
                            <div class="col-lg-6 column">
                                <div class="widget">
                                    <h3 class="footer-title">Contact Us</h3>
                                    <div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                {{-- <span class="footer-contact" style="color: #F6CD2D">Phone: </span><a
                                                    style="color: white" href="#" title=""></a>
                                                <br> --}}

                                                <span class="footer-contact" style="color: #F6CD2D">Email: </span><a
                                                    style="color: white" href="mailto:info@iilonga.com"
                                                    title="">info@iilonga.com</a>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
                <div class="bottom-line">
                    <span>© {{ now()->year }} Iilonga All rights reserved. Developed by Pixel-Penguin</span>
                    <a href="#scrollup" class="scrollup" title=""><i class="la la-arrow-up"></i></a>
                </div>
            </footer>

        </div>


        <div class="account-popup-area signin-popup-box">
            <div class="account-popup">
                <span class="close-popup"><i class="la la-close"></i></span>
                <h3>User Login</h3>
                {{-- <span>Click To Login</span> --}}
                {{-- <div class="select-user">
                    <span>Candidate</span>
                    <span>Employer</span>
                </div> --}}
                <form action="{{ route('login') }}" method="post" autocomplete="false">
                    @csrf
                    <div class="cfield">
                        <input id="email" name="email" type="email" placeholder="Email "
                            class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}"
                            required autocomplete="email" autofocus />
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <i class="la la-user"></i>
                    </div>
                    <div class="cfield">
                        <input id="password" name="password" type="password" placeholder="********"
                            class="form-control @error('password') is-invalid @enderror" name="password" required
                            autocomplete="current-password">
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <i class="la la-key"></i>
                    </div>
                    <p class="remember-label">
                        <input type="checkbox" name="remember" id="remember"
                            {{ old('remember') ? 'checked' : '' }}>

                        <label for="remember">Remember
                            me</label>
                    </p>
                    <a href="#" title="">Forgot Password?</a>
                    <button class="red-button-border-filled" type="submit">Login</button>






                </form>

            </div>
        </div><!-- LOGIN POPUP -->
        <a href="https://forms.gle/746UUZCJYqQHqBZS9" target="_blank" class="floatButton">


            Feedback

        </a>
        <div class="account-popup-area signup-popup-box">
            <div class="account-popup">
                <span class="close-popup"><i class="la la-close"></i></span>
                <h3>Sign Up</h3>
                <div class="select-user">
                    <span>Candidate</span>
                    <span>Employer</span>
                </div>
                <form>
                    <div class="cfield">
                        <input type="text" placeholder="Username" />
                        <i class="la la-user"></i>
                    </div>
                    <div class="cfield">
                        <input type="password" placeholder="********" />
                        <i class="la la-key"></i>
                    </div>
                    <div class="cfield">
                        <input type="text" placeholder="Email" />
                        <i class="la la-envelope-o"></i>
                    </div>
                    <div class="dropdown-field">
                        <select data-placeholder="Please Select Specialism" class="chosen">
                            <option>Web Development</option>
                            <option>Web Designing</option>
                            <option>Art & Culture</option>
                            <option>Reading & Writing</option>
                        </select>
                    </div>
                    <div class="cfield">
                        <input type="text" placeholder="Phone Number" />
                        <i class="la la-phone"></i>
                    </div>
                    <button type="submit">Signup</button>
                </form>
                <div class="extra-login">
                    <span>Or</span>
                    <div class="login-social">
                        <a class="fb-login" href="#" title=""><i class="fa fa-facebook"></i></a>
                        <a class="tw-login" href="#" title=""><i class="fa fa-twitter"></i></a>
                    </div>
                </div>
            </div>
        </div><!-- SIGNUP POPUP -->
    </div>
    <script src="/js/app.js"></script>
    <script src="/template/js/jquery.min.js" type="text/javascript"></script>
    <script src="/template/js/modernizr.js" type="text/javascript"></script>
    <script src="/template/js/script.js" type="text/javascript"></script>
    <script src="/template/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/template/js/wow.min.js" type="text/javascript"></script>
    <script src="/template/js/slick.min.js" type="text/javascript"></script>
    <script src="/template/js/parallax.js" type="text/javascript"></script>



    <script src="/template/js/tag.js" type="text/javascript"></script>
    <script src="/template/js/select-chosen.js" type="text/javascript"></script>













    @yield('scripts')

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-QFDCC1B25P"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'G-QFDCC1B25P');
    </script>

    {{-- <script>
        @if ($errors->any())
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Something went wrong!'
            })
        @endif
    </script> --}}



</body>

</html>
