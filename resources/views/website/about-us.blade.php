@extends('layouts.website')

@section('content')
    <div class="page-loading">
        <img src="/template/images/loader.gif" alt="" />
        <span>Skip Loader</span>
    </div>

    <div class="theme-layout" id="scrollup">



        {{-- <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item active" aria-current="page">Home</li>
            </ol>
          </nav>
          
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">Library</li>
            </ol>
          </nav> --}}
          
          {{-- <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item"><a href="#">Library</a></li>
              <li class="breadcrumb-item active" aria-current="page">Data</li>
            </ol>
          </nav> --}}

        

        <section>
            <div class="block no-padding  gray">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="inner2">
                                <div class="inner-title2">
                                    <h3>About Us</h3>
                                    {{-- <span>Keep up to date with the latest news</span> --}}
                                </div>
                                <div class="page-breacrumbs">
                                    <ul class="breadcrumbs">
                                        <li><a href="/" title="">Home</a></li>
                                        {{-- <li><a href="#" title="">Pages</a></li> --}}
                                        <li><a href="/about-us" title="">About Us</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>



        <section>
            <div class="block">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 column">
                            
                            <h3>About Us</h3>
<p>Iilonga is an online recruitment agency that was established to connect candidates with recruiters where the business model was based on creating added benefits and outstanding value for both parties.</p>
<h3>Mission </h3>
<p>Our mission is to empower job recruiters and job seekers by connecting them through cost-effective strategies.</p>
<h3>Vision</h3>
<p>Our vision is to create a platform for all people to achieve their fullest potential through finding suitable job opportunities. We work to be trusted by recruiting companies and finding innovative solutions in accordance with their requirements whilst striving to be the enablers of positive change.</p>
<h3>Values</h3>
<p>Trustworthy &ndash; providing a platform where individual&rsquo;s identities are protected and recruiters pay only for what they need</p>
<p>Compassion &ndash; we have compassion for all people, which is why our platform caters for all people and recruiters&rsquo; needs</p>
<p>Innovative &ndash; incorporating innovation to find solutions to connect job seekers with recruiters</p>
<p>Integrity &ndash; we act with integrity in everything we do</p>
<h3>Individual Job Seeker</h3>
<p>Iilonga acts as a platform where candidates can upload CV&rsquo;s for free. The platform provides the opportunity for companies to view candidate CV&rsquo;s whilst protecting the candidate&rsquo;s identity. Companies have the option of purchasing CV&rsquo;s should the candidate meet the company&rsquo;s job specifications. It is for this reason important for candidates to regularly update and maintain their profiles, to ensure for successful placements.</p>
<p>Key advantages: Private and Confidential, Free of Charge Registration, Access to a wide range of job opportunities</p>
<h3>Recruiter</h3>
<p>Iilonga acts as a free platform for employers to search the website for suitable candidates for their job requirements. Only when employers find suitable candidates that meet their job specifications, CV&rsquo;s can be purchased and at which stage the identity of the candidate becomes known. At this stage, the employer is provided with an opportunity to engage with the candidate to proceed with the recruitment process on its own terms. It therefore acts as a cost-effective and efficient job recruitment platform.</p>
<p>Key advantages: Cost-effective (Only pay for what you need, No more thousands of emails, No more expensive advertising), Access to a wide range of CV&rsquo;s</p>

                        </div>
                        
                    </div>
                </div>
            </div>
        </section>



    </div>
@endsection
@section('scripts')
@endsection

@section('style')
    <style>

    </style>
@endsection
