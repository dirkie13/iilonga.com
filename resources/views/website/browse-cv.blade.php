@extends('layouts.website')
@section('content')

    {{-- <div class="page-loading">
        <img src="images/loader.gif" alt="" />
        <span>Skip Loader</span>
    </div> --}}

    <div class="theme-layout" id="scrollup">
        @include('includes.employer-header')







        <section>
            <div class="block remove-top">
                <div class="container">
                    <div class="row no-gape">
                        <aside class="col-lg-3 column">
                            <form action="/browse-cv" method="get">
                                {{-- @csrf --}}
                                <div class="widget border" style="margin-bottom: 35px">
                                    <h3 class="sb-title open">JOB Category</h3>
                                    <div id="filterContainer" class="posted_widget">
                                        <input class="inputCheckBoxes"
                                            {{ request()->filled('allCategories') ? 'checked' : '' }} type="checkbox"
                                            name="allCategories" id="all-types"><label for="all-types">All
                                            Types</label><br />

                                        <p>
                                            @foreach ($jobCategories as $jobCategory)
                                                <input class="inputCheckBoxes" type="checkbox"
                                                    @if (in_array((string) $jobCategory->id, $jobCategoriesInput)) checked @endif {{-- onChange="this.form.submit()
                                            
                                                {{ request()->filled('category_id.samsung') ? 'checked' : '' }}" --}}
                                                    name="jobCategory[]" id="jobCategory{{ $jobCategory->id }}"
                                                    value={{ $jobCategory->id }}><label
                                                    for="jobCategory{{ $jobCategory->id }}">{{ $jobCategory->name }}</label><br />
                                            @endforeach
                                        </p>
                                    </div>
                                </div>
                                 <!-- <div class="widget border">
                                    <h3 class="sb-title open">Job Type</h3>
                                    <div class="type_widget">
                                        <p class="flchek"><input checked type="checkbox" name="choosetype"
                                                id="all-types-type"><label for="all-types-type">All Types</label></p>
                                        @foreach ($employmentTypes as $employmentType)
                                            <p class="flchek"><input type="checkbox"
                                                    name="employmentType{{ $employmentType->id }}"
                                                    id="employmentType{{ $employmentType->id }}"><label
                                                    for="employmentType{{ $employmentType->id }}">{{ $employmentType->name }}</label>
                                            </p>
                                        @endforeach


                                    </div>
                                </div>  -->

                                <div class="widget border">
                                    <h3 class="sb-title open">Years of Experience</h3>
                                    <div class="posted_widget">
                                        <!-- {{-- <p class="flchek"><input checked type="radio" name="experience"
                                                id="all-types-type"><label for="all-types-type">All Types</label></p> --}} -->
                                        @foreach ($yearsOfExperiences as $key => $yearsOfExperience)
                                            <input type="radio" value="{{ $yearsOfExperience->id }}"
                                                name="yearsOfExperienceChosen"
                                                id="yearsOfExperience{{ $yearsOfExperience->id }}"
                                                
                                                @if ($yearsOfExperienceInput == $yearsOfExperience->id) checked @endif><label
                                                for="yearsOfExperience{{ $yearsOfExperience->id }}">{{ $yearsOfExperience->name }}</label><br />
                                        @endforeach


                                    </div>
                                </div>




                                {{-- <div class="widget border">
                                    <h3 class="sb-title open">Date Posted</h3>
                                    <div class="posted_widget">
                                       <input type="radio" name="choose" id="232"><label for="232">Last Hour</label><br />
                                       <input type="radio" name="choose" id="wwqe"><label for="wwqe">Last 24 hours</label><br />
                                       <input type="radio" name="choose" id="erewr"><label for="erewr">Last 7 days</label><br />
                                       <input type="radio" name="choose" id="qwe"><label for="qwe">Last 14 days</label><br />
                                       <input type="radio" name="choose" id="wqe"><label for="wqe">Last 30 days</label><br />
                                       <input type="radio" name="choose" id="qweqw"><label class="nm" for="qweqw">All</label><br />
                                    </div>
                                </div> --}}







                                <button style="inline-size: -webkit-fill-available;inline-block;"
                                    class="update-button red-button-border-filled" style="" type="submit">Search</button>
                            </form>


                        </aside>

                        <div class="col-lg-9 column">
                            {{-- <div class="modrn-joblist np">
					 		<div class="filterbar">
					 			<span class="emlthis"><a href="mailto:example.com" title=""><i class="la la-envelope-o"></i> Email me Jobs Like These</a></span>
					 			<div class="sortby-sec">
					 				<span>Sort by</span>
					 				<select data-placeholder="Most Recent" class="chosen">
										<option>Most Recent</option>
										<option>Most Recent</option>
										<option>Most Recent</option>
										<option>Most Recent</option>
									</select>
									<select data-placeholder="20 Per Page" class="chosen">
										<option>30 Per Page</option>
										<option>40 Per Page</option>
										<option>50 Per Page</option>
										<option>60 Per Page</option>
									</select>
					 			</div>
					 			<h5>98 Jobs & Vacancies</h5>
					 		</div>
						 </div><!-- MOdern Job LIst --> --}}
                            @if ($users->count() == 0)
                                <h1 style="text-align-last: center;     margin-top: 35px;">No results!</h1>
                            @else
                                <div class="job-list-modern">

                                    @foreach ($users as $user)
                                        <div id="resultsUsers">
                                            <div class="job-listings-sec ">
                                                <div class="job-listing wtabs">
                                                    <div class="job-title-sec">
                                                        @if ($user->profile_image != null)
                                                            <div class="c-logo"> <img
                                                                    src="{{ env('CLOUDINARY_WEBSITE_URL') }}c_pad,w_51,h_51/{{ $user->profile_image }}.jpg"
                                                                    alt="profile_image" />
                                                            </div>
                                                        @else
                                                            <div class="c-logo"> <img
                                                                    src="{{ env('CLOUDINARY_WEBSITE_URL') }}c_pad,w_51,h_51/website/website/logo/iilonga_icon-01_lbwd5p.jpg"
                                                                    alt="logo" />
                                                            </div>
                                                        @endif


                                                        <h3><a href="#"
                                                                title="">{{ $user->userCandidate->professional_title }}</a>
                                                        </h3>
                                                        <span
                                                            style="color: #707070">{{ $user->userCandidate->city == null ? 'Not specified' : $user->userCandidate->city }}</span>
                                                        <div class="job-lctn">Skills: @foreach ($user->userCandidate->skills as $key => $skill)
                                                                @if (++$key == count($user->userCandidate->skills))
                                                                    {{ $skill->name }}
                                                                @else
                                                                    {{ $skill->name }},
                                                                @endif
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                    <div class="view-profile-button">
                                                        <button
                                                            onclick="window.location.href='/candidate-profile/{{ $user->userCandidate->id }}'"
                                                            class="">View Profile</button>


                                                    </div>
                                                </div>
                                                <!-- Job -->
                                            </div>
                                        </div>
                                    @endforeach
                                    <div class="pagination">
                                        {{-- {!! $users->links() !!} --}}
                                        @if ($users->hasPages())
                                        <ul >
                                            {{-- Previous Page Link --}}
                                            @if ($users->onFirstPage())
                                                {{-- <li class="disabled"><a>«</a></li> --}}
                                            @else
                                                {{-- <li><a href="{{ $users->withQueryString()->previousPageUrl() }}" rel="prev">«</a></li> --}}
                                                <li class="prev "><a href="{{ $users->withQueryString()->previousPageUrl() }}"><i
                                                    class="la la-long-arrow-left"></i> Prev</a>
                                        </li>
                                            @endif

                                            @if($users->currentPage() > 3)
                                                <li class="hidden-xs"><a href="{{ $users->withQueryString()->url(1) }}">1</a></li>
                                            @endif
                                            @if($users->currentPage() > 4)
                                                <li><a>...</a></li>
                                            @endif
                                            @foreach(range(1, $users->lastPage()) as $i)
                                                @if($i >= $users->currentPage() - 2 && $i <= $users->currentPage() + 2)
                                                    @if ($i == $users->currentPage())
                                                        <li class="active"><a class="active">{{ $i }}</a></li>
                                                    @else
                                                        <li><a href="{{ $users->withQueryString()->url($i) }}">{{ $i }}</a></li>
                                                    @endif
                                                @endif
                                            @endforeach
                                            @if($users->currentPage() < $users->lastPage() - 3)
                                                <li><a>...</a></li>
                                            @endif
                                            @if($users->currentPage() < $users->lastPage() - 2)
                                                <li class="hidden-xs"><a href="{{ $users->withQueryString()->url($users->lastPage()) }}">{{ $users->lastPage() }}</a></li>
                                            @endif

                                            {{-- Next Page Link --}}
                                            @if ($users->hasMorePages())
                                            <li class="next"><a href="{{ $users->withQueryString()->nextPageUrl() }}">Next <i
                                                class="la la-long-arrow-right"></i></a></li>
                                            @else
                                                {{-- <li class="disabled"><a>»</a></li> --}}
                                            @endif
                                        </ul>
                                        @endif
                                    </div><!-- Pagination -->
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>



    </div>

@endsection
@section('scripts')
    <script>
        // function addNewResult() {
        //     var newEntry = ``;


        // }


        // $(document).ready(function() {

        //     $.ajax({
        //         url: '',
        //         type: 'GET'
        //         success: function(data) {

        //         },
        //         error: function(xhr, ajaxOptions, thrownError) {
        //             alert(xhr.status);
        //             alert(thrownError);
        //         }
        //     });
        // })

        function checkCheckBoxesEmpty() {
            if (($(".inputCheckBoxes:checked").length < 1)) {
                $('#all-types').prop("checked", true);
            }
        }

        $(document).ready(function() {
            $('.inputCheckBoxes').change(function(e) { //add hierdie class by alles. Ek het nou sopas!!!!!!!!

                if ($(this).attr('id') == 'all-types') {
                    if ($(this).is(':checked')) {
                        $('.inputCheckBoxes').each(function(item) {
                            if ($(this).attr('id') != 'all-types') {
                                $(this).prop("checked", false);
                            }
                        })
                    }
                } else {
                    $('#all-types').prop("checked", false);
                }

                checkCheckBoxesEmpty();


            });

            checkCheckBoxesEmpty();
        });
    </script>
@endsection

@section('style')
    <style>
        #filterContainer label {
            height: auto
        }

        .job-lctn {}

        .view-profile-button button {
            border: 1px solid #E53737;
            background-color: white;
            float: none;
            display: inline-block;
            position: relative;
            top: 4px;
            margin-left: 13px;
            color: #707070;
            border-color: #E53737;

            border-radius: 4px;
        }

        .job-title-sec>h3>a {
            color: #707070;
        }
        

    </style>
@endsection
