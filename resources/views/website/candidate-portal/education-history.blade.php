@extends('layouts.website')

@section('content')

    {{-- <div class="page-loading">
        <img src="images/loader.gif" alt="" />
        <span>Skip Loader</span>
    </div> --}}

    <div class="theme-layout" id="scrollup">





        
        @include('includes.candidate-header')
        <section  style="z-index: 100">
            <div class="block no-padding">
                <div class="container">
                    <div class="row no-gape">
                        
                        @include('includes.candidate-navigation')
                        <education-history></education-history>
                    </div>
                </div>
            </div>
        </section>



    </div>

    <!-- Profile Sidebar -->



@endsection
@section('scripts')

@endsection

@section('style')

@endsection
