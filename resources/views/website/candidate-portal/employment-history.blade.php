@extends('layouts.website')

@section('content')

    {{-- <div class="page-loading">
        <img src="images/loader.gif" alt="" />
        <span>Skip Loader</span>
    </div> --}}

    <div class="theme-layout" id="scrollup">





        {{-- <section class="overlape">
            <div class="block no-padding">
                <div data-velocity="-.1"
                    style="background: url(http://placehold.it/1600x800) repeat scroll 50% 422.28px transparent;"
                    class="parallax scrolly-invisible no-parallax"></div><!-- PARALLAX BACKGROUND IMAGE -->
                <div class="container fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="inner-header">
                                <h3>Welcome Conrad</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section> --}}
        @include('includes.candidate-header')
        <section style="z-index: 100">
            <div class="block no-padding">
                <div class="container">
                    <div class="row no-gape">

                        @include('includes.candidate-navigation')
                        <employment-history></employment-history>
                    </div>
                </div>
            </div>
        </section>



    </div>

    <!-- Profile Sidebar -->



@endsection
@section('scripts')

@endsection

@section('style')
    <style>
        .modal-backdrop {
    /* bug fix - no overlay */    
    /* display: none;     */
    /* z-index: -1; */
    z-index: 1020;
}


    </style>
@endsection
