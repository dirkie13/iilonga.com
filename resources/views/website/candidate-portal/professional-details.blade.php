@extends('layouts.website')

@section('content')

    {{-- <div class="page-loading">
        <img src="images/loader.gif" alt="" />
        <span>Skip Loader</span>
    </div> --}}

    <div class="theme-layout" id="scrollup">






        @include('includes.candidate-header')
        <section style="z-index: 999">
            {{-- <div id="app" class="wrapper "> --}}

            {{-- </div> --}}
            <div class="block no-padding">
                <div class="container">
                    <div class="row no-gape">

                        @include('includes.candidate-navigation')

                        <professional-detail-list></professional-detail-list>

                    </div>
                </div>
            </div>
        </section>



    </div>

    <!-- Profile Sidebar -->



@endsection
@section('scripts')

@endsection

@section('style')
    <style>
        /* .modal { background: rgba(000, 000, 000, 0.8); min-height:1000000px; } */
        .modal-backdrop {
            z-index: 1020;
        }

        

    </style>
@endsection
