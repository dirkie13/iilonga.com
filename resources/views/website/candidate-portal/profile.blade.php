@extends('layouts.website')

@section('content')
    {{-- <div class="page-loading">
        <img src="images/loader.gif" alt="" />
        <span>Skip Loader</span>
    </div> --}}

    <div class="theme-layout" id="scrollup">






        @include('includes.candidate-header')
        <section>
            <div class="block no-padding">
                <div class="container">
                    <div class="row no-gape">

                        @include('includes.candidate-navigation')
                        <div class="col-lg-9 column">
                            <div class="padding-left">
                                <div class="profile-title">
                                    <h3>My Profile</h3>
                                    <div class="upload-img-bar">
                                        <span class="round">

                                            @if (strlen($userCandidate->profile_image) > 0)
                                                <img style="border-radius:50%"
                                                    src="{{ env('CLOUDINARY_WEBSITE_URL') }}c_fill,g_face,w_140,h_140/{{ $userCandidate->profile_image }}.jpg"
                                                    alt="profile_image" />
                                            @else
                                                <img src="https://via.placeholder.com/140" alt="" />
                                            @endif


                                        </span>
                                        <form action="{{ route('candidate.profile.image.upload') }}" method="post"
                                            enctype="multipart/form-data">
                                            @csrf
                                            <div class="upload-info">
                                                {{-- <a href="#" title="">Browse</a> --}}
                                                <input type="file" id="img" name="image" accept="image/*">
                                                <span>Max file sizes allowed are 10MB</span>
                                            </div>
                                            <div class="col-lg-6">
                                                <button class="update-button red-button-border-filled" style=""
                                                    type="submit">Upload</button>
                                            </div>
                                        </form>
                                    </div>

                                </div>
                                <div class="profile-form-edit">
                                    <form action="{{ '/candidate/profile/update' }}" method="post" autocomplete="false">
                                        @csrf
                                        @if (Session::has('success'))
                                            <div class="alert alert-success alert-dismissible">
                                                <button type="button" class="close" data-dismiss="alert">×</button>
                                                {{ Session::get('success') }}
                                            </div>
                                        @elseif(Session::has('failed'))
                                            <div class="alert alert-danger alert-dismissible">
                                                <button type="button" class="close" data-dismiss="alert">×</button>
                                                {{ Session::get('failed') }}
                                            </div>
                                        @endif

                                        @if (count($errors))    
                                        <div class="row mt-3">
                                            <div class="alert alert-danger alert-dismissible">
                                                <button type="button" class="close" data-dismiss="alert">×</button>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </div>
                                        </div>
                                        @endif



                                        <div class="row">
                                            <div class="col-lg-6">
                                                <span class="pf-title">Name</span>
                                                <div class="pf-field">
                                                    <input value="{{ old('name', $user->name) }}" name="name"
                                                        type="text" placeholder="e.g. John" />
                                                    {!! $errors->first('name', "<span class='text-danger'>:message</span>") !!}
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <span class="pf-title">Surname</span>
                                                <div class="pf-field">
                                                    <input value="{{ old('surname', $userCandidate->surname) }}"
                                                        name="surname" type="text" placeholder="e.g. Doe" />
                                                    {!! $errors->first('surname', "<span class='text-danger'>:message</span>") !!}
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <span class="pf-title">Date of Birth</span>
                                                <div class="pf-field">
                                                    <input
                                                        value="{{ old('date_of_birth', $userCandidate->date_of_birth) }}"
                                                        name="date_of_birth" type="date" placeholder="" />
                                                    {!! $errors->first('date_of_birth', "<span class='text-danger'>:message</span>") !!}
                                                </div>
                                            </div>
                                            {{-- <div class="col-lg-6">
                                                <span class="pf-title">Email Address</span>
                                                <div class="pf-field">
                                                    <input value="{{ old('email', $user->email) }}" name="email" type="email" placeholder="johndoe@email.com" />
                                                    {!! $errors->first('email', "<span class='text-danger'>:message</span>") !!}
                                                </div>
                                            </div> --}}
                                            <div class="col-lg-6">
                                                <span class="pf-title">Cellphone number</span>
                                                <div class="pf-field">
                                                    <input value="{{ old('cellphone', $user->cellphone) }}"
                                                        name="cellphone" type="text" placeholder="0811111111" />
                                                    {!! $errors->first('cellphone', "<span class='text-danger'>:message</span>") !!}
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <span class="pf-title">Current City/Town</span>
                                                <div class="pf-field">
                                                    <input value="{{ old('city', $userCandidate->city) }}" name="city"
                                                        type="text" placeholder="e.g. Windhoek" />
                                                    {!! $errors->first('city', "<span class='text-danger'>:message</span>") !!}

                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <span class="pf-title">Nationality</span>
                                                <div class="pf-field">
                                                    <select name="nationality_id" data-placeholder="nationality"
                                                        class="chosen">
                                                        @foreach ($countries as $country)
                                                            <option
                                                                @if (old('nationality_id', $userCandidate->nationality_id) == $country->id) selected @elseif (old('nationality_id', $userCandidate->nationality_id) == null || old('nationality_id', $userCandidate->nationality_id) == '')@if ($country->id == 152)  selected @endif
                                                                @endif
                                                                value="{{ $country->id }}">
                                                                {{ $country->name }}</option>
                                                        @endforeach
                                                        {{-- <option>South Africa</option> --}}
                                                    </select>

                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <span class="pf-title">Ethnicity</span>
                                                <div class="pf-field">
                                                    <select name="ethnicity" data-placeholder="ethnicity" class="chosen">
                                                        @foreach ($ethnicities as $ethnicity)
                                                            <option @if (old('ethnicity', $userCandidate->ethnicity_id) == $ethnicity->id) selected @endif
                                                                value="{{ $ethnicity->id }}">
                                                                {{ $ethnicity->name }}
                                                            </option>
                                                        @endforeach

                                                    </select>
                                                    {!! $errors->first('ethnicity', "<span class='text-danger'>:message</span>") !!}
                                                </div>
                                            </div>
                                            {{-- <div class="col-lg-6">
                                                <span class="pf-title">Experience</span>
                                                <div class="pf-field">
                                                    <select data-placeholder="Allow In Search" class="chosen">
                                                       <option>2-6 Years</option>
                                                       <option>6-12 Years</option>
                                                   </select>
                                                </div>
                                            </div> --}}
                                            <div class="col-lg-6">
                                                <span class="pf-title">Preferred cities of employment</span>
                                                <div class="pf-field">
                                                    <select name="cities_of_employment[]" multiple style=""
                                                        data-placeholder="Cities"
                                                        class="chosen preferedCitiesEmploymentMulti">
                                                        @foreach ($cities as $city)
                                                            <option value="{{ $city->id }}"
                                                                {{ in_array($city->id, $cities_of_employment_array) ? 'selected' : '' }}>
                                                                {{ $city->name }}</option>
                                                        @endforeach

                                                    </select>
                                                    {!! $errors->first('cities_of_employment', "<span class='text-danger'>:message</span>") !!}
                                                </div>
                                            </div>


                                            <div class="col-lg-6">
                                                <span class="pf-title">Employment type</span>
                                                <div class="pf-field">
                                                    <select name="employment_type" data-placeholder="employment_type"
                                                        class="chosen">
                                                        @foreach ($employment_types as $employment_type)
                                                            <option @if (old('employment_type', $userCandidate->employment_type_id) == $employment_type->id) selected @endif
                                                                value="{{ $employment_type->id }}">
                                                                {{ $employment_type->name }}
                                                            </option>
                                                        @endforeach

                                                    </select>
                                                    {!! $errors->first('ethnicity', "<span class='text-danger'>:message</span>") !!}
                                                </div>
                                            </div>


                                            <div class="col-lg-12">
                                                <span class="pf-title">About me(small summary)</span>
                                                <div class="pf-field">
                                                    <textarea name="about_me">{{ old('about_me', $userCandidate->about_me) }}</textarea>
                                                    {!! $errors->first('about_me', "<span class='text-danger'>:message</span>") !!}
                                                </div>
                                            </div>

                                            @if ($userCandidate->active == true)

                                                <div class="col-lg-12">

                                                    <button class="update-button red-button-border-filled" style=""
                                                        type="submit">Update</button>
                                                </div>
                                            @else
                                                {{-- <div class="col-lg-12">
                                                    <a  class="update-button red-button-border-filled anchor-button" href="/candidate/professional_details">Save and Continue </a>
                                                    
                                                </div> --}}
                                                <div class="col-lg-12">

                                                    <button class="update-button red-button-border-filled" style=""
                                                        type="submit">Save and Continue</button>
                                                </div>
                                            @endif
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>



    </div>

    <!-- Profile Sidebar -->
@endsection
@section('scripts')
@endsection

@section('style')
    <style>
        .preferedCitiesEmploymentMulti {
            position: relative;
            display: block;
            overflow: hidden;
            padding: 0 0 0 8px;
            height: 25px;
            border-radius: 8px;
            background-color: #fff;
            color: #888888;
            text-decoration: none;
            white-space: nowrap;
            line-height: 24px;
            float: left;
            width: 100%;
            text-align: left;
            padding: 0;
            padding: 14px 45px 14px 15px;
            height: auto;
        }

        .chosen-container-single a {

            background: none !important;

        }
    </style>
@endsection
