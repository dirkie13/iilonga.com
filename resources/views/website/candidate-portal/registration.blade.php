@extends('layouts.website')

@section('content')



    {{-- <div class="page-loading">
        <img src="images/loader.gif" alt="" />
        <span>Skip Loader</span>
    </div> --}}

    <div class="theme-layout" id="scrollup">





        <section class="overlape">
            <div class="block no-padding">
                <div data-velocity="-.1"
                    style="background: url({{ env('CLOUDINARY_WEBSITE_URL') }}c_fill,w_1600,h_800/website/website/images/businessman-checking-time-from-watch-1_ziwzos_ufkhwh.jpg) repeat scroll 50% 422.28px transparent;"
                    class="parallax scrolly-invisible no-parallax"></div><!-- PARALLAX BACKGROUND IMAGE -->
                <div class="container fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="inner-header">
                                <h3>Candidate Registration</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section>
            <div class="block no-padding">
                <div class="container">
                    <div class="row no-gape">

                        <div class="col-lg-9 column">
                            <div class="padding-left">

                                <div class="profile-form-edit">
                                    <?php
                                    $sum1 = mt_rand(1, 10);
                                    $sum2 = mt_rand(1, 10);
                                    ?>
                                    <form action="/candidate/registration/store" method="post" autocomplete="false">
                                        @csrf
                                        @if (Session::has('success'))
                                            <div class="alert alert-success alert-dismissible">
                                                <button type="button" class="close" data-dismiss="alert">×</button>
                                                {{ Session::get('success') }}
                                            </div>
                                        @elseif(Session::has('failed'))
                                            <div class="alert alert-danger alert-dismissible">
                                                <button type="button" class="close" data-dismiss="alert">×</button>
                                                {{ Session::get('failed') }}
                                            </div>
                                        @endif
                                        <div class="row">
                                            <div class="col-lg-12 mt-3"
                                                style=" width:100%; display: flex; justify-content: center;">
                                                <h5
                                                    style="display: inline-block; margin-left: 0 auto; margin-right: 0 auto">
                                                    Enter your details</h5>
                                            </div>
                                        </div>
                                        @if ($errors->any())
                                            <div class="alert alert-danger">
                                                <ul class="list-unstyled">
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <span class="pf-title">Name</span>
                                                <div class="pf-field">
                                                    <input name="name" type="text" placeholder="John"
                                                        value="{{ old('name') }}" />
                                                    {!! $errors->first('name', "<span class='text-danger'>:message</span>") !!}
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <span class="pf-title">Surname</span>
                                                <div class="pf-field">
                                                    <input name="surname" type="text" placeholder="Doe"
                                                        value="{{ old('surname') }}" />
                                                    {!! $errors->first('surname', "<span class='text-danger'>:message</span>") !!}
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <span class="pf-title">Cellphone</span>
                                                <div class="pf-field">
                                                    <input name="cellphone" type="text" placeholder="+264 81 123 4567"
                                                    value="{{ old('cellphone') }}" autocomplete="tel" />
                                                    {!! $errors->first('cellphone', "<span class='text-danger'>:message</span>") !!}
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <span class="pf-title">Email</span>
                                                <div class="pf-field">
                                                    <input name="email" type="email" placeholder="johndoe@email.com"
                                                        value="{{ old('email') }}" />
                                                    {!! $errors->first('email', "<span class='text-danger'>:message</span>") !!}
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <span class="pf-title">Password</span>
                                                <div class="pf-field">

                                                    <input minlength="10" name="password" type="password" placeholder="********"
                                                        value="{{ old('password') }}" autocomplete="new-password" />
                                                    {!! $errors->first('password', "<span class='text-danger'>:message</span>") !!}

                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <span class="pf-title">Confirm Password</span>
                                                <div class="pf-field">
                                                    <input name="confirm_password" type="password" placeholder="********"
                                                        value="{{ old('confirm_password') }}" />
                                                    {!! $errors->first('confirm_password', "<span class='text-danger'>:message</span>") !!}
                                                </div>
                                            </div>
                                            <input name="sum1" type="hidden" value="{{ $sum1 }}" />
                                            <input name="sum2" type="hidden" value="{{ $sum2 }}" />
                                            <div class="col-md-3">
                                                <h4>What is {{ $sum1 }} + {{ $sum2 }}</h4>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <input id="captcha" type="text" class="form-control"
                                                    placeholder="Enter Result" name="captcha">
                                            </div>
                                            <div class="form-check col-12">
                                                <input name="accept" class="form-check-input" type="checkbox" value=""
                                                    id="accept" onclick="Enable(this)">
                                                <label class="form-check-label" for="accept">
                                                    Accept Terms and Conditions - read terms and conditions <a style="color: blue" href="/terms-and-conditions">here</a>
                                                </label>
                                            </div>
                                            <div style="
                                                                width:100%;
                                                                display: flex;
                                                                justify-content: center;" class="col-lg-12 mt-3 mb-3">
                                                <button disabled  id="submit" class="red-button-border-filled" style="
                                                                        " type="submit">Continue</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>



    </div>

    <div class="profile-sidebar">
        <span class="close-profile"><i class="la la-close"></i></span>
        <div class="can-detail-s">
            <div class="cst"><img src="https://via.placeholder.com/145" alt="placeholder" /></div>
            <h3>David CARLOS</h3>
            <span><i>UX / UI Designer</i> at Atract Solutions</span>
            <p>creativelayers088@gmail.com</p>
            <p>Member Since, 2017</p>
            <p><i class="la la-map-marker"></i>Istanbul / Turkey</p>
        </div>
        <div class="tree_widget-sec">
            <ul>
                <li><a href="candidates_profile.html" title=""><i class="la la-file-text"></i>My Profile</a></li>
                <li><a href="candidates_my_resume.html" title=""><i class="la la-briefcase"></i>My Resume</a></li>
                <li><a href="candidates_shortlist.html" title=""><i class="la la-money"></i>Shorlisted Job</a></li>
                <li><a href="candidates_applied_jobs.html" title=""><i class="la la-paper-plane"></i>Applied Job</a></li>
                <li><a href="candidates_job_alert.html" title=""><i class="la la-user"></i>Job Alerts</a></li>
                <li><a href="candidates_cv_cover_letter.html" title=""><i class="la la-file-text"></i>Cv & Cover Letter</a>
                </li>
                <li><a href="candidates_change_password.html" title=""><i class="la la-flash"></i>Change Password</a>
                </li>
                <li><a href="#" title=""><i class="la la-unlink"></i>Logout</a></li>
            </ul>
        </div>
    </div><!-- Profile Sidebar -->



@endsection
@section('scripts')

    <script type="text/javascript">
        Enable = function(val) {
            var sbmt = document.getElementById("submit"); //id of button

            if (val.checked == true) {
                sbmt.disabled = false;
                sbmt.style.background = '#4172D8';
            } else {
                sbmt.disabled = true;
                sbmt.style.background = 'grey';
            }
        }
    </script>

@endsection

@section('style')
    <style>
        button:hover,
        button:focus {
            color: #1a1111 !important;
            /* background-color: #723c3c !important; */
        }

        label::before {
            content: " ";
            border: 2px solid #e6e7ef;

            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            -ms-border-radius: 3px;
            -o-border-radius: 3px;
            border-radius: 3px;
        }

    </style>
@endsection
