@extends('layouts.website')

@section('content')
    {{-- <div class="page-loading">
        <img src="images/loader.gif" alt="" />
        <span>Skip Loader</span>
    </div> --}}

    <div class="theme-layout" id="scrollup">





        {{-- <section class="overlape">
            <div class="block no-padding">
                <div data-velocity="-.1"
                    style="background: url(http://placehold.it/1600x800) repeat scroll 50% 422.28px transparent;"
                    class="parallax scrolly-invisible no-parallax"></div><!-- PARALLAX BACKGROUND IMAGE -->
                <div class="container fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="inner-header">
                                <h3>Welcome Conrad</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section> --}}
        @include('includes.candidate-header')

        
        <section>
            <div class="block no-padding">
                <div class="container">
                    <div class="row no-gape">
                        
                        @include('includes.candidate-navigation')
                        
                        <upload-cv></upload-cv>
                        
                        
                    </div>
                </div>
            </div>
        </section>



    </div>

    <!-- Profile Sidebar -->
@endsection
@section('scripts')
    <script>
        // Add the following code if you want the name of the file appear on select
        // $(".custom-file-input").on("change", function() {
        //     var fileName = $(this).val().split("\\").pop();
        //     $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
        // });

        

    </script>
@endsection

@section('style')
    <style>
        .custom-file-upload {
            border: 1px solid #ccc;
            display: inline-block;
            padding: 6px 12px;
            cursor: pointer;
        }

    </style>
@endsection
