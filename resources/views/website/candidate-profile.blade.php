@extends('layouts.website')
@section('content')
    {{-- <div class="page-loading">
        <img src="images/loader.gif" alt="" />
        <span>Skip Loader</span>
    </div> --}}

    <div class="theme-layout" id="scrollup">

        @include('includes.employer-header')







        <section class="overlape">
            <div class="block remove-top">
                <div class="container">
                    <div class="row">
                        {{-- @include('includes.employer-navigation') --}}
                        <div class="col-lg-12">
                            <div class="cand-single-user">
                                <div class="share-bar circle"></div>
                                <div class="can-detail-s mb-3 pb-3">








                                    <div class="cst">



                                        @if (strlen($user->userCandidate->profile_image) > 0)
                                            <img style="border-radius:50%"
                                                src="{{ env('CLOUDINARY_WEBSITE_URL') }}c_fill,g_face,w_145,h_145/{{ $user->userCandidate->profile_image }}.jpg"
                                                alt="profile_image" />
                                        @else
                                            <img src="https://via.placeholder.com/145" alt="placeholder" />
                                        @endif






                                    </div>
                                    <h3>{{ $user->name }} {{ $user->userCandidate->surname }}</h3>
                                    <span><i>{{ $user->userCandidate->professional_title }}</i></span>
                                    <p>{{ $user->cellphone }}</p>
                                    <p>{{ $user->email }}</p>
                                    <p>Member Since, {{ $user->created_at->year }}</p>
                                    {{-- <p><i class="la la-map-marker"></i>Istanbul / Turkey</p>
				 				<div class="skills-badge">
				 					<span>Photoshop</span><span>Designer</span><span>Ilustrator</span>
				 				</div> --}}
                                </div>
                                <div class="download-cv"></div>
                            </div>
                            <div class="cand-details-sec">
                                @if (Session::has('success'))
                                    <div class="alert alert-success alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        {{ Session::get('success') }}
                                    </div>
                                @elseif(Session::has('failed'))
                                    <div class="alert alert-danger alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        {{ Session::get('failed') }}
                                    </div>
                                @elseif(Session::has('noTokens'))
                                    <div class="alert alert-danger alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        {{ Session::get('noTokens') }}
                                    </div>
                                @endif
                                <div class="row no-gape">

                                    <div class="col-lg-12 column">
                                        <div class="cand-details">
                                            {{-- <div class="job-overview style2">
								 			<ul>
								 				<li><i class="la la-money"></i><h3>Offerd Salary</h3><span>£15,000 - £20,000</span></li>
								 				<li><i class="la la-mars-double"></i><h3>Gender</h3><span>Female</span></li>
								 				<li><i class="la la-thumb-tack"></i><h3>Career Level</h3><span>Executive</span></li>
								 				<li><i class="la la-puzzle-piece"></i><h3>Industry</h3><span>Management</span></li>
								 				<li><i class="la la-shield"></i><h3>Experience</h3><span>2 Years</span></li>
								 				<li><i class="la la-line-chart "></i><h3>Qualification</h3><span>Bachelor Degree</span></li>
								 			</ul>
								 		</div><!-- Job Overview --> --}}
                                            @if ($user->userCandidate->about_me != '')
                                                <h2>About Me</h2>
                                                <p>{{ $user->userCandidate->about_me }}</p>
                                            @endif

                                            @if ($user->userCandidate->ethnicity_id != '')
                                                <h2>Ethnicity</h2>
                                                <p>{{ $user->userCandidate->ethnicity->name }}</p>
                                            @endif


                                            @if ($user->userCandidate->educationHistories()->count() > 0)
                                                <div class="edu-history-sec">
                                                    <h2>Education</h2>


                                                    @foreach ($user->userCandidate->educationHistories as $educationHistory)
                                                        <div class="edu-history">
                                                            <i class="la la-graduation-cap"></i>
                                                            <div class="edu-hisinfo">
                                                                <h3>{{ $educationHistory->name }}</h3>
                                                                <i>{{ Carbon\Carbon::parse($educationHistory->date_from)->format('Y') }}
                                                                    -
                                                                    {{ Carbon\Carbon::parse($educationHistory->date_to)->format('Y') }}</i>
                                                                {{-- <span><i>Computer Science</i></span> --}}
                                                                <p>{{ $educationHistory->summary }}</p>
                                                            </div>
                                                        </div>
                                                    @endforeach

                                                </div>
                                            @endif
                                            @if ($user->userCandidate->employmentHistories()->count() > 0)
                                                <div class="edu-history-sec">
                                                    <h2>Work & Experience</h2>
                                                    @foreach ($user->userCandidate->employmentHistories as $employmentHistory)
                                                        <div class="edu-history style2">
                                                            <i></i>
                                                            <div class="edu-hisinfo">
                                                                <h3>{{ $employmentHistory->title }}
                                                                    <span>{{ $employmentHistory->name }}</span>
                                                                </h3>
                                                                <i>{{ Carbon\Carbon::parse($employmentHistory->date_from)->format('Y') }}
                                                                    -
                                                                    {{ Carbon\Carbon::parse($employmentHistory->date_to)->format('Y') }}</i>
                                                                <p>{{ $employmentHistory->summary }}</p>
                                                            </div>
                                                        </div>
                                                    @endforeach

                                                </div>
                                            @endif
                                            @if ($user->userCandidate->jobCategories()->count() > 0)
                                                <div class="edu-history-sec">
                                                    <h2>Job Categories</h2>

                                                    <div class="edu-history style2">


                                                        <div class="skills-badge">
                                                            @foreach ($user->userCandidate->jobCategories as $jobCategory)
                                                                <span>{{ $jobCategory->name }}</span>
                                                            @endforeach
                                                        </div>
                                                    </div>


                                                </div>
                                            @endif
                                            @if ($user->userCandidate->skills()->count() > 0)
                                                <div class="edu-history-sec">
                                                    <h2>Professional Skills</h2>

                                                    <div class="edu-history style2">


                                                        <div class="skills-badge">
                                                            @foreach ($user->userCandidate->skills as $skill)
                                                                <span>{{ $skill->name }}</span>
                                                            @endforeach
                                                        </div>
                                                    </div>


                                                </div>
                                            @endif

                                            @if ($user->userCandidate->skills()->count() > 0)
                                                <div class="edu-history-sec">
                                                    <h2>Prefered Cities of Employment</h2>

                                                    <div class="edu-history style2">


                                                        <div class="skills-badge">
                                                            @foreach ($user->cities as $city)
                                                                <span>{{ $city->name }}</span>
                                                            @endforeach
                                                        </div>
                                                    </div>


                                                </div>
                                            @endif
                                            <div style="place-content: center;" class="row">

                                                {{-- <button
                                                    style="background-color: #FF9292; display: inline-block;
                                                                        margin: 0 auto; color: white"
                                                    type="submit">Download my Cv</button> --}}
                                                @auth
                                                    @if ($user->role == 'business')
                                                        @if ($bought == true)
                                                            @if ($user->userCandidate->cv != '')
                                                                <a href="{{ env('CLOUDINARY_WEBSITE_URL') }}{{ $user->userCandidate->cv }}.pdf" target="blank"
                                                                    download>
                                                                    <button style="margin: 0 auto;inline-block;"
                                                                        class="update-button red-button-border-filled" style=""
                                                                        type="button">Download CV</button>
                                                                </a>
                                                            @else
                                                            <div style="place-content: center;" class="alert alert-danger d-flex">
                                                                <div >
                                                                    No cv to download
                                                                </div>
                                                            </div>
    
                                                            @endif
                                                        @else
                                                            <form
                                                                action="/employer/candidate-profile/buy-candidate/0/{{ $user->userCandidate->id }}"
                                                                method="POST">
                                                                @csrf
                                                                <a onclick="return confirm('Are you sure you want to buy this candidate?')"
                                                                    download>
                                                                    <button style="margin: 0 auto;inline-block;"
                                                                        class="update-button red-button-border-filled" style=""
                                                                        type="submit">Buy Candidate</button>
                                                                </a>
                                                            </form>
                                                        @endif
                                                    @endif


                                                @endauth


                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>



    </div>
@endsection
@section('scripts')
@endsection

@section('style')
    <style>
        .skills-badge span {
            background-color: #C25151;
            color: #FFFFFF;
            border-radius: 5px;
        }

        .row button {
            border-radius: 5px
        }

    </style>
@endsection
