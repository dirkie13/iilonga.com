@extends('layouts.website')

@section('content')
    <div class="page-loading">
        <img src="/template/images/loader.gif" alt="" />
        <span>Skip Loader</span>
    </div>

    <div class="theme-layout" id="scrollup">





        <section>
            <div class="block no-padding  gray">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="inner2">
                                <div class="inner-title2">
                                    <h3>Contact</h3>
                                    {{-- <span>Keep up to date with the latest news</span> --}}
                                </div>
                                <div class="page-breacrumbs">
                                    <ul class="breadcrumbs">
                                        <li><a href="/" title="">Home</a></li>
                                        {{-- <li><a href="#" title="">Pages</a></li> --}}
                                        <li><a href="/contact-us" title="">Contact</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>



        <section>
            <div class="block">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 column">
                            <div class="contact-form">
                                @if (Session::has('success'))
                                    <div class="alert alert-success alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        {{ Session::get('success') }}
                                    </div>
                                @elseif(Session::has('failed'))
                                    <div class="alert alert-danger alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        {{ Session::get('failed') }}
                                    </div>
                                @endif
                                <h3>Keep In Touch</h3>

                                <?php
                                $sum1 = mt_rand(1, 10);
                                $sum2 = mt_rand(1, 10);
                                ?>

                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul class="list-unstyled">
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                                <form method="POST" action="/contact-us/send-email">
                                    @csrf
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <span class="pf-title">Full Name</span>
                                            <div class="pf-field">
                                                <input value="{{ old('fullName') }}" name="fullName" required type="text"
                                                    placeholder="e.g. John Doe" />
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <span class="pf-title">Email</span>
                                            <div class="pf-field">
                                                <input value="{{ old('email') }}" name="email" required type="text"
                                                    placeholder="e.g. johndoe@email.com" />
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <span class="pf-title">Subject</span>
                                            <div class="pf-field">
                                                <input value="{{ old('subject') }}" name="subject" required type="text"
                                                    placeholder="e.g. Question" />
                                            </div>
                                        </div>
                                        <input name="sum1" type="hidden" value="{{ $sum1 }}" />
                                        <input name="sum2" type="hidden" value="{{ $sum2 }}" />
                                        <div class="col-lg-12">
                                            <span class="pf-title">Message</span>
                                            <div class="pf-field">
                                                <textarea name="message" required>{{ old('message') }}</textarea>
                                            </div>
                                        </div>
                                        {{-- <div class="row"> --}}
                                        <div class="col-md-4">
                                            <h4>What is {{ $sum1 }} + {{ $sum2 }}</h4>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <input id="captcha" type="text" class="form-control"
                                                placeholder="Enter Result" name="captcha">
                                        </div>
                                        {{-- </div> --}}
                                        <div class="col-lg-12">
                                            <button class="red-button-border-filled" type="submit">Send</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-6 column">
                            <div class="contact-textinfo style2">
                                <h3>Iilonga Office</h3>
                                <ul>
                                    <li><i class="la la-map-marker"></i><span>
                                            Windhoek, Namibia.</span></li>
                                    {{-- <li><i class="la la-phone"></i><span>Call Us : </span></li> --}}
                                    {{-- <li><i class="la la-fax"></i><span>Fax :</span></li> --}}
                                    <li><i class="la la-envelope-o"></i><span>Email : info@iilonga.com</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>



    </div>
@endsection
@section('scripts')
@endsection

@section('style')
    <style>

    </style>
@endsection
