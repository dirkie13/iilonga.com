<html>

<head>
  <title>Buy Some Credit!</title>
	
	{{-- <link rel="stylesheet" type="text/css" href="/template/css/bootstrap.min.css"> --}}
	<link rel="stylesheet" type="text/css" href="/template/css/bootstrap.css" />
	
	<style>
	
		#paytodaybtn > div{ margin: auto}

	</style>
</head>

<body>
	
	<div class="container">
		<div class="mt-3">
			<div class="lg:container lg:mx-auto" style="background: #fff">
			  <div class="p-12" style="text-align: center">
				
				<img style="max-width: 80%" src="{{ env('CLOUDINARY_WEBSITE_URL') }}c_scale,w_500/v1/website/website/logo/iilonga-01_lwnogx_liwaic.png" />
				@if($payment->paid == false)d
				<p style="margin-top: 30px">You're about to buy <strong>{{ $payment->tokens }}</strong> tokens for <strong>N${{ $payment->amount }}</strong></p>

				<div id="paytodaybtn"></div>
				
				<script src="{{env('PAYTODAY_SCRIPT_SRC')}}"></script>
				
				<script type="text/javascript">
				//<![CDATA[
				document.addEventListener('DOMContentLoaded', function() {

				<?php
				  $amount = explode('.', $payment->amount);

				  
				  //PayToday require us to add double 0 after the amount. For example N$200 would be N$20000, so this script just does that
				  if(isset($amount[1])){
					$amount[1] = str_pad($amount[1], 2, '0');

				  }
				  else{
					$amount[1] = '00';
				  }

				  
				?>

				createButton('{{ env('PAYTODAY_BUSINESS_ID') }}', '{{ env('PAYTODAY_BUSINESS_NAME') }}', '{{ $amount[0] }}{{ $amount[1] }}', '{{env('PAYTODAY_REDIRECT_LINK')}}/employer/wallet/confirmpayment/{{ $payment->id }}', 'IilongaPayment{{ $payment->id }}');
				});

				
				//]]>
				</script>
				@else
				<p>Payment is done. <a href="/admin/buycredit/pay/{{ $payment->id }}/done">Click here to continue</a></p>
				@endif
				  
				 <a style="margin-top: 20px" href="/employer/wallet" class="btn btn-primary">Go Back</a>
			  </div>
			</div>
		  </div>

	</div>
  
</body>

</html>