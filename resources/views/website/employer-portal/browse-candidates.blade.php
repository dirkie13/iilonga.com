@extends('layouts.website')
@section('content')

    {{-- <div class="page-loading">
        <img src="images/loader.gif" alt="" />
        <span>Skip Loader</span>
    </div> --}}

    <div class="theme-layout" id="scrollup">

        @include('includes.employer-header')







        <section>
            <div class="block remove-top">
                <div class="container">
                    <div class="row no-gape">
                        
                        @include('includes.employer-navigation')
                        <div class="col-lg-9 column">
                            <div class="modrn-joblist np">
					 		{{-- <div class="filterbar">
					 			
					 			<div class="sortby-sec">
					 				<span>Sort by</span>
					 				<select data-placeholder="Most Recent" class="chosen">
										<option>Most Recent</option>
										<option>Most Recent</option>
										<option>Most Recent</option>
										<option>Most Recent</option>
									</select>
									<select data-placeholder="20 Per Page" class="chosen">
										<option>30 Per Page</option>
										<option>40 Per Page</option>
										<option>50 Per Page</option>
										<option>60 Per Page</option>
									</select>
					 			</div>
					 			<h5>98 Jobs & Vacancies</h5>
					 		</div> --}}
                             
                             <div class="container-fluid bg-light " style="margin-bottom: 20px;">
                                <div class="row">
                                    <form  id="form_search" style="display:contents">
                                        <div class="col-12 col-md-3 ">
                                            <div class="form-group ">
                                                
                                                <label class="form-check-label brand-label" for="minimum_experience">
                                                    Minimum Experience
                                                </label>
                                                <select style="" class="form-control bottom-content" name="minimum_experience">
                                                    @foreach ($yearsOfExperiences as $key => $yearsOfExperience)
                                                        
                                                        <option @if (Request::get('minimum_experience') == $yearsOfExperience->value) selected @endif
                                                            value="{{ $yearsOfExperience->value }}">{{ $yearsOfExperience->name }}
                                                        </option>
                                                        @endforeach
        
                                                </select>
        
        
                                            </div>
                                        </div>
                                        
        
        
                                        <div class="col-12 col-md-3">
                                            <div class="form-group ">
                                                
                                                <label class="form-check-label brand-label" for="ascOrDesc">
                                                    Sort By
                                                </label>
                                                <select class="form-control bottom-content" style="" name="minimumExperienceAscOrDesc">
                                                    
                                                        
                                                    <option @if (Request::get('minimumExperienceAscOrDesc') == 'asc') selected @endif value="asc" >Asc</option>
                                                    <option @if (Request::get('minimumExperienceAscOrDesc') == 'desc') selected @endif value="desc" >Desc</option>
        
                                                </select>
        
        
                                            </div>
                                        </div>
        
        
        
        
        
        
                                        <div class="col-12 col-md-3">
                                            <div class="form-group">
                                                <label class="form-check-label brand-label" for="employment-type">
                                                    Employment type
                                                </label>
                                                <select name="employment_type" id="employment-type" class="form-control">
                                                    <option value="all" selected>All</option>
                                                    @foreach ($employmentTypes as $key => $employmentType)
                                                        <option @if (Request::get('employment_type') == $employmentType->id) selected  @endif
                                                            value="{{ $employmentType->id }}">{{ $employmentType->name }}
                                                        </option>
                                                    @endforeach
        
                                                </select>
                                            </div>
                                        </div>
        
                                        
        
                                        <div class="col-12 col-md-3">
                                            <button style="margin-top: 14px;" type="submit"
                                                class="btn btn-primary btn-block">Search</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
						 </div>
                         <!-- MOdern Job LIst -->
                            <div class="job-list-modern">
                                @foreach ($users as $user)
                                    <div class="job-listings-sec ">
                                        <div class="job-listing wtabs">
                                            <div class="job-title-sec">
                                                @if($user->profile_image != null)
                                                <div class="c-logo"> <img src="{{ env('CLOUDINARY_WEBSITE_URL') }}c_pad,w_51,h_51/{{$user->profile_image}}.jpg"
                                                    alt="logo" />
                                                </div>
                                                    @else
                                                    <div class="c-logo"> <img src="{{ env('CLOUDINARY_WEBSITE_URL') }}c_pad,w_51,h_51/website/website/logo/iilonga_icon-01_lbwd5p.jpg"
                                                        alt="logo" />
                                                    </div>
                                                    @endif
                                                <h3><a href="#" title="">{{$user->userCandidate->professional_title}}</a></h3>
                                                <span style="color: #707070">{{$user->userCandidate->city == null? 'Not specified': $user->userCandidate->city}}</span>
                                                <div class="job-lctn">Skills: @foreach ($user->userCandidate->skills as $key => $skill)
                                                    @if (++$key == count($user->userCandidate->skills))
                                                        {{ $skill->name }}
                                                    @else
                                                        {{ $skill->name }},
                                                    @endif
                                                @endforeach
                                            </div>
                                            </div>
                                            <div class="view-profile-button">
                                                <button onclick="window.location.href='/employer/candidate-profile/{{$jobId}}/{{$user->userCandidate->id}}'"
                                                    class="">View Profile</button>


                                            </div>
                                        </div>
                                        <!-- Job -->
                                    </div>
                                @endforeach
                                {{-- <div class="pagination">
                                    
                                    <ul>
                                        @if ($users->currentPage() != 1)
                                            <li class="prev "><a href="{{ $users->withQueryString()->previousPageUrl() }}"><i
                                                        class="la la-long-arrow-left"></i> Prev</a>
                                            </li>
                                        @endif
                                        @for ($i = 1; $i <= $users->lastPage(); $i++)

                                            <li class="{{ $users->currentPage() == $i ? ' active' : '' }}"><a
                                                    href="{{ $users->withQueryString()->url($i) }}">{{ $i }}</a></li>
                                        @endfor
                                        
                                        @if ($users->hasMorePages())
                                            <li class="next"><a href="{{ $users->withQueryString()->nextPageUrl() }}">Next <i
                                                        class="la la-long-arrow-right"></i></a></li>
                                        @endif
                                    </ul>
                                </div> --}}

                                <div class="pagination">
                                    {{-- {!! $users->links() !!} --}}
                                    @if ($users->hasPages())
                                    <ul >
                                        {{-- Previous Page Link --}}
                                        @if ($users->onFirstPage())
                                            {{-- <li class="disabled"><a>«</a></li> --}}
                                        @else
                                            {{-- <li><a href="{{ $users->withQueryString()->previousPageUrl() }}" rel="prev">«</a></li> --}}
                                            <li class="prev "><a href="{{ $users->withQueryString()->previousPageUrl() }}"><i
                                                class="la la-long-arrow-left"></i> Prev</a>
                                    </li>
                                        @endif

                                        @if($users->currentPage() > 3)
                                            <li class="hidden-xs"><a href="{{ $users->withQueryString()->url(1) }}">1</a></li>
                                        @endif
                                        @if($users->currentPage() > 4)
                                            <li><a>...</a></li>
                                        @endif
                                        @foreach(range(1, $users->lastPage()) as $i)
                                            @if($i >= $users->currentPage() - 2 && $i <= $users->currentPage() + 2)
                                                @if ($i == $users->currentPage())
                                                    <li class="active"><a class="active">{{ $i }}</a></li>
                                                @else
                                                    <li><a href="{{ $users->withQueryString()->url($i) }}">{{ $i }}</a></li>
                                                @endif
                                            @endif
                                        @endforeach
                                        @if($users->currentPage() < $users->lastPage() - 3)
                                            <li><a>...</a></li>
                                        @endif
                                        @if($users->currentPage() < $users->lastPage() - 2)
                                            <li class="hidden-xs"><a href="{{ $users->withQueryString()->url($users->lastPage()) }}">{{ $users->lastPage() }}</a></li>
                                        @endif

                                        {{-- Next Page Link --}}
                                        @if ($users->hasMorePages())
                                        <li class="next"><a href="{{ $users->withQueryString()->nextPageUrl() }}">Next <i
                                            class="la la-long-arrow-right"></i></a></li>
                                        @else
                                            {{-- <li class="disabled"><a>»</a></li> --}}
                                        @endif
                                    </ul>
                                    @endif
                                </div><!-- Pagination -->

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>



    </div>

@endsection
@section('scripts')
{{-- <script>

function checkCheckBoxesEmpty() {
    if (($(".inputCheckBoxes:checked").length < 1)) {
        $('#all-types').prop("checked", true);
    }
}

$(document).ready(function() {
    $('.inputCheckBoxes').change(function(e) { //add hierdie class by alles. Ek het nou sopas!!!!!!!!

        if ($(this).attr('id') == 'all-types') {
            if ($(this).is(':checked')) {
                $('.inputCheckBoxes').each(function(item) {
                    if ($(this).attr('id') != 'all-types') {
                        $(this).prop("checked", false);
                    }
                })
            }
        } else {
            $('#all-types').prop("checked", false);
        }

        checkCheckBoxesEmpty();


    });

    checkCheckBoxesEmpty();
});
</script> --}}

<script>
    function buildUrl() {

        var url = '?';

        var count = 0;

        var jobId = '<?php echo $jobId; ?>';

        $('#form_search input, #form_search select').each(function() {
            if ($(this).val() != '') {
                if (count == 0) {
                    url += $(this).attr('name') + '=' + $(this).val();
                } else {
                    url += "&" + $(this).attr('name') + '=' + $(this).val();
                }

                count++;
            }
        });

        //alert(url);
        window.location = '/employer/browse-candidates/'+ jobId + url;

    }

    $(document).ready(function() {

        $('#form_search').submit(function(e) {
            e.preventDefault();
            buildUrl();
        })

        
        $(".sel-sort").on('change', function() {
            $("input[name='sort']").val($(this).val());
            $("#searchForm").trigger('submit');
        });

        $(".sel-view").on('change', function() {
            $("input[name='view']").val($(this).val());
            $("#searchForm").trigger('submit');
        });
    })
</script>

{{-- <script type='text/javascript'>

function getModels(){
    // Department id
    var id = $('#brands').val();

    // Empty the dropdown
    $('#brandModels').find('option').not(':first').remove();

    // AJAX request 
    $.ajax({
        url: 'cars/getModels/'+id,
        type: 'get',
        dataType: 'json',
        success: function(response){

            var len = 0;
            if(response['data'] != null){
                len = response['data'].length;
                // console.log(len);
            }

            if(len > 0){
                // Read data and create <option >
                for(var i=0; i<len; i++){

                    var id = response['data'][i].id;
                    var name = response['data'][i].name;

                    var option = "<option value='"+id+"'>"+name+"</option>";
                    // console.log(option);
                    $("#brandModels").append(option); 
                }
            }

        }
    });
}

// jQuery(window).load(function () {
//     getModels();
// });

// $(document).ready(function(){

//    // Department Change
//    $('#brands').change(function(){ 
//      getModels(); 
//    });
// });
</script> --}}

@endsection

@section('style')
    <style>
        

        .view-profile-button button {
            border: 1px solid #E53737;
            background-color: white;
            float: none;
            display: inline-block;
            position: relative;
            top: 4px;
            margin-left: 13px;
            color: #707070;
            border-color: #E53737;

            border-radius: 4px;
        }

        .job-title-sec>h3>a {
            color: #707070;
        }

        label{padding:3px !important; font-size: 11px}
</style>
@endsection
