@extends('layouts.website')

@section('content')
    <div class="theme-layout" id="scrollup">






        @include('includes.employer-header')
        <section>
            <div class="block no-padding">
                <div class="container">
                    <div class="row no-gape">

                        @include('includes.employer-navigation')
                        <div class="col-lg-9 column">
                            <div class="padding-left">
                                <div class="profile-title">
                                    <h3>Add a New Job</h3>


                                </div>
                                <div class="profile-form-edit">
                                    <form action="{{ '/employer/post-new-job/create' }}" method="post"
                                        autocomplete="false">
                                        @csrf
                                        @if (Session::has('success'))
                                            <div class="alert alert-success alert-dismissible">
                                                <button type="button" class="close" data-dismiss="alert">×</button>
                                                {{ Session::get('success') }}
                                            </div>
                                        @elseif(Session::has('failed'))
                                            <div class="alert alert-danger alert-dismissible">
                                                <button type="button" class="close" data-dismiss="alert">×</button>
                                                {{ Session::get('failed') }}
                                            </div>
                                        @endif
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <span class="pf-title">Job Title</span>
                                                <div class="pf-field">
                                                    <input required value="{{ old('job_title') }}" name="job_title"
                                                        type="text" placeholder="e.g. Diesel Mechanic" />
                                                    {!! $errors->first('job_title', "<span class='text-danger'>:message</span>") !!}
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <span class="pf-title">Job Categories</span>
                                                <div class="pf-field">
                                                    <select name="job_categories[]" multiple="multiple" style="position: relative;
                                                                        display: block;
                                                                        overflow: hidden;
                                                                        padding: 0 0 0 8px;
                                                                        height: 25px;
                                                                        border-radius: 8px;
                                                                        background-color: #fff;
                                                                        color: #888888;
                                                                        text-decoration: none;
                                                                        white-space: nowrap;
                                                                        line-height: 24px;
                                                                        float: left;
                                                                        width: 100%;
                                                                        text-align: left;
                                                                        padding: 0;
                                                                        padding: 14px 45px 14px 15px;
                                                                        height: auto;" data-placeholder="Job Category"
                                                        class="chosen">
                                                        @foreach ($mainJobCategories as $mainJobCategory)
                                                            <optgroup class="select2-result-selectable" label="{{$mainJobCategory->name}}">
                                                                @foreach ($mainJobCategory->children as $child)
                                                                    <option value='{{ $child->id }}'>
                                                                        {{ $child->name }}
                                                                    </option>
                                                                @endforeach
                                                            </optgroup>
                                                        @endforeach


                                                    </select>
                                                    {!! $errors->first('job_categories', "<span class='text-danger'>:message</span>") !!}
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <span class="pf-title">Job Type</span>
                                                <div class="pf-field">
                                                    <input required value="{{ old('job_type') }}" name="job_type"
                                                        type="text" placeholder="e.g. Admin position" />
                                                    {!! $errors->first('job_type', "<span class='text-danger'>:message</span>") !!}
                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                <span class="pf-title">City</span>
                                                <div class="pf-field">
                                                    <input required value="{{ old('city') }}" name="city" type="text"
                                                        placeholder="e.g. Windhoek" />
                                                    {!! $errors->first('city', "<span class='text-danger'>:message</span>") !!}

                                                </div>
                                            </div>







                                            <div style="margin-bottom: 50px; margin-top: 50px" class="col-lg-12">
                                                <button class="update-button red-button-border-filled" style=""
                                                    type="submit">Add</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>



    </div>

    <!-- Profile Sidebar -->
@endsection
@section('scripts')
@endsection

@section('style')
    <style>

    </style>
@endsection
