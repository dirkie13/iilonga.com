@extends('layouts.website')

@section('content')
    {{-- <div class="page-loading">
        <img src="public/template/images/loader.gif" alt="" />
        <span>Skip Loader</span>
    </div> --}}

    <div class="theme-layout" id="scrollup">






        @include('includes.employer-header')
        <section>
            <div class="block no-padding">
                <div class="container">
                    <div class="row no-gape">

                        @include('includes.employer-navigation')
                        <div class="col-lg-9 column">
                            <div class="padding-left">
                                <div class="profile-title">
                                    <h3>Company Profile</h3>


                                </div>
                                <div class="profile-form-edit">
                                    <form action="{{ '/employer/profile/update' }}" method="post" autocomplete="false">
                                        @csrf
                                        @if (Session::has('success'))
                                            <div class="alert alert-success alert-dismissible">
                                                <button type="button" class="close" data-dismiss="alert">×</button>
                                                {{ Session::get('success') }}
                                            </div>
                                        @elseif(Session::has('failed'))
                                            <div class="alert alert-danger alert-dismissible">
                                                <button type="button" class="close" data-dismiss="alert">×</button>
                                                {{ Session::get('failed') }}
                                            </div>
                                        @endif

                                        @if (count($errors))    
                                        <div class="row mt-3">
                                            <div class="alert alert-danger alert-dismissible">
                                                <button type="button" class="close" data-dismiss="alert">×</button>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </div>
                                        </div>
                                        @endif



                                        <div class="row">
                                            <div class="col-lg-12">
                                                <span class="pf-title">Company Name</span>
                                                <div class="pf-field">
                                                    <input value="{{ old('company_name', $user->name) }}"
                                                        name="company_name" type="text" placeholder="e.g. John" />
                                                    {!! $errors->first('company_name', "<span class='text-danger'>:message</span>") !!}
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <span class="pf-title">Since</span>
                                                <div class="pf-field">
                                                    <input value="{{ old('since', $userBusiness->since) }}" name="since"
                                                        type="date" placeholder="1 Jan 2021" />
                                                    {!! $errors->first('since', "<span class='text-danger'>:message</span>") !!}
                                                </div>
                                            </div>
                                            <div class="col-lg-2">
                                                <span class="pf-title">Team Size</span>
                                                <div class="pf-field">
                                                    <input value="{{ old('team_size', $userBusiness->team_size) }}"
                                                        style="padding-right: 0px" name="team_size" type="text"
                                                        placeholder="e.g. 4" />
                                                    {!! $errors->first('team_size', "<span class='text-danger'>:message</span>") !!}
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <span class="pf-title">Business Type</span>
                                                <div class="pf-field">
                                                    <input
                                                        value="{{ old('business_type', $userBusiness->business_type) }}"
                                                        name="business_type" type="text" placeholder="e.g. Mechanic" />
                                                    {!! $errors->first('business_type', "<span class='text-danger'>:message</span>") !!}
                                                </div>
                                            </div>

                                            <div style="margin-top: 30px" class="col-lg-12">
                                                <h4>Contact Details</h4>
                                            </div>

                                            {{-- <div class="col-lg-4">
                                                <span class="pf-title">Email Address</span>
                                                <div class="pf-field">
                                                    <input value="{{ old('email', $user->email) }}" name="email" type="email" placeholder="johndoe@email.com" />
                                                    {!! $errors->first('email', "<span class='text-danger'>:message</span>") !!}
                                                </div>
                                            </div> --}}
                                            <div class="col-lg-4">
                                                <span class="pf-title">Phone Number</span>
                                                <div class="pf-field">
                                                    <input value="{{ old('cellphone', $user->cellphone) }}"
                                                        name="cellphone" type="text" placeholder="e.g. 0811111111" />
                                                    {!! $errors->first('phone_number', "<span class='text-danger'>:message</span>") !!}
                                                </div>
                                            </div>
                                            <div class="col-lg-8">
                                                <span class="pf-title">Website</span>
                                                <div class="pf-field">
                                                    <input value="{{ old('website', $userBusiness->website) }}"
                                                        name="website" type="text" placeholder="e.g. www.website.com" />
                                                    {!! $errors->first('website', "<span class='text-danger'>:message</span>") !!}
                                                </div>
                                            </div>




                                            <div class="col-lg-6">
                                                <span class="pf-title">Country</span>
                                                <div class="pf-field">
                                                    <select name="country_id" {{-- value="{{ old('country_id', $userBusiness->country_id) }}" --}}
                                                        data-placeholder="country" class="chosen">
                                                        @foreach ($countries as $country)
                                                            <option
                                                                @if (old('country_id', $userBusiness->country_id) == $country->id) selected @elseif (old('country_id', $userBusiness->country_id) == null || old('country_id', $userBusiness->country_id) == '')@if ($country->id == 152)  selected @endif
                                                                @endif
                                                                value="{{ $country->id }}">{{ $country->name }}
                                                            </option>
                                                        @endforeach

                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <span class="pf-title">City</span>
                                                <div class="pf-field">
                                                    <input value="{{ old('city', $userBusiness->city) }}" name="city"
                                                        type="text" placeholder="e.g. Windhoek" />
                                                    {!! $errors->first('city', "<span class='text-danger'>:message</span>") !!}
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <span class="pf-title">Company Street Address</span>
                                                <div class="pf-field">
                                                    <input value="{{ old('address', $userBusiness->street_address) }}"
                                                        name="address" type="text"
                                                        placeholder="e.g. 43, Berg street, Klein Windhoek" />
                                                    {!! $errors->first('address', "<span class='text-danger'>:message</span>") !!}
                                                </div>
                                            </div>



                                            @if (Auth::check())

                                                @if (Auth::user()->userBusiness->business_type != null)

                                                    <div class="col-lg-12">
                                                        <button class="update-button red-button-border-filled" style=""
                                                            type="submit">Update</button>
                                                    </div>
                                                @else
                                                    <div class="col-lg-12">
                                                        <button class="update-button red-button-border-filled" style=""
                                                            type="submit">Update and Continue</button>
                                                    </div>
                                                @endif
                                            @endif
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>



    </div>

    <!-- Profile Sidebar -->
@endsection
@section('scripts')
@endsection

@section('style')
    <style>
        /* button:hover,
                    button:focus {
                        color: #1a1111 !important;
                        background-color: white !important;
                        border: 2px solid #FF9292;

                    } */

        .chosen-container a {

            background: none !important;

        }


        /* button {
                        
                        border: 2px solid #FF9292;

                    } */

    </style>
@endsection
