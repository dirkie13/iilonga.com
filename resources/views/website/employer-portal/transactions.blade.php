@extends('layouts.website')

@section('content')
    {{-- <div class="page-loading">
        <img src="/template/images/loader.gif" alt="" />
        <span>Skip Loader</span>
    </div> --}}

    <div class="theme-layout" id="scrollup">






        @include('includes.employer-header')
        <section>
            <div class="block no-padding">
                <div class="container">
                    <div class="row no-gape">

                        @include('includes.employer-navigation')
                        <div class="col-lg-9 column">
                            <div class="padding-left">
                                <div class="profile-title">
                                    <h3>Transactions</h3>


                                </div>

                                @if (count($tokens) == 0)
                                    
                                    <div class="m-3">
                                        <h3 style=" margin-top: 100px" style="">You have no transactions</h3>
                                    </div>
                                @endif

                                @foreach ($tokens as $token)
                                    <div style=" border-bottom: 1px solid #979797; " class="profile-form-edit row m-3 p-3">

                                        <div class=" col-lg-12 " style="margin-left: 15px; margin-right:15px">
                                            <h5>CV OF {{ $token->userCandidate->user->name }}
                                                {{ $token->userCandidate->surname }} PURCHASED ON THE
                                                {{ Carbon\Carbon::parse($token->date_used)->format('d M Y') }}</h5>
                                        </div>



                                        @if ($token->userCandidate->cv != '')
                                            <div class=" col-lg-12 m-3">
                                                <a href="{{ env('CLOUDINARY_WEBSITE_URL') }}{{ $token->userCandidate->cv }}.pdf"
                                                    download>
                                                    <button style="" class=" red-button-border-filled" style=""
                                                        type="submit">Download CV</button>
                                                </a>
                                            </div>
                                        @endif


                                        {{-- <div class=" col-lg-12 m-3">
                                            <button class="red-button-border-filled">Download CV</button>
                                        </div> --}}
                                        <div class=" col-lg-12 m-3">
                                            {{-- <button class="red-button-border-filled">View Resumé</button> --}}
                                            <button
                                                onclick="window.location.href='/employer/candidate-profile/{{ $token->job_id }}/{{ $token->userCandidate->id }}'"
                                                class="red-button-border-filled">View Profile</button>
                                        </div>


                                    </div>
                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>



    </div>

    <!-- Profile Sidebar -->
@endsection
@section('scripts')
@endsection

@section('style')
    <style>
        .profile-form-edit h5 {
            color: #979797;
            float: left;
            text-align: left;

        }


        /* .profile-form-edit button:hover {
                            background-color: #979797;
                            color: #ffffff;
                        } */


        .profile-form-edit button {


            padding: 10px;

            float: left;
        }

    </style>
@endsection
