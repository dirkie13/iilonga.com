@extends('layouts.website')

@section('content')

    {{-- <div class="page-loading">
        <img src="public/template/images/loader.gif" alt="" />
        <span>Skip Loader</span>
    </div> --}}

    <div class="theme-layout" id="scrollup">






        @include('includes.employer-header')
        <section>
            <div class="block no-padding">
                <div class="container">
                    <div class="row no-gape">

                        @include('includes.employer-navigation')
                        <div class="col-lg-9 column">
                            <div class="padding-left">
                                <div class="profile-title">
                                    <h3>Wallet</h3>


                                </div>
                                <div class="profile-form-edit row m-3">


                                    <div style="border:1px solid #979797; border-radius: 5px; padding: 10px"
                                        class="simple-text-block col-lg-12 m-3">
                                        <h3 style="" class="">Your Tokens</h3>
                                        <br><br>
                                        <h5 style="" class="">{{$tokenQuantity}} TOKENS</h5>

                                    </div>
                                    <div class=" col-lg-12 " style="margin-left: 15px; margin-right:15px">
                                        <div >
                                            <a href="wallet/purchase-tokens">
                                            <button class="purchase-more-tokens-button  red-button-border-filled">Purchase more tokens</button>
                                            </a>
                                        </div>
                                        <br><br>
                                        <div >
                                            <a href="wallet/purchase-history">
                                            <button class="transaction-history-button red-button-border-filled">Transaction History</button>
                                        </a>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>



    </div>

    <!-- Profile Sidebar -->



@endsection
@section('scripts')

@endsection

@section('style')
    <style>
        .simple-text-block h5 {
            color: #979797;
            float: left;
            /* text-align: -webkit-left; */

        }

        .simple-text-block h3 {
            color: #979797;
            float: left;
            text-align: left;

        }

        

        .purchase-more-tokens-button {
            /* background-color: #1D7DDC; */
            float: left;
         
            
            /* width: 240px; */
        }
        .purchase-more-tokens-button:focus,
        .purchase-more-tokens-button:hover {
            background-color: white;
        
            color: #1D7DDC;
           
        }

        .transaction-history-button {
            /* background-color: #0F4881; */
            float: left;
        
            /* border: 1px solid #979797;
            border-radius: 7px; */
            width: 240px;

        }
        .transaction-history-button:focus,
        .transaction-history-button:hover {
            background-color: white;
            color: #0F4881;
        
            
           

        }

    </style>
@endsection
