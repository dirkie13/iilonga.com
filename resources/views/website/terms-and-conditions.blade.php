@extends('layouts.website')

@section('content')
    <div class="page-loading">
        <img src="/template/images/loader.gif" alt="" />
        <span>Skip Loader</span>
    </div>

    <div class="theme-layout" id="scrollup">


       


        <section>
            <div class="block no-padding  gray">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="inner2">
                                <div class="inner-title2">
                                    <h3>Terms and Conditions</h3>
                                    
                                </div>
                                <div class="page-breacrumbs">
                                    <ul class="breadcrumbs">
                                        <li><a href="/" title="">Home</a></li>
                                       
                                        <li><a href="/contact-us" title="">Terms and Conditions</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>



        <section>
            <div class="block">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 column">
                            {{-- <p style='margin-top:0cm;margin-right:0cm;margin-bottom:11.25pt;margin-left:0cm;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";background:white;'><span style='font-size:14px;font-family:"Arial","sans-serif";color:#555555;'>These Terms and Conditions (&quot;the Terms and Conditions&quot;) govern your (&quot;the User&quot; / &quot;You&quot;) use of the Iilonga Namibia website located at the domain name (www.iilonga.com) (&quot;the Website&quot;). By accessing and using the Website, the User agrees to be bound by the Terms and Conditions set out in this legal notice. If the User does not wish to be bound by these Terms and Conditions, the User may not access, display, use, download, and/or otherwise copy or distribute any Content obtained from the Website. These Terms and Conditions become effective when you access the site for the first time and constitute a binding agreement between Iilonga Namibia and the User, which will always prevail. The current version of these Terms and Conditions will govern our respective rights and obligations each time you access this site.</span></p>
<p style='margin-top:15.0pt;margin-right:0cm;margin-bottom:11.25pt;margin-left:0cm;line-height:115%;font-size:15px;font-family:"Calibri","sans-serif";background:white;'><span style='font-size:34px;line-height:115%;font-family:"Arial","sans-serif";color:#3E4859;'>Online Services:</span></p>
<p style='margin-top:0cm;margin-right:0cm;margin-bottom:11.25pt;margin-left:0cm;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";background:white;'><span style='font-size:14px;font-family:"Arial","sans-serif";color:#555555;'>Iilonga Namibia&apos;s online products and services (&quot;Online Services&quot;) are subject to registration procedures and approvals, which Iilonga Namibia may accept or reject at their sole discretion. The Online Services are governed by separate terms and conditions (&quot;Online Service Terms&quot;) that are available on the relevant sections of the Website where the Online Services are provided. In the event of conflict between these Terms and Conditions and the Online Service Terms, the provisions of the Online Service Terms will prevail.</span></p>
<p style='margin-top:15.0pt;margin-right:0cm;margin-bottom:11.25pt;margin-left:0cm;line-height:115%;font-size:15px;font-family:"Calibri","sans-serif";background:white;'><span style='font-size:34px;line-height:115%;font-family:"Arial","sans-serif";color:#3E4859;'>Privacy and Security:</span></p>
<p style='margin-top:0cm;margin-right:0cm;margin-bottom:11.25pt;margin-left:0cm;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";background:white;'><span style='font-size:14px;font-family:"Arial","sans-serif";color:#555555;'>Iilonga Namibia receives various types of information (&quot;the Information&apos;&apos;) from Users who access the Website. Iilonga Namibia makes every effort to protect any Information received by it. Despite such undertaking, it is possible for Internet-based communications to be intercepted. Without the use of encryption, the Internet is not a secure medium and privacy cannot be ensured. Internet e-mail is vulnerable to interception and forging. Iilonga Namibia will not be responsible for any damages the User or any third party may suffer as a result of the transmission of confidential or personal information that the User submits to Iilonga Namibia through the Internet, or that the User expressly or implicitly authorizes Iilonga Namibia to receive, or for any errors or any changes made to any transmitted information. To ensure acquaintance with and awareness of the privacy measures and polices of Iilonga Namibia, the User is urged to read and understand Iilonga Namibia&apos;s Privacy and Security Policy, outlining Iilonga Namibia&apos;s commitment to the User&apos;s privacy and the security of their personal information.</span></p>
<p style='margin-top:15.0pt;margin-right:0cm;margin-bottom:11.25pt;margin-left:0cm;line-height:115%;font-size:15px;font-family:"Calibri","sans-serif";background:white;'><span style='font-size:34px;line-height:115%;font-family:"Arial","sans-serif";color:#3E4859;'>Updating of these Terms and Conditions:</span></p>
<p style='margin-top:0cm;margin-right:0cm;margin-bottom:11.25pt;margin-left:0cm;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";background:white;'><span style='font-size:14px;font-family:"Arial","sans-serif";color:#555555;'>Iilonga Namibia reserves the right to, amend, change, modify, add to or remove from portions or the whole of these Terms and Conditions from time to time. Changes to these Terms and Conditions will become effective upon such changes being posted to the Website. By accessing the Website You are bound to the version of the Terms and Conditions published here at the time of any visit to the Website. You agree to view the current version each time you access the Website. The User&apos;s continued use of this Website following the posting of changes or updates will be considered notice of the User&apos;s acceptance to abide by and be bound by these Terms and Conditions, including such changes or updates.</span></p>
<p style='margin-top:0cm;margin-right:0cm;margin-bottom:11.25pt;margin-left:0cm;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";background:white;'><span style='font-size:14px;font-family:"Arial","sans-serif";color:#555555;'>Iilonga Namibia reserves the right to make any changes to the Website, the Content, or to products and/or services offered through the Website at any time and without notice. A certificate signed by the administrator responsible for maintaining the Website will be prima facie proof of the date of publication and content of the current version and all previous versions of the Terms and Conditions.</span></p>
<p style='margin-top:15.0pt;margin-right:0cm;margin-bottom:11.25pt;margin-left:0cm;line-height:115%;font-size:15px;font-family:"Calibri","sans-serif";background:white;'><span style='font-size:34px;line-height:115%;font-family:"Arial","sans-serif";color:#3E4859;'>Copyright and Intellectual Property Rights:</span></p>
<p style='margin-top:0cm;margin-right:0cm;margin-bottom:11.25pt;margin-left:0cm;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";background:white;'><span style='font-size:14px;font-family:"Arial","sans-serif";color:#555555;'>Iilonga Namibia provides certain information (&ldquo;the Content&rdquo;) on the Website. Content currently or anticipated to be displayed at this Website is provided by Iilonga Namibia, its affiliates and/or subsidiary, or any other third party owners of such content, and includes but is not limited to Literary Works, Musical Works, Artistic Works, Sound Recordings, Cinematograph Films, Sound and Television Broadcasts, Program-Carrying Signals, Published Editions and Computer Programs. All such proprietary works, and the compilation of the proprietary works, are copyright of Iilonga Namibia, its affiliates or subsidiary, or any other third party owner of such rights (&ldquo;the Owners&rdquo;), and is protected by Namibia and International copyright laws. Accordingly, any unauthorized copying, reproduction, retransmission, distribution, dissemination, sale, publication, broadcast or other circulation or exploitation of the Content or any component thereof will constitute an infringement of such copyright and other intellectual property rights; save that you may use the materials or any component thereof for your own internal purposes and for the purposes of ordering service/s from Iilonga Namibia.</span></p>
<p style='margin-top:0cm;margin-right:0cm;margin-bottom:11.25pt;margin-left:0cm;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";background:white;'><span style='font-size:14px;font-family:"Arial","sans-serif";color:#555555;'>Without limiting the generality of the aforementioned, the User is authorized to view, download and copy to a local hard drive or disk, print and make copies of such printouts, provided that:</span></p>
<ul style="list-style-type: undefined;margin-left:26px;">
    <li><span style='font-family:"Arial","sans-serif";font-size:10.5pt;color:#555555;'>the Content is used exclusively for considering the use of the Online Services and for no other commercial purposes; and</span></li>
    <li><span style='font-family:"Arial","sans-serif";font-size:10.5pt;color:#555555;'>any reproduction of the proprietary Content from the Website or a portion/s of it must include Iilonga Namibia&rsquo;s copyright notice in its entirety.</span></li>
</ul>
<p style='margin-top:0cm;margin-right:0cm;margin-bottom:11.25pt;margin-left:0cm;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";background:white;'><span style='font-size:14px;font-family:"Arial","sans-serif";color:#555555;'>The trademarks, names, logos and service marks (collectively &quot;Trademarks&quot;) displayed on the Website are the registered and unregistered Trademarks of Iilonga Namibia. Nothing contained on the Website should be construed as granting any license or right to use any Trademark without the prior written permission of Iilonga Namibia. Except as specified in these Terms and Conditions, the User is not granted a license or any other right including without limitation under Copyright, Trademark, Patent or other Intellectual Property Rights in or to the Content and Trademarks. All rights in and to the Content and Trademarks are reserved and retained by Iilonga Namibia and/or the Owners, as the case may be.</span></p>
<p style='margin-top:0cm;margin-right:0cm;margin-bottom:11.25pt;margin-left:0cm;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";background:white;'><span style='font-size:14px;font-family:"Arial","sans-serif";color:#555555;'>Irrespective of the existence of Copyright, the User acknowledges that Iilonga Namibia and/or the Owners, as the case may be are the proprietors of all the Content and Trademarks on the Website, whether it constitutes confidential information or not, and that the User has no right, title or interest in any such material.</span></p>
<p style='margin-top:15.0pt;margin-right:0cm;margin-bottom:11.25pt;margin-left:0cm;line-height:115%;font-size:15px;font-family:"Calibri","sans-serif";background:white;'><span style='font-size:34px;line-height:115%;font-family:"Arial","sans-serif";color:#3E4859;'>Limited License to General Users:</span></p>
<ul style="list-style-type: undefined;margin-left:26px;">
    <li><span style='font-family:"Arial","sans-serif";font-size:10.5pt;color:#555555;'>Iilonga Namibia grants to the User, subject to the further terms of these Terms and Conditions, a non-exclusive, non-transferable, limited and revocable right to access, display, use, download and otherwise copy the current and future Content for personal, non-commercial and information purposes only.</span></li>
    <li><span style='font-family:"Arial","sans-serif";font-size:10.5pt;color:#555555;'>This Website and the Content may not be reproduced, duplicated, copied, resold, visited or otherwise exploited for any commercial purpose without the express prior written consent of Iilonga Namibia.</span></li>
    <li><span style='font-family:"Arial","sans-serif";font-size:10.5pt;color:#555555;'>The license does not allow the User to collect product or service listings, descriptions or other information displayed here, and does not allow any derivative use of this Website or the Content for the benefit of another merchant.</span></li>
    <li><span style='font-family:"Arial","sans-serif";font-size:10.5pt;color:#555555;'>The User may not frame nor use framing technologies to enclose the Website or the Content nor any part thereof without the express written consent of Iilonga Namibia</span></li>
    <li><span style='font-family:"Arial","sans-serif";font-size:10.5pt;color:#555555;'>The User is restricted to use the Website and Content, only for lawful purposes and warrants that he/she shall not:</span>
        <ol style="list-style-type: circle;">
            <li><span style='font-family:"Arial","sans-serif";font-size:10.5pt;color:#555555;'>use the Website to transmit material which is in violation of any law or regulation, which is obscene, threatening, racist, menacing, offensive, defamatory, in breach of an duty of confidence, in breach of any intellectual property rights, or otherwise objectionable or unlawful; and</span></li>
            <li><span style='font-family:"Arial","sans-serif";font-size:10.5pt;color:#555555;'>other than for personal and non-commercial use, store on a computer, or print copies of extracts from the Website, and other than for personal and non- commercial use, &quot;mirror&quot; or cache any of the Content of Website on a server, or copy, adapt, modify or re-use the text or graphics obtained from the Website, without the prior written permission of Iilonga Namibia.</span></li>
        </ol>
    </li>
    <li><span style='font-family:"Arial","sans-serif";font-size:10.5pt;color:#555555;'>Iilonga Namibia does not offer products or services to minors. If you are under the age of 18, you may not respond to or otherwise accept or act upon any offers in the Website.</span></li>
    <li><span style='font-family:"Arial","sans-serif";font-size:10.5pt;color:#555555;'>Iilonga Namibia, their affiliates or subsidiaries reserve the right to refuse service, terminate accounts, remove or edit Content, or cancel orders at their sole discretion.</span></li>
    <li><span style='font-family:"Arial","sans-serif";font-size:10.5pt;color:#555555;'>Any unauthorized use terminates this license.</span></li>
</ul>
<p style='margin-top:15.0pt;margin-right:0cm;margin-bottom:11.25pt;margin-left:0cm;line-height:115%;font-size:15px;font-family:"Calibri","sans-serif";background:white;'><span style='font-size:34px;line-height:115%;font-family:"Arial","sans-serif";color:#3E4859;'>Permission for Hyperlinks, Deep Linking, Crawlers and Meta Tags:</span></p>
<p style='margin-top:0cm;margin-right:0cm;margin-bottom:11.25pt;margin-left:0cm;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";background:white;'><span style='font-size:14px;font-family:"Arial","sans-serif";color:#555555;'>No User may establish a hyperlink, frame, metatag or similar reference, whether electronically or otherwise (collectively referred to as &ldquo;Linking&rdquo;), to the Website or any subsidiary pages before receiving Iilonga Namibia&rsquo;s prior written approval, which may be withheld or granted subject to the conditions Iilonga Namibia may specify from time to time.</span></p>
<p style='margin-top:0cm;margin-right:0cm;margin-bottom:11.25pt;margin-left:0cm;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";background:white;'><span style='font-size:14px;font-family:"Arial","sans-serif";color:#555555;'>An application for Linking must be submitted via the Contact Us page at www.iilonga.com. Once received Iilonga Namibia will do its best to respond and enter into further discussions with the User. If the User does not receive a written response from Iilonga Namibia within five business days, the User must consider the request as having been rejected.</span></p>
<p style='margin-top:0cm;margin-right:0cm;margin-bottom:11.25pt;margin-left:0cm;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";background:white;'><span style='font-size:14px;font-family:"Arial","sans-serif";color:#555555;'>A breach of this provision entitles Iilonga Namibia to take legal action without prior notice to the User and the User agrees to reimburse Iilonga Namibia with the costs associated with such legal action, on an attorney and own client scale.</span></p>
<p style='margin-top:15.0pt;margin-right:0cm;margin-bottom:11.25pt;margin-left:0cm;line-height:115%;font-size:15px;font-family:"Calibri","sans-serif";background:white;'><span style='font-size:34px;line-height:115%;font-family:"Arial","sans-serif";color:#3E4859;'>Hyperlinks:</span></p>
<p style='margin-top:0cm;margin-right:0cm;margin-bottom:11.25pt;margin-left:0cm;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";background:white;'><span style='font-size:14px;font-family:"Arial","sans-serif";color:#555555;'>Notwithstanding the fact that hyperlinks exist, in the Terms and Conditions, to facilitate access to notices, policies and legislation that are incorporated into the Terms and Conditions, the User agrees that in those instances, where some or all of the hyperlinks malfunction or are not operational, such occurrence shall not affect the validity or enforceability of the Terms and Conditions. The User undertakes to, at their own convenience and discretion, review and acquaints themselves with necessary documents and/ or terms.</span></p>
<p style='margin-top:15.0pt;margin-right:0cm;margin-bottom:11.25pt;margin-left:0cm;line-height:115%;font-size:15px;font-family:"Calibri","sans-serif";background:white;'><span style='font-size:34px;line-height:115%;font-family:"Arial","sans-serif";color:#3E4859;'>External Links:</span></p>
<p style='margin-top:0cm;margin-right:0cm;margin-bottom:11.25pt;margin-left:0cm;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";background:white;'><span style='font-size:14px;font-family:"Arial","sans-serif";color:#555555;'>External links may be provided for your convenience, but they are beyond the control of Iilonga Namibia and no representation is made as to their content. Use or reliance on any external links provided is at the User&rsquo;s own risk. When visiting external links you must refer to that external website&rsquo;s terms and conditions of use. No hypertext links shall be created from any website controlled by you or otherwise to the Website without the express prior written permission of Iilonga Namibia.</span></p>
<p style='margin-top:15.0pt;margin-right:0cm;margin-bottom:11.25pt;margin-left:0cm;line-height:115%;font-size:15px;font-family:"Calibri","sans-serif";background:white;'><span style='font-size:34px;line-height:115%;font-family:"Arial","sans-serif";color:#3E4859;'>Crawlers and Spiders:</span></p>
<p style='margin-top:0cm;margin-right:0cm;margin-bottom:11.25pt;margin-left:0cm;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";background:white;'><span style='font-size:14px;font-family:"Arial","sans-serif";color:#555555;'>The User undertakes not to use any technology to search and /or gain information from the Website without Iilonga Namibia&rsquo;s written consent.</span></p>
<p style='margin-top:15.0pt;margin-right:0cm;margin-bottom:11.25pt;margin-left:0cm;line-height:115%;font-size:15px;font-family:"Calibri","sans-serif";background:white;'><span style='font-size:34px;line-height:115%;font-family:"Arial","sans-serif";color:#3E4859;'>Software Downloads:</span></p>
<p style='margin-top:0cm;margin-right:0cm;margin-bottom:11.25pt;margin-left:0cm;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";background:white;'><span style='font-size:14px;font-family:"Arial","sans-serif";color:#555555;'>Software, if any, made available for download on or via the Website may be governed by license conditions that establish a legal relationship with the licensor. The User indemnifies Iilonga Namibia against any breach of these license conditions. Iilonga Namibia gives no warranty and makes no representation, whether express or implied, as to the quality or fitness for purpose of the use of such software.</span></p>
<p style='margin-top:0cm;margin-right:0cm;margin-bottom:11.25pt;margin-left:0cm;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";background:white;'><span style='font-size:14px;font-family:"Arial","sans-serif";color:#555555;'>No warranty, whether express or implied, is given that any files, downloads or applications available via the Website is free of viruses, Trojans, bombs, time-locks or any other data or code which has the ability to corrupt or affect the operation of the User&rsquo;s computer, database, network or other information system.</span></p>
<p style='margin-top:15.0pt;margin-right:0cm;margin-bottom:11.25pt;margin-left:0cm;line-height:115%;font-size:15px;font-family:"Calibri","sans-serif";background:white;'><span style='font-size:34px;line-height:115%;font-family:"Arial","sans-serif";color:#3E4859;'>Termination, Suspension and Limitation:</span></p>
<p style='margin-top:0cm;margin-right:0cm;margin-bottom:11.25pt;margin-left:0cm;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";background:white;'><span style='font-size:14px;font-family:"Arial","sans-serif";color:#555555;'>Iilonga Namibia reserves the right to:</span></p>
<ul style="list-style-type: undefined;margin-left:26px;">
    <li><span style='font-family:"Arial","sans-serif";font-size:10.5pt;color:#555555;'>modify, suspend or discontinue the Website, whether temporarily or permanently, without notice;</span></li>
    <li><span style='font-family:"Arial","sans-serif";font-size:10.5pt;color:#555555;'>impose limits or conditions on certain services, features or functions; and</span></li>
    <li><span style='font-family:"Arial","sans-serif";font-size:10.5pt;color:#555555;'>restrict access to parts of or all of the services on the Website.</span></li>
</ul>
<p style='margin-top:15.0pt;margin-right:0cm;margin-bottom:11.25pt;margin-left:0cm;line-height:115%;font-size:15px;font-family:"Calibri","sans-serif";background:white;'><span style='font-size:34px;line-height:115%;font-family:"Arial","sans-serif";color:#3E4859;'>Disclaimer and Limitation of Liability:</span></p>
<p style='margin-top:0cm;margin-right:0cm;margin-bottom:11.25pt;margin-left:0cm;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";background:white;'><span style='font-size:14px;font-family:"Arial","sans-serif";color:#555555;'>Although Iilonga Namibia has taken reasonable care to ensure that the Content on the Website is accurate and that the User will not suffer loss or damage as a result of their use of the Website, the Website is provided on an &ldquo;as is&rdquo; basis. Use of the Website is entirely at the User&rsquo;s own risk. The User assumes full responsibility for any loss or damage resulting from their use of the Website and their reliance on any of the Content or a part/s thereof, contained on the Website.</span></p>
<p style='margin-top:0cm;margin-right:0cm;margin-bottom:11.25pt;margin-left:0cm;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";background:white;'><span style='font-size:14px;font-family:"Arial","sans-serif";color:#555555;'>The Website and all Content on the Website, including any current or future offer of products or services, are provided on an &ldquo;as is&rdquo; basis, and may include inaccuracies or typographical errors. Iilonga Namibia makes no warranty or representation as to the availability, accuracy or completeness of the Content. Neither Iilonga Namibia nor any holding company, affiliate or subsidiary of Iilonga Namibia, shall be held liable for any direct or indirect, special, consequential or other damage of any kind whatsoever suffered or incurred, related to the use of, or the inability to access or use the Content or the Website or any functionality thereof, or of any linked website, even if Iilonga Namibia is expressly advised thereof.</span></p>
<p style='margin-top:0cm;margin-right:0cm;margin-bottom:11.25pt;margin-left:0cm;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";background:white;'><span style='font-size:14px;font-family:"Arial","sans-serif";color:#555555;'>Without derogating from the generality of the above, Iilonga Namibia will not be liable for:</span></p>
<ul style="list-style-type: undefined;margin-left:26px;">
    <li><span style='font-family:"Arial","sans-serif";font-size:10.5pt;color:#555555;'>any interruption, malfunction, downtime or other failure of the Website or related database, system or network, for whatever reason;</span></li>
    <li><span style='font-family:"Arial","sans-serif";font-size:10.5pt;color:#555555;'>any loss or damage in respect of customer data or other data, directly or indirectly, caused as a result of any malfunction of the Website or related database, system or network, power failures, unlawful access to or theft of data, computer viruses or destructive code on Iilonga Namibia&rsquo;s Website or related database, system or network, programming defects or negligence; or</span></li>
    <li><span style='font-family:"Arial","sans-serif";font-size:10.5pt;color:#555555;'>any event over which Iilonga Namibia has no direct control.</span></li>
</ul>
<p style='margin-top:15.0pt;margin-right:0cm;margin-bottom:11.25pt;margin-left:0cm;line-height:115%;font-size:15px;font-family:"Calibri","sans-serif";background:white;'><span style='font-size:34px;line-height:115%;font-family:"Arial","sans-serif";color:#3E4859;'>Indemnity:</span></p>
<p style='margin-top:0cm;margin-right:0cm;margin-bottom:11.25pt;margin-left:0cm;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";background:white;'><span style='font-size:14px;font-family:"Arial","sans-serif";color:#555555;'>The User unconditionally and irrevocably indemnifies and holds Iilonga Namibia harmless against all and any loss, liability, actions, lawsuits, proceedings, costs, demands and damages of all and every kind, (including direct, indirect, special or consequential damages), and whether in an action based on contract, negligence or any other action, arising out of or in connection with the failure or delay in the performance of the services offered on the Website, the use of the services offered on the Website, the Content available on the Website or any other matter, directly or indirectly, related to the User&rsquo;s use of the Website, whether due to Iilonga Namibia&rsquo;s negligence or not.</span></p>
<p style='margin-top:15.0pt;margin-right:0cm;margin-bottom:11.25pt;margin-left:0cm;line-height:115%;font-size:15px;font-family:"Calibri","sans-serif";background:white;'><span style='font-size:34px;line-height:115%;font-family:"Arial","sans-serif";color:#3E4859;'>Choice of Law:</span></p>
<p style='margin-top:0cm;margin-right:0cm;margin-bottom:11.25pt;margin-left:0cm;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";background:white;'><span style='font-size:14px;font-family:"Arial","sans-serif";color:#555555;'>This Website is controlled, operated and administered by Iilonga Namibia from its offices as set out below within the Republic of Namibia. Iilonga Namibia makes no representation that the Content is appropriate or available for use in any other locations or countries. Access to the Website from territories or countries where the Content is illegal, is prohibited. The User may not use this Website in violation of Namibian export laws and regulations. If the User accesses this Website from locations outside of Namibia, that User is responsible for compliance with all local laws. These Terms and Conditions shall be governed by the laws of the Republic of Namibia and the User consents to the Namibian jurisdiction, in the event of any dispute.</span></p>
<p style='margin-top:0cm;margin-right:0cm;margin-bottom:11.25pt;margin-left:0cm;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";background:white;'><span style='font-size:14px;font-family:"Arial","sans-serif";color:#555555;'>If any of the provisions of these Terms and Conditions are found by a court of competent jurisdiction to be invalid or unenforceable, that provision shall be enforced to the maximum extent permissible so as to give effect to the intent of these Terms and Conditions, and the remainder of these Terms and Conditions shall continue in full force and effect.</span></p>
<p style='margin-top:15.0pt;margin-right:0cm;margin-bottom:11.25pt;margin-left:0cm;line-height:115%;font-size:15px;font-family:"Calibri","sans-serif";background:white;'><span style='font-size:34px;line-height:115%;font-family:"Arial","sans-serif";color:#3E4859;'>General:</span></p>
<p style='margin-top:0cm;margin-right:0cm;margin-bottom:11.25pt;margin-left:0cm;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";background:white;'><span style='font-size:14px;font-family:"Arial","sans-serif";color:#555555;'>The headings of the clauses in the Terms and Conditions are provided for convenience and ease of reference only and will not be used to interpret, modify or amplify the terms of the conditions.</span></p>
<p style='margin-top:0cm;margin-right:0cm;margin-bottom:11.25pt;margin-left:0cm;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";background:white;'><span style='font-size:14px;font-family:"Arial","sans-serif";color:#555555;'>The Terms and Conditions are severable, in that if any provision is determined to be illegal or unenforceable by any court of competent jurisdiction, then such provision shall be deemed to have been deleted without affecting the remaining provisions of the Terms and Conditions.</span></p>
<p style='margin-top:0cm;margin-right:0cm;margin-bottom:11.25pt;margin-left:0cm;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";background:white;'><span style='font-size:14px;font-family:"Arial","sans-serif";color:#555555;'>Iilonga Namibia&rsquo;s failure or delay to exercise any particular right or provision of the Terms and Conditions shall not constitute a waiver of such right or provision, whether this is done expressly or implied, nor will it affect the validity of any part these Terms and Conditions or prejudice Iilonga Namibia&rsquo;s right to take subsequent action against the User, unless acknowledged and agreed to by Iilonga Namibia in writing.</span></p>
<p style='margin-top:0cm;margin-right:0cm;margin-bottom:11.25pt;margin-left:0cm;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";background:white;'><span style='font-size:14px;font-family:"Arial","sans-serif";color:#555555;'>Neither the User nor Iilonga Namibia, shall be bound by any express, tacit or implied representation, warranty, promise or the like not recorded herein. These Terms and Conditions supersede and replace all prior commitments, undertakings or representations, whether written or oral, between the User and Iilonga Namibia in respect of the subject matter hereof.</span></p>
<p style='margin-top:0cm;margin-right:0cm;margin-bottom:11.25pt;margin-left:0cm;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";background:white;'><span style='font-size:14px;font-family:"Arial","sans-serif";color:#555555;'>Iilonga Namibia shall be entitled to cede, assign and delegate all or any of its rights and obligations in terms of these Terms and Conditions.</span></p>
<p style='margin-top:0cm;margin-right:0cm;margin-bottom:11.25pt;margin-left:0cm;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";background:white;'><span style='font-size:14px;font-family:"Arial","sans-serif";color:#555555;'>Should Iilonga Namibia be prevented from fulfilling any of its obligations to you as a result of any event of force majeure, then those obligations shall be deemed to have been suspended to the extent that and for as long as Iilonga Namibia is so prevented from fulfilling them and your corresponding obligations shall be suspended to the corresponding extent. In the event that force majeure continues for more than fourteen days after it has fist occurred then Iilonga Namibia shall be entitled (but not obliged) to terminate all of its rights and obligations in terms of or arising out of these Terms and Conditions by giving notice to the User. An &quot;event of force majeure&quot; shall mean any event or circumstance whatsoever which is not within the reasonable control of including, without limitation, vis major, casus fortuitus, any act of God, strike, theft, riots, explosion, insurrection or other similar disorder, war (whether declared or not) or military operations, the downtime of any external telecommunications line, power failure, international restrictions, any requirement of any international authority, any requirement of any government or other competent local authority, any court order, export control or shortage of transport facilities</span></p>
<p style='margin-top:0cm;margin-right:0cm;margin-bottom:11.25pt;margin-left:0cm;line-height:normal;font-size:15px;font-family:"Calibri","sans-serif";background:white;'><span style='font-size:14px;font-family:"Arial","sans-serif";color:#555555;'>The Terms and Conditions, as varied by Iilonga Namibia from time to time, constitutes the entire and sole agreement between Iilonga Namibia and the User with regard to the use of the Content and this Website.</span></p>
<p style='margin-top:0cm;margin-right:0cm;margin-bottom:10.0pt;margin-left:0cm;line-height:115%;font-size:15px;font-family:"Calibri","sans-serif";'>&nbsp;</p> --}}


                            <p>
                                <a name="_gjdgxs"></a>
                                These Terms and Conditions ("the Terms and Conditions") govern your ("the
                                User" / "You") use of the Iilonga Namibia website located at the domain
                                name (www.iilonga.com) ("the Website"). By accessing and using the Website,
                                the User agrees to be bound by the Terms and Conditions set out in this
                                legal notice. If the User does not wish to be bound by these Terms and
                                Conditions, the User may not access, display, use, download, and/or
                                otherwise copy or distribute any Content obtained from the Website. These
                                Terms and Conditions become effective when you access the site for the
                                first time and constitute a binding agreement between Iilonga Namibia and
                                the User, which will always prevail. The current version of these Terms and
                                Conditions will govern our respective rights and obligations each time you
                                access this site.
                            </p>
                            <h3>
                                Online Services:
                            </h3>
                            <p>
                                Iilonga Namibia's online products and services ("Online Services") are
                                subject to registration procedures and approvals, which Iilonga Namibia may
                                accept or reject at their sole discretion. The Online Services are governed
                                by separate terms and conditions ("Online Service Terms") that are
                                available on the relevant sections of the Website where the Online Services
                                are provided. In the event of conflict between these Terms and Conditions
                                and the Online Service Terms, the provisions of the Online Service Terms
                                will prevail.
                            </p>
                            <h3>
                                Privacy and Security:
                            </h3>
                            <p>
                                Iilonga Namibia receives various types of information ("the Information'')
                                from Users who access the Website. Iilonga Namibia makes every effort to
                                protect any Information received by it. Despite such undertaking, it is
                                possible for Internet-based communications to be intercepted. Without the
                                use of encryption, the Internet is not a secure medium and privacy cannot
                                be ensured. Internet e-mail is vulnerable to interception and forging.
                                Iilonga Namibia will not be responsible for any damages the User or any
                                third party may suffer as a result of the transmission of confidential or
                                personal information that the User submits to Iilonga Namibia through the
                                Internet, or that the User expressly or implicitly authorizes Iilonga
                                Namibia to receive, or for any errors or any changes made to any
                                transmitted information. To ensure acquaintance with and awareness of the
                                privacy measures and polices of Iilonga Namibia, the User is urged to read
                                and understand Iilonga Namibia's Privacy and Security Policy, outlining
                                Iilonga Namibia's commitment to the User's privacy and the security of
                                their personal information.
                            </p>
                            <h3>
                                Updating of these Terms and Conditions:
                            </h3>
                            <p>
                                Iilonga Namibia reserves the right to, amend, change, modify, add to or
                                remove from portions or the whole of these Terms and Conditions from time
                                to time. Changes to these Terms and Conditions will become effective upon
                                such changes being posted to the Website. By accessing the Website You are
                                bound to the version of the Terms and Conditions published here at the time
                                of any visit to the Website. You agree to view the current version each
                                time you access the Website. The User's continued use of this Website
                                following the posting of changes or updates will be considered notice of
                                the User's acceptance to abide by and be bound by these Terms and
                                Conditions, including such changes or updates.
                            </p>
                            <p>
                                Iilonga Namibia reserves the right to make any changes to the Website, the
                                Content, or to products and/or services offered through the Website at any
                                time and without notice. A certificate signed by the administrator
                                responsible for maintaining the Website will be prima facie proof of the
                                date of publication and content of the current version and all previous
                                versions of the Terms and Conditions.
                            </p>
                            <h3>
                                Copyright and Intellectual Property Rights:
                            </h3>
                            <p>
                                Iilonga Namibia provides certain information (“the Content”) on the
                                Website. Content currently or anticipated to be displayed at this Website
                                is provided by Iilonga Namibia, its affiliates and/or subsidiary, or any
                                other third party owners of such content, and includes but is not limited
                                to Literary Works, Musical Works, Artistic Works, Sound Recordings,
                                Cinematograph Films, Sound and Television Broadcasts, Program-Carrying
                                Signals, Published Editions and Computer Programs. All such proprietary
                                works, and the compilation of the proprietary works, are copyright of
                                Iilonga Namibia, its affiliates or subsidiary, or any other third party
                                owner of such rights (“the Owners”), and is protected by Namibia and
                                International copyright laws. Accordingly, any unauthorized copying,
                                reproduction, retransmission, distribution, dissemination, sale,
                                publication, broadcast or other circulation or exploitation of the Content
                                or any component thereof will constitute an infringement of such copyright
                                and other intellectual property rights; save that you may use the materials
                                or any component thereof for your own internal purposes and for the
                                purposes of ordering service/s from Iilonga Namibia.
                            </p>
                            <p>
                                Without limiting the generality of the aforementioned, the User is
                                authorized to view, download and copy to a local hard drive or disk, print
                                and make copies of such printouts, provided that:
                            </p>
                            <p>
                                ● the Content is used exclusively for considering the use of the Online
                                Services and for no other commercial purposes; and
                            </p>
                            <p>
                                ● any reproduction of the proprietary Content from the Website or a
                                portion/s of it must include Iilonga Namibia’s copyright notice in its
                                entirety.
                            </p>
                            <p>
                                The trademarks, names, logos and service marks (collectively "Trademarks")
                                displayed on the Website are the registered and unregistered Trademarks of
                                Iilonga Namibia. Nothing contained on the Website should be construed as
                                granting any license or right to use any Trademark without the prior
                                written permission of Iilonga Namibia. Except as specified in these Terms
                                and Conditions, the User is not granted a license or any other right
                                including without limitation under Copyright, Trademark, Patent or other
                                Intellectual Property Rights in or to the Content and Trademarks. All
                                rights in and to the Content and Trademarks are reserved and retained by
                                Iilonga Namibia and/or the Owners, as the case may be.
                            </p>
                            <p>
                                Irrespective of the existence of Copyright, the User acknowledges that
                                Iilonga Namibia and/or the Owners, as the case may be are the proprietors
                                of all the Content and Trademarks on the Website, whether it constitutes
                                confidential information or not, and that the User has no right, title or
                                interest in any such material.
                            </p>
                            <h3>
                                Limited License to General Users:
                            </h3>
                            <p>
                                ● Iilonga Namibia grants to the User, subject to the further terms of these
                                Terms and Conditions, a non-exclusive, non-transferable, limited and
                                revocable right to access, display, use, download and otherwise copy the
                                current and future Content for personal, non-commercial and information
                                purposes only.
                            </p>
                            <p>
                                ● This Website and the Content may not be reproduced, duplicated, copied,
                                resold, visited or otherwise exploited for any commercial purpose without
                                the express prior written consent of Iilonga Namibia.
                            </p>
                            <p>
                                ● The license does not allow the User to collect product or service
                                listings, descriptions or other information displayed here, and does not
                                allow any derivative use of this Website or the Content for the benefit of
                                another merchant.
                            </p>
                            <p>
                                ● The User may not frame nor use framing technologies to enclose the
                                Website or the Content nor any part thereof without the express written
                                consent of Iilonga Namibia
                            </p>
                            <p>
                                ● The User is restricted to use the Website and Content, only for lawful
                                purposes and warrants that he/she shall not:
                            </p>
                            <p>
                                o use the Website to transmit material which is in violation of any law or
                                regulation, which is obscene, threatening, racist, menacing, offensive,
                                defamatory, in breach of an duty of confidence, in breach of any
                                intellectual property rights, or otherwise objectionable or unlawful; and
                            </p>
                            <p>
                                o other than for personal and non-commercial use, store on a computer, or
                                print copies of extracts from the Website, and other than for personal and
                                non- commercial use, "mirror" or cache any of the Content of Website on a
                                server, or copy, adapt, modify or re-use the text or graphics obtained from
                                the Website, without the prior written permission of Iilonga Namibia.
                            </p>
                            <p>
                                ● Iilonga Namibia does not offer products or services to minors. If you are
                                under the age of 18, you may not respond to or otherwise accept or act upon
                                any offers in the Website.
                            </p>
                            <p>
                                ● Iilonga Namibia, their affiliates or subsidiaries reserve the right to
                                refuse service, terminate accounts, remove or edit Content, or cancel
                                orders at their sole discretion.
                            </p>
                            <p>
                                ● Any unauthorized use terminates this license.
                            </p>
                            <h3>
                                Permission for Hyperlinks, Deep Linking, Crawlers and Meta Tags:
                            </h3>
                            <p>
                                No User may establish a hyperlink, frame, metatag or similar reference,
                                whether electronically or otherwise (collectively referred to as
                                “Linking”), to the Website or any subsidiary pages before receiving Iilonga
                                Namibia’s prior written approval, which may be withheld or granted subject
                                to the conditions Iilonga Namibia may specify from time to time.
                            </p>
                            <p>
                                An application for Linking must be submitted via the Contact Us page at
                                www.iilonga.com. Once received Iilonga Namibia will do its best to respond
                                and enter into further discussions with the User. If the User does not
                                receive a written response from Iilonga Namibia within five business days,
                                the User must consider the request as having been rejected.
                            </p>
                            <p>
                                A breach of this provision entitles Iilonga Namibia to take legal action
                                without prior notice to the User and the User agrees to reimburse Iilonga
                                Namibia with the costs associated with such legal action, on an attorney
                                and own client scale.
                            </p>
                            <h3>
                                Hyperlinks:
                            </h3>
                            <p>
                                Notwithstanding the fact that hyperlinks exist, in the Terms and
                                Conditions, to facilitate access to notices, policies and legislation that
                                are incorporated into the Terms and Conditions, the User agrees that in
                                those instances, where some or all of the hyperlinks malfunction or are not
                                operational, such occurrence shall not affect the validity or
                                enforceability of the Terms and Conditions. The User undertakes to, at
                                their own convenience and discretion, review and acquaints themselves with
                                necessary documents and/ or terms.
                            </p>
                            <h3>
                                External Links:
                            </h3>
                            <p>
                                External links may be provided for your convenience, but they are beyond
                                the control of Iilonga Namibia and no representation is made as to their
                                content. Use or reliance on any external links provided is at the User’s
                                own risk. When visiting external links you must refer to that external
                                website’s terms and conditions of use. No hypertext links shall be created
                                from any website controlled by you or otherwise to the Website without the
                                express prior written permission of Iilonga Namibia.
                            </p>
                            <h3>
                                Crawlers and Spiders:
                            </h3>
                            <p>
                                The User undertakes not to use any technology to search and /or gain
                                information from the Website without Iilonga Namibia’s written consent.
                            </p>
                            <h3>
                                Software Downloads:
                            </h3>
                            <p>
                                Software, if any, made available for download on or via the Website may be
                                governed by license conditions that establish a legal relationship with the
                                licensor. The User indemnifies Iilonga Namibia against any breach of these
                                license conditions. Iilonga Namibia gives no warranty and makes no
                                representation, whether express or implied, as to the quality or fitness
                                for purpose of the use of such software.
                            </p>
                            <p>
                                No warranty, whether express or implied, is given that any files, downloads
                                or applications available via the Website is free of viruses, Trojans,
                                bombs, time-locks or any other data or code which has the ability to
                                corrupt or affect the operation of the User’s computer, database, network
                                or other information system.
                            </p>
                            <h3>
                                Termination, Suspension and Limitation:
                            </h3>
                            <p>
                                Iilonga Namibia reserves the right to:
                            </p>
                            <p>
                                ● modify, suspend or discontinue the Website, whether temporarily or
                                permanently, without notice;
                            </p>
                            <p>
                                ● impose limits or conditions on certain services, features or functions;
                                and
                            </p>
                            <p>
                                ● restrict access to parts of or all of the services on the Website.
                            </p>
                            <h3>
                                Disclaimer and Limitation of Liability:
                            </h3>
                            <p>
                                Although Iilonga Namibia has taken reasonable care to ensure that the
                                Content on the Website is accurate and that the User will not suffer loss
                                or damage as a result of their use of the Website, the Website is provided
                                on an “as is” basis. Use of the Website is entirely at the User’s own risk.
                                The User assumes full responsibility for any loss or damage resulting from
                                their use of the Website and their reliance on any of the Content or a
                                part/s thereof, contained on the Website.
                            </p>
                            <p>
                                The Website and all Content on the Website, including any current or future
                                offer of products or services, are provided on an “as is” basis, and may
                                include inaccuracies or typographical errors. Iilonga Namibia makes no
                                warranty or representation as to the availability, accuracy or completeness
                                of the Content. Neither Iilonga Namibia nor any holding company, affiliate
                                or subsidiary of Iilonga Namibia, shall be held liable for any direct or
                                indirect, special, consequential or other damage of any kind whatsoever
                                suffered or incurred, related to the use of, or the inability to access or
                                use the Content or the Website or any functionality thereof, or of any
                                linked website, even if Iilonga Namibia is expressly advised thereof.
                            </p>
                            <p>
                                Without derogating from the generality of the above, Iilonga Namibia will
                                not be liable for:
                            </p>
                            <p>
                                ● any interruption, malfunction, downtime or other failure of the Website
                                or related database, system or network, for whatever reason;
                            </p>
                            <p>
                                ● any loss or damage in respect of customer data or other data, directly or
                                indirectly, caused as a result of any malfunction of the Website or related
                                database, system or network, power failures, unlawful access to or theft of
                                data, computer viruses or destructive code on Iilonga Namibia’s Website or
                                related database, system or network, programming defects or negligence; or
                            </p>
                            <p>
                                ● any event over which Iilonga Namibia has no direct control.
                            </p>
                            <h3>
                                Indemnity:
                            </h3>
                            <p>
                                The User unconditionally and irrevocably indemnifies and holds Iilonga
                                Namibia harmless against all and any loss, liability, actions, lawsuits,
                                proceedings, costs, demands and damages of all and every kind, (including
                                direct, indirect, special or consequential damages), and whether in an
                                action based on contract, negligence or any other action, arising out of or
                                in connection with the failure or delay in the performance of the services
                                offered on the Website, the use of the services offered on the Website, the
                                Content available on the Website or any other matter, directly or
                                indirectly, related to the User’s use of the Website, whether due to
                                Iilonga Namibia’s negligence or not.
                            </p>
                            <h3>
                                Choice of Law:
                            </h3>
                            <p>
                                This Website is controlled, operated and administered by Iilonga Namibia
                                from its offices as set out below within the Republic of Namibia. Iilonga
                                Namibia makes no representation that the Content is appropriate or
                                available for use in any other locations or countries. Access to the
                                Website from territories or countries where the Content is illegal, is
                                prohibited. The User may not use this Website in violation of Namibian
                                export laws and regulations. If the User accesses this Website from
                                locations outside of Namibia, that User is responsible for compliance with
                                all local laws. These Terms and Conditions shall be governed by the laws of
                                the Republic of Namibia and the User consents to the Namibian jurisdiction,
                                in the event of any dispute.
                            </p>
                            <p>
                                If any of the provisions of these Terms and Conditions are found by a court
                                of competent jurisdiction to be invalid or unenforceable, that provision
                                shall be enforced to the maximum extent permissible so as to give effect to
                                the intent of these Terms and Conditions, and the remainder of these Terms
                                and Conditions shall continue in full force and effect.
                            </p>
                            <h3>
                                General:
                            </h3>
                            <p>
                                The headings of the clauses in the Terms and Conditions are provided for
                                convenience and ease of reference only and will not be used to interpret,
                                modify or amplify the terms of the conditions.
                            </p>
                            <p>
                                The Terms and Conditions are severable, in that if any provision is
                                determined to be illegal or unenforceable by any court of competent
                                jurisdiction, then such provision shall be deemed to have been deleted
                                without affecting the remaining provisions of the Terms and Conditions.
                            </p>
                            <p>
                                Iilonga Namibia’s failure or delay to exercise any particular right or
                                provision of the Terms and Conditions shall not constitute a waiver of such
                                right or provision, whether this is done expressly or implied, nor will it
                                affect the validity of any part these Terms and Conditions or prejudice
                                Iilonga Namibia’s right to take subsequent action against the User, unless
                                acknowledged and agreed to by Iilonga Namibia in writing.
                            </p>
                            <p>
                                Neither the User nor Iilonga Namibia, shall be bound by any express, tacit
                                or implied representation, warranty, promise or the like not recorded
                                herein. These Terms and Conditions supersede and replace all prior
                                commitments, undertakings or representations, whether written or oral,
                                between the User and Iilonga Namibia in respect of the subject matter
                                hereof.
                            </p>
                            <p>
                                Iilonga Namibia shall be entitled to cede, assign and delegate all or any
                                of its rights and obligations in terms of these Terms and Conditions.
                            </p>
                            <p>
                                Should Iilonga Namibia be prevented from fulfilling any of its obligations
                                to you as a result of any event of force majeure, then those obligations
                                shall be deemed to have been suspended to the extent that and for as long
                                as Iilonga Namibia is so prevented from fulfilling them and your
                                corresponding obligations shall be suspended to the corresponding extent.
                                In the event that force majeure continues for more than fourteen days after
                                it has fist occurred then Iilonga Namibia shall be entitled (but not
                                obliged) to terminate all of its rights and obligations in terms of or
                                arising out of these Terms and Conditions by giving notice to the User. An
                                "event of force majeure" shall mean any event or circumstance whatsoever
                                which is not within the reasonable control of including, without
                                limitation, vis major, casus fortuitus, any act of God, strike, theft,
                                riots, explosion, insurrection or other similar disorder, war (whether
                                declared or not) or military operations, the downtime of any external
                                telecommunications line, power failure, international restrictions, any
                                requirement of any international authority, any requirement of any
                                government or other competent local authority, any court order, export
                                control or shortage of transport facilities
                            </p>
                            <p>
                                The Terms and Conditions, as varied by Iilonga Namibia from time to time,
                                constitutes the entire and sole agreement between Iilonga Namibia and the
                                User with regard to the use of the Content and this Website.
                            </p>

                        </div>

                    </div>
                </div>
            </div>
        </section>



    </div>
@endsection
@section('scripts')
@endsection

@section('style')
    <style>

    </style>
@endsection
