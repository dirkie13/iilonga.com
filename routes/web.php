<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();
Route::get('/change-password', 'App\Http\Controllers\ChangePasswordController@index');
Route::post('/change-password', 'App\Http\Controllers\ChangePasswordController@store')->name('change.password');

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::post('/login2', [App\Http\Controllers\HomeController::class, 'login']);



Route::group([
    'namespace'        =>     'App\Http\Controllers',



], function () {
    Route::get('/contact-us', 'ContactUsController@index')->name('contact-us');
    Route::post('/contact-us/send-email', 'ContactUsController@sendEmail')->name('contact-us-send-email');

    Route::get('/about-us', 'AboutUsController@index')->name('about-us');

    Route::get('/terms-and-conditions', 'TermsAndConditionsController@index')->name('terms-and-condtions');

    Route::get('/browse-cv', 'BrowseCvController@index')->name('browse-cv');
    Route::get('/browse-cvs', 'BrowseCvController@filterResults');
    Route::get('/candidate-profile/{id}', 'CandidateProfileController@index')->name('candidate-profile-guest');
});


Route::group([
    'namespace'        =>     'App\Http\Controllers\Candidate',
    'middleware'    =>    ['isCandidate'],
    'prefix'        =>     'candidate',

], function () {


    Route::get('/profile', 'ProfileController@index')->name('candidate.profile');
    Route::post('/profile/update', 'ProfileController@update');
    Route::post('/profile/image/upload', 'ProfileController@imageUpload')->name('candidate.profile.image.upload');
    Route::get('/profile/toggle-activate', 'ProfileController@toggleActivate');

    Route::get('/professional_details', 'ProfessionalDetailController@index')->name('professional-details');
    Route::get('/professional_details/getAllDetail', 'ProfessionalDetailController@getDetail');
    Route::post('/professional_details/skill/add', 'ProfessionalDetailController@addSkill');
    Route::post('/professional_details/skill/edit', 'ProfessionalDetailController@editSkill');
    Route::delete('/professional_details/skill/remove/{skillId}', 'ProfessionalDetailController@removeSkill');

    Route::post('/professional_details/professional_title/update', 'ProfessionalDetailController@updateProfessionalTitle');



    Route::post('/professional_details/profession_category/add', 'ProfessionalDetailController@addCategory');
    Route::post('/professional_details/profession_category/edit', 'ProfessionalDetailController@editCategory');
    Route::delete('/professional_details/profession_category/remove/{categoryId}', 'ProfessionalDetailController@removeCategory');


    Route::get('/employment_history', 'EmploymentHistoryController@index')->name('employment-history');
    Route::get('/employment_history/getAllDetail', 'EmploymentHistoryController@getDetail');
    Route::post('/employment_history/add', 'EmploymentHistoryController@addEmploymentHistory');
    Route::post('/employment_history/edit', 'EmploymentHistoryController@editEmploymentHistory');
    Route::delete('/employment_history/remove/{historyId}', 'EmploymentHistoryController@removeEmploymentHistory');

    Route::get('/education_history', 'EducationHistoryController@index')->name('education-history');

    Route::get('/education_history/getAllDetail', 'EducationHistoryController@getDetail');
    Route::post('/education_history/add', 'EducationHistoryController@addEducationHistory');
    Route::post('/education_history/edit', 'EducationHistoryController@editEducationHistory');
    Route::delete('/education_history/remove/{historyId}', 'EducationHistoryController@removeEducationHistory');



    Route::get('/upload_cv', 'UploadCVController@index')->name('upload-cv');
    Route::get('/upload_cv/getAllDetail', 'UploadCVController@getDetail');

    Route::post('/upload_cv/upload', 'UploadCVController@cvUpload')->name('candidate.cv.upload');
    Route::delete('/upload_cv/remove', 'UploadCVController@cvRemove');
});


Route::group([
    'namespace'        =>     'App\Http\Controllers\Candidate',
    'prefix'        =>     'candidate',

], function () {
    Route::get('/registration', 'RegistrationController@index')->name('registration');
    Route::post('/registration/store', 'RegistrationController@store');
});



Route::group([
    'namespace'        =>     'App\Http\Controllers\Employer',
    'middleware'    =>    ['isBusiness'],
    'prefix'        =>     'employer',

], function () {


    Route::get('/profile', 'ProfileController@index')->name('profile');
    Route::post('/profile/update', 'ProfileController@update');
    Route::get('/manage-jobs', 'ManageJobsController@index')->name('manage-jobs');
    Route::post('/manage-jobs/job-done', 'ManageJobsController@jobDone');
    Route::get('/manage-jobs/getAllDetail', 'ManageJobsController@getDetail');

    Route::get('/wallet', 'WalletController@index')->name('wallet');
    Route::get('/transactions', 'TransactionsController@index')->name('transactions');



    Route::get('/post-new-job', 'PostNewJobController@index')->name('post-new-job');
    Route::post('/post-new-job/create', 'PostNewJobController@create');

    Route::get('/browse-candidates/{jobId}', 'BrowseCandidatesController@index')->name('browse-canidates');
    Route::get('/candidate-profile/{jobId}/{id}', 'CandidateProfileController@index')->name('candidate-profile');
    Route::post('/candidate-profile/buy-candidate/{jobId}/{id}', 'CandidateProfileController@buyCandidate');

    Route::get('/browse-candidates/bought-candidates/{jobId}', 'BrowseCandidatesController@boughtCandidatesIndex');

    Route::get('/wallet/purchase-tokens', 'WalletController@purchaseTokensIndex')->name('purchase-tokens');
    Route::post('/wallet/purchase-tokens/add-payment', 'WalletController@addPayment');

    Route::get('/wallet/purchase-tokens/pay/{paymentId}', 'WalletController@pay');
    Route::get('/wallet/purchase-tokens/pay/{paymentId}/done', 'WalletController@paymentDone');

    Route::get('wallet/confirmpayment/{reference}', 'WalletController@confirmPayment');
    Route::get('wallet/checkpayment/{reference}', 'WalletController@checkPayment');

    Route::get('/wallet/purchase-history', 'WalletController@purchaseHistoryIndex')->name('purchase-history');
    Route::get('/wallet/purchase-history/get-transactions', 'WalletController@getTransactions');
});

Route::group([
    'namespace'        =>     'App\Http\Controllers\Employer',

    'prefix'        =>     'employer',

], function () {

    Route::get('/registration', 'RegistrationController@index')->name('registration');
    Route::post('/registration/store', 'RegistrationController@store');
});

Route::group([
    'namespace'        =>     'App\Http\Controllers\Import',
    'prefix'        =>     'import',

], function () {

    //Route::post('/jobcategories', 'ImportJobCategoriesController@index');
});
